# Kickstart defaults file for an interative install.
# This is not loaded if a kickstart file is provided on the command line.
auth --enableshadow --passalgo=sha512
firstboot --enable
lang en_US.UTF-8
timezone --utc Europe/Zurich
keyboard us
firewall --port=7001:udp --port=4241:tcp --service=ssh
autopart --type=lvm
