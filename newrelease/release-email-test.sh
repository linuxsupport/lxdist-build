MINOR="7.7"
AUTHOR="Ben Morrice"
RELEASE_TEST="2019-09-18"
RELEASE_DATE="2019-09-30"
OS_IMAGE_DATE="2019-09-17"
OS_IMAGE_UUID="6787adb6-982f-4310-a6a8-53d6cee15d0c"
#

DAY=`date -d"$RELEASE_TEST" "+%A"`
TEST_DATE=`date -d"$RELEASE_TEST" "+%d.%m.%Y"`
REL_DATE=`date -d"$RELEASE_DATE" "+%d.%m.%Y"`

cat << EOF
To: linux-users@cern.ch
Bcc: linux-announce@cern.ch, linux-announce-test@cern.ch, ai-admins@cern.ch
Subject : CERN CentOS $MINOR testing available for installation

Dear Linux users.

We are pleased to announce the newly updated CERN Centos Linux (CC7) version:

          *****************************************
          CC $MINOR TEST is available for installation
          *****************************************
                as of $DAY $TEST_DATE

and will become the default CC7 production version on $REL_DATE.

For information about CC${MINOR/.//}, including installation
instructions please visit:

         http://cern.ch/linux/centos7/

* Installation:

  - via PXE network boot menu (http://cern.ch/linux/install/) 

  - non-PXE installation:

    http://linuxsoft.cern.ch/cern/centos/$MINOR/os/x86_64/images/

    and corresponding installation path is:

    http://linuxsoft.cern.ch/cern/centos/$MINOR/os/x86_64/

  - Automated PXE installations using AIMS2 

    Boot target is:
  
      CC${MINOR/./}_X86_64_TEST

  - Openstack TEST image is also available:
    name : CC7 TEST - x86_64 [${OS_IMAGE_DATE}]
    id   : ${OS_IMAGE_UUID}
   
  - Puppet managed VMs can be created with the following options:
    ai-bs --cc7 --nova-image-edition Test [...]
 
* Upgrading from previous CERN CentOS 7.X release:

  All updates will be released to production repositories only on $REL_DATE.

  Until then CC7 system can be updated by running:

  # yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
  # yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update

* Problem reporting:

  Please report problems to Linux.Support@cern.ch

-- 
$AUTHOR on behalf of the Linux Team
EOF
