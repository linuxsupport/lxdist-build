#!/bin/sh

TOP=/mnt/data2/dist/cern/centos/7.8.2003/os/x86_64/CERN/
CREATEREPO=/usr/bin/createrepo
COMPS=$TOP/build/CERN-comps.xml

/usr/bin/xmllint --noout --nowarning --relaxng $TOP/build/comps.rng $COMPS

if [ $? -ne 0 ]; then
 echo "?? problem with comps .. or not"
fi

$CREATEREPO -g $COMPS $TOP

