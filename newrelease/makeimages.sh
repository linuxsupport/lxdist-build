#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root and disable selinux"
  exit
fi

TREE=/mnt/data2/dist/cern/centos/7.8.2003/os/x86_64/

/bin/rm -fv $TREE/CERN/build/upd.img.tmp

(cd $TREE/CERN/build/updates.img ; /usr/bin/find . | /bin/cpio -c -o ) | /bin/gzip -9cv  > $TREE/CERN/build/upd.img.tmp 

/bin/mv -vf $TREE/CERN/build/upd.img.tmp $TREE/images/updates.img

/bin/rm -fv $TREE/CERN/build/pro.img.tmp
/bin/rm -fv $TREE/CERN/build/product.img/.buildstamp

cat > $TREE/CERN/build/product.img/.buildstamp <<EOF
[Main]
Product=CERN CentOS
Version=7
BugURL=https://its.cern.ch/jira/browse/CERNCENTOS
IsFinal=True
UUID=`date +%Y%m%d%H00`.x86_64
EOF

(cd $TREE/CERN/build/product.img ; /usr/bin/find . | /bin/cpio -c -o ) | /bin/gzip -9cv  > $TREE/CERN/build/pro.img.tmp 

/bin/mv -vf $TREE/CERN/build/pro.img.tmp $TREE/images/product.img

/bin/rm -rf $TREE/CERN/build/boot.iso/*
mkdir $TREE/CERN/build/boot.iso
/bin/cp -rf $TREE/CERN/build/boot.iso.org/* $TREE/CERN/build/boot.iso/
/bin/cp -v $TREE/images/product.img $TREE/CERN/build/boot.iso/images/
/bin/cp -v $TREE/images/updates.img $TREE/CERN/build/boot.iso/images/

# new tool
# genisoimage -U -r -v -T -J -joliet-long -V "CentOS 7 x86_64" -volset "CentOS 7 x86_64" -A "CentOS 7 x86_64" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot -o NEWISO.iso $TREE/CERN/build/boot.iso/
pwd

/usr/bin/mkisofs -U -A "CentOS 7 x86_64" -V "CentOS 7 x86_64" \
-volset "CentOS 7 x86_64" -J -joliet-long -r -v -T -x ./lost+found \
-o $TREE/CERN/build/tmp_boot.iso \
-b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 \
-boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot $TREE/CERN/build/boot.iso/

/usr/bin/isohybrid -u $TREE/CERN/build/tmp_boot.iso

/usr/bin/implantisomd5 $TREE/CERN/build/tmp_boot.iso

/bin/mv -vf $TREE/CERN/build/tmp_boot.iso $TREE/images/boot.iso

/bin/rm -rf $TREE/CERN/build/boot.iso/*
