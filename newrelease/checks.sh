#!/bin/bash

MINOR=7.8
VERBOSE=true
ACTION=false

DESTANACONDA="/mnt/data2/dist/cern/centos/$MINOR/os/x86_64/CERN/Packages"
DESTPROD="/mnt/data2/dist/cern/centos/7/cern/x86_64/Packages"
DESTTEST="/mnt/data2/dist/cern/centos/7/cern-testing/x86_64/Packages"

[ ! -f $DESTANACONDA/../../RPM-GPG-KEY-cern ] && echo "RPM-GPG-KEY-cern key is missing in os/"

echo "### Checking repoclosure status"
$(/usr/bin/repoclosure --repofrompath=tmpcern$MINOR,/mnt/data2/dist/cern/centos/$MINOR/os/x86_64/CERN --repofrompath=tmpos$MINOR,/mnt/data2/dist/cern/centos/$MINOR/os/x86_64/ -r tmpcern$MINOR -r tmpos$MINOR &>/dev/null)
if [ $? -eq 0 ] 
then
	if ( $VERBOSE ); then echo "repoclosure OK"; fi
else
	echo "[ERROR] repoclosure failing. Please check which package is missing."
fi

echo "### Checking comps.xml status"
$(diff $DESTPROD/../comps.xml /mnt/data2/dist/cern/centos/$MINOR/os/x86_64/CERN/build/CERN-comps.xml &> /dev/null)
if [ $? -eq 0 ] 
then
	if ( $VERBOSE ); then  echo "comps.xml OK in repository CERN"; fi
else
	echo "[ERROR] comps.xml mismatch with Anaconda and repository CERN"
fi

$(diff $DESTTEST/../comps.xml /mnt/data2/dist/cern/centos/$MINOR/os/x86_64/CERN/build/CERN-comps.xml &> /dev/null)
if [ $? -eq 0 ]
then
	if ( $VERBOSE ); then echo "comps.xml OK in repository CERN-TESTING"; fi
else
	echo "[ERROR] comps.xml mismatch with Anaconda and repository CERN-TESTING"
fi
echo ""
echo ""
echo "### Checking if packages available at install time are available in standard repos"
for pkg in `ls /mnt/data2/dist/cern/centos/$MINOR/os/x86_64/CERN/Packages`
do
	if [ $(ls $DESTPROD/$pkg 2>/dev/null) ]
	then
		if ( $VERBOSE ); then  echo "$pkg in PROD"; fi
	elif [ $(ls $DESTTEST/$pkg 2>/dev/null) ]
	then
		if ( $VERBOSE ); then  echo "$pkg in TEST"; fi
	else	
		if ( $ACTION )
		then
			/bin/cp /mnt/data2/dist/cern/centos/$MINOR/os/x86_64/CERN/Packages/$pkg $DESTTEST/
		fi
		echo "[ERROR] $pkg need to be copied in $DESTTEST/"
	fi
done

echo "### Checking that all RPMS are signed"
echo "-> $DESTTEST" 
rpm -K $DESTTEST/*rpm | grep -v gpg
echo "-> $DESTPROD" 
rpm -K $DESTPROD/*rpm | grep -v gpg
echo "-> $DESTANACONDA" 
rpm -K $DESTANACONDA/*rpm | grep -v gpg
