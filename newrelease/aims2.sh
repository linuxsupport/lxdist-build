#!/bin/bash

MINOR="7.8"
DATA2_PATH="/mnt/data2/dist/cern/centos/$MINOR/"

# Uncomment what is needed.

# _TEST IMAGE
aims2 delimg CC${MINOR/./}_X86_64; aims2 addimg --name CC${MINOR/./}_X86_64 --arch x86_64 --vmlinuz  $DATA2_PATH/os/x86_64/images/pxeboot/vmlinuz --description "CERN CENTOS $MINOR X86_64 TEST" --initrd $DATA2_PATH/os/x86_64/images/pxeboot/initrd.img --uefi
aims2 delimg CC${MINOR/./}_X86_64_TEST; aims2 addimg --name CC${MINOR/./}_X86_64_TEST --arch x86_64 --vmlinuz  ${DATA2_PATH}os/x86_64/images/pxeboot/vmlinuz --description "CERN CENTOS $MINOR X86_64 TEST" --initrd ${DATA2_PATH}os/x86_64/images/pxeboot/initrd.img --uefi

# QA (aimstest.cern.ch)
#aims2 --testserver delimg CC${MINOR/./}_X86_64; aims2 --testserver addimg --name CC${MINOR/./}_X86_64 --arch x86_64 --vmlinuz  $DATA2_PATH/os/x86_64/images/pxeboot/vmlinuz --description "CERN CENTOS $MINOR X86_64" --initrd $DATA2_PATH/os/x86_64/images/pxeboot/initrd.img --uefi
#aims2 --testserver delimg CC7_X86_64; aims2 --testserver addimg --name CC7_X86_64 --arch x86_64 --vmlinuz  $DATA2_PATH/os/x86_64/images/pxeboot/vmlinuz --description "CERN CENTOS 7 X86_64 (LATEST)" --initrd $DATA2_PATH/os/x86_64/images/pxeboot/initrd.img --uefi

# PROD (aims.cern.ch)
#aims2 delimg CC${MINOR/./}_X86_64; aims2 addimg --name CC${MINOR/./}_X86_64 --arch x86_64 --vmlinuz  $DATA2_PATH/os/x86_64/images/pxeboot/vmlinuz --description "CERN CENTOS $MINOR X86_64" --initrd $DATA2_PATH/os/x86_64/images/pxeboot/initrd.img --uefi
#aims2 delimg CC7_X86_64; aims2 addimg --name CC7_X86_64 --arch x86_64 --vmlinuz  $DATA2_PATH/os/x86_64/images/pxeboot/vmlinuz --description "CERN CENTOS 7 X86_64 (LATEST)" --initrd $DATA2_PATH/os/x86_64/images/pxeboot/initrd.img --uefi
#aims2 delimg CC${MINOR/./}_X86_64_TEST;
