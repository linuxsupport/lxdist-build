#!/bin/bash

# Sync from linuxsoft centos mirror
# Dry run only
# Please use the output command to do the real sync

VERSION="7.7.1908"
RSYNC="/usr/bin/rsync -vrlptD" 
SOURCE="/mnt/data1/dist/centos/$VERSION/os/x86_64/"
DEST="/mnt/data2/dist/cern/centos/$VERSION/os/x86_64/"

echo "Dry run:"
$RSYNC -4 -n $SOURCE $DEST
echo "If you want to proceed, please run:"
echo $RSYNC -4 $SOURCE $DEST
