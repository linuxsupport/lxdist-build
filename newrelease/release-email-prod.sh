# Settings
MINOR="7.7"
AUTHOR="Ben Morrice"
RELEASE_DATE="2019-09-30"
OS_IMAGE_DATE="2019-09-17"
OS_IMAGE_UUID="6787adb6-982f-4310-a6a8-53d6cee15d0c"
#

DAY=`date -d"$RELEASE_DATE" "+%A"`
REL_DATE=`date -d"$RELEASE_DATE" "+%d.%m.%Y"`

cat << EOF
To: linux-users@cern.ch
Bcc: linux-announce@cern.ch, linux-announce-test@cern.ch, ai-admins@cern.ch
Subject: CERN CentOS $MINOR available for installation

Dear Linux users.

We are pleased to announce that the updated CERN Centos Linux (CC7) version:

                  *******************
                  CC $MINOR is available 
                  *******************
                as of $DAY $REL_DATE

and is now default CC7 production version.

For information about CC${MINOR/./}, including installation
instructions please visit:

         http://cern.ch/linux/centos7/

* Installation:

  - via PXE network boot menu (http://cern.ch/linux/install/) 

  - non-PXE installation:

    http://linuxsoft.cern.ch/cern/centos/$MINOR/os/x86_64/images/

    and corresponding installation path is:

    http://linuxsoft.cern.ch/cern/centos/$MINOR/os/x86_64/

  - Automated PXE installations using AIMS2 

    Boot targets are:
  
      CC7_X86_64
      CC${MINOR/./}_X86_64

  - A new Openstack image is also available:

      name : CC7 - x86_64 [${OS_IMAGE_DATE}]
      id   : ${OS_IMAGE_UUID}
 
* Upgrading from previous CERN CentOS 7.X release:

  CC7 system can be updated by running:

  # yum --enablerepo={updates,extras,cern,cernonly} clean all
  # yum --enablerepo={updates,extras,cern,cernonly} update

-- 
$AUTHOR on behalf of the Linux Team
EOF
