#!/bin/bash

TOP1=/mnt/data1/dist/cern/centos/7.8.2003/os/x86_64/
TOP2=/mnt/data2/dist/cern/centos/7.8.2003/os/x86_64/


echo "Dry run:"
/usr/bin/rsync -n -avu --delete-after $TOP2 $TOP1  
echo "If you want to proceed, please run:"
echo /usr/bin/rsync -avu --delete-after $TOP2 $TOP1
