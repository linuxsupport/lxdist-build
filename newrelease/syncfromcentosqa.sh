#!/bin/bash

# Sync from qa.centos.org
# Dry run only
# Please use the output command to do the real sync

VERSION="7.8.2003"
RSYNC="/usr/bin/rsync -vrlptD" 
SOURCE="rsync://qa.centos.org/c5beta-qa/CentOS/$VERSION/os/x86_64/"
DEST="/mnt/data2/dist/cern/centos/$VERSION/os/x86_64/"

echo "Dry run:"
$RSYNC -4 -n $SOURCE $DEST
echo "If you want to proceed, please run:"
echo $RSYNC -4 $SOURCE $DEST
