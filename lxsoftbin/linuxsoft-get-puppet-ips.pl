#!/usr/bin/perl
#
# Get IPs of all puppet nodes our PuppetDB knows about and printout a flat json
#
# 2018-10-04 v0.0.1 Jaroslaw.Polok@cern.ch


use strict;
use warnings;
use JSON;
use File::Temp qw/ tempfile /;
use Data::Dumper;
use FileHandle;
use 5.010;

use constant {
CONFIGFILE           => '/etc/linuxsoft-rhel-access.conf',
};

sub errorout {
 my ($msg,$code)=@_;
 printf("Error: ".$msg."\n");
 exit($code);
}

sub readconf {
 my ($cfgfile)= @_;
 my $conf = new FileHandle;
 my %opts = ();
 $conf->open($cfgfile) or errorout("Missing configuration ($cfgfile): $!",-1);
 while(my $l = $conf->getline()){
  chomp($l);
  next if $l =~ m{^#}; 
  next if $l =~ m{^\s$}; 
  my($key,$value) = split("=", $l);
  next if (!defined($key));
  next if ($key =~ m{^WebSet$});
  $opts{'_cnf'}{$key} = $value;
 }
 $conf->close();

 errorout("Configuration missing LanDBUser/LanDBPass.",-1) 
   if (!defined($opts{'_cnf'}{LanDBUser}) || !defined($opts{'_cnf'}{LanDBPass}));

 return (%opts);
}

sub get_puppet_puppetdb {
 my ($user, $pass) = @_;
 my (@puppet_nodes);
 my ($tmpfh,$tmpfname)=tempfile("get_puppet_db-XXXXX",UNLINK=>1);

 $ENV{'KRB5CCNAME'}="FILE:$tmpfname";

 system('echo -n '.$pass.' | kinit '.$user.' 2>&1 >> /dev/null');

 my $curl="/usr/bin/curl -s -L -X GET -u: --negotiate https://constable-simple.cern.ch:9081/pdb/query/v4/facts";

 my $todo=$curl."/ipaddress";
 my $outjson=`$todo`;
 my @out = @{decode_json($outjson)};

foreach my $hdata (@out) {
  next if ($hdata->{value} =~/null/);
  next if ($hdata->{value} =~/^127.0.0.1$/); # could appear if ipv6 only hosts come.
  push(@puppet_nodes,$hdata->{value});
  }

 $todo=$curl."/ipaddress6";
 $outjson=`$todo`;
 @out = @{decode_json($outjson)};

foreach my $hdata (@out) {
  next if ($hdata->{value} =~/null/);
  next if ($hdata->{value} =~/^fe80::/); # link-local address, wont see in our logs.
  push(@puppet_nodes,$hdata->{value});
  }

return @puppet_nodes;
}


my %opts = readconf(CONFIGFILE);
my @allnodes = get_puppet_puppetdb($opts{'_cnf'}{'LanDBUser'},$opts{'_cnf'}{'LanDBPass'});

if (@allnodes) {
 print "{\n";
 foreach my $node (@allnodes) {
  print " \"".$node."\" => true,\n";
 }
 print "}\n";
}



