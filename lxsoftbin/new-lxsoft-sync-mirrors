#!/bin/bash
#
# lxdist-sync-mirrors
#
# Jaroslaw.Polok@cern.ch

[[ $EUID -eq 0 ]] && echo "Do not run it as root. Exit!" && exit 1

#MIRRORSL="rsync.scientificlinux.org::scientific"
MIRRORDAG="rsync://apt.sw.be/pub/freshrpms/pub/dag"
MIRRORATRPMS="rsync://rsync.hrz.tu-chemnitz.de/ftp/pub/linux/ATrpms/"
#MIRRORELREPO="elrepo.reloumirrors.net::elrepo"
MIRRORELREPO="rsync://mirrors.ircam.fr/pub/elrepo/"
MIRRORREPOFORGE="rsync://mirror1.hs-esslingen.de/repoforge/"
MIRROREMI="/afs/cern.ch/project/emi/repository/dist/EMI/"
MIRRORTHEFOREMAN="http://yum.theforeman.org/"
MIRRORCENTOS="eu-msync.centos.org::CentOS"
#MIRRORCENTOSSOURCES="mirror.nsc.liu.se::centos-store"
#MIRRORCENTOSSOURCES="archive.kernel.org::centos-vault"
MIRRORCENTOSSOURCES="vault.centos.org::centos-full-store"
MIRRORCENTOSDEBUG="debuginfo.centos.org::centos-full-debuginfo"
MIRRORLINUXTECH="http://pkgrepo.linuxtech.net/el6/"
MIRRORXROOTD="http://xrootd.cern.ch/sw/repos"
MIRRORCENTOSALT="eu-msync.centos.org::altarch"

MASTER=lxsoftadm01.cern.ch

[ -f /etc/lxsoft.conf ] && . /etc/lxsoft.conf

[ x`/bin/hostname` != x$MASTER ] && echo "This host (`/bin/hostname`) is not MASTER ($MASTER). Exit." && exit 0

SYNC=all
VERBOSE=0
CHECKSUM=0
NOACTION=0

RSYNC="/usr/bin/rsync"
# exclude "hidden" rpm files - typicall other (upstream) rsync in progress..
RSYNC_OPTS=" -H -rlptD --safe-links --delay-updates --delete-after --max-delete=5000 --timeout=3600 --exclude=.*rpm.?*"
RSYNC_VERBOSE=" -v"

help() {
    echo "$(basename $0) [ -c ] [ -v ] [ -q ] [ -n ] [ -f ] [ -s ARG ]"
    echo "          -c = enforce rsync checksums (VERY slow!!)"
    echo "          -v = more verbose sync"
    echo "          -q = be quiet"
    echo "          -n = noaction"
    echo "          -s ARG , where ARG centos, dag, jpackage, atrpms, elrepo, repoforge, emi, linuxtech, centos, centosvault, centosalt, centosdebuginfo"
    exit 0
}

while getopts ":vqnchas:" flag; do
    [ "$flag" == "v" ] && VERBOSE=1
    [ "$flag" == "h" ] && help
    [ "$flag" == "s" ] && SYNC=$OPTARG
    [ "$flag" == "c" ] && CHECKSUM=1
    [ "$flag" == "n" ] && NOACTION=1
    [ "$flag" == "q" ] && RSYNC_VERBOSE=""
done

RSYNC_OPTS="$RSYNC_OPTS $RSYNC_VERBOSE"

[ "$SYNC" == "all" ] && TODO="centosdebuginfo" || TODO=$SYNC
[ $VERBOSE -eq 1 ] && RSYNC_OPTS="$RSYNC_OPTS --progress"
[ $CHECKSUM -eq 1 ] && RSYNC_OPTS="$RSYNC_OPTS -c"
[ $NOACTION -eq 1 ] && RSYNC_OPTS="$RSYNC_OPTS -n"

for COMP in ${TODO}; do

    RSYNC_OPTS_ORG=$RSYNC_OPTS

    case "$COMP" in
    #  "scientific")         TOP_REMOTE=$MIRRORSL;
    #     TOP_LOCAL="/mnt/data1/dist/scientific";
    #     RSYNC_OPTS="$RSYNC_OPTS --exclude=obsolete/" ;;

    "centos")
        TOP_REMOTE=$MIRRORCENTOS
        TOP_LOCAL="/mnt/data1/dist/centos"
        RSYNC_OPTS=""
        [ $VERBOSE -eq 1 ] && RSYNC_OPTS="--progress"
        RSYNC_OPTS="$RSYNC_OPTS -aqzH --delete --delay-updates --exclude=altarch/"
        ;;

    "centosalt")
        TOP_REMOTE=$MIRRORCENTOSALT
        TOP_LOCAL="/mnt/data1/dist/centos/altarch"
        RSYNC_OPTS=""
        [ $VERBOSE -eq 1 ] && RSYNC_OPTS="--progress"
        RSYNC_OPTS="$RSYNC_OPTS -aqzH --delete --delay-updates"
        ;;

    "centosvault")
        TOP_REMOTE=$MIRRORCENTOSSOURCES
        TOP_LOCAL="/mnt/data1/dist/centos-vault"
        RSYNC_OPTS=""
        [ $VERBOSE -eq 1 ] && RSYNC_OPTS="--progress"
        RSYNC_OPTS="$RSYNC_OPTS -aqzH --delete --delay-updates"
        ;;

    "centosdebuginfo")
        TOP_REMOTE=$MIRRORCENTOSDEBUG
        TOP_LOCAL="/mnt/data1/dist/centos-debuginfo"
        RSYNC_OPTS=""
        [ $VERBOSE -eq 1 ] && RSYNC_OPTS="--progress"
        RSYNC_OPTS="$RSYNC_OPTS -aqzH --delete --delay-updates"
        ;;

    "dag")
        TOP_REMOTE="$MIRRORDAG"
        TOP_LOCAL="/mnt/data1/dist/dag"
        RSYNC_OPTS="$RSYNC_OPTS --delete-excluded --exclude=dries/ --exclude=rpmforge/ --exclude=redhat/6.2/ --exclude=redhat/7.3/ --exclude=redhat/8.0/ --exclude=redhat/9/ --exclude=redhat/el2.1/ --exclude=redhat/el3/ --exclude=redhat/el?/en/ppc/ --exclude=bert/ --exclude=fedora/1/ --exclude=fedora/2/ --exclude=fedora/3/ --exclude=fedora/4/ --exclude=fedora/5/ --exclude=fedora/6/ --exclude=redhat/el4/"
        # fix some corrupted file from DAG, detectd by RPM checker. Notified Dag via web interface, no reaction..
        RSYNC_OPTS="$RSYNC_OPTS --exclude=tinc-1.0.4-1.1.el3.rf.x86_64.rpm --exclude=blobwars-1.07-1.el5.rf.i386.rpm --exclude=mod_suphp-0.6.0-2.1.el3.rf.i386.rpm --exclude=mod_suphp-0.6.0-2.rf.src.rpm"
        ;;

    "jpackage")
        TOP_REMOTE="rsync://mirrors.dotsrc.org/jpackage"
        TOP_LOCAL="/mnt/data1/dist/jpackage"
        RSYNC_OPTS="$RSYNC_OPTS --exclude=1.0/ --exclude=1.5/ --exclude=1.6/ --exclude=1.7/"
        ;;

    "atrpms")
        TOP_REMOTE="$MIRRORATRPMS"
        TOP_LOCAL="/mnt/data1/dist/atrpms"
        RSYNC_OPTS="$RSYNC_OPTS --exclude=sl3* --exclude=el* --exclude=fc* --exclude=rawhide* --exclude=rh* --exclude=sl10* --exclude=sl4* --exclude=f15* --exclude=f16* --exclude=f17* --exclude=f18* --exclude=f19* --exclude=f20* --exclude=f21*"
        RSYNC_OPTS="$RSYNC_OPTS --exclude=src/f15* --exclude=src/f16/* --exclude=src/f17* --exclude=src/sl4* --exclude=src/f18* --exclude=src/f19* --exclude=src/f20* --exclude=src/f21*"
        RSYNC_OPTS="$RSYNC_OPTS --exclude=debug/sl4* --exclude=debug/f15* --exclude=debug/f16* --exclude=debug/f17* --exclude=debug/f18* --exclude=debug/f19* --exclude=debug/f20* --exclude=debug/f21*"
        ;;

    "elrepo")
        TOP_REMOTE=$MIRRORELREPO
        TOP_LOCAL="/mnt/data1/dist/elrepo"
        RSYNC_OPTS="$RSYNC_OPTS --exclude=favicon.ico"
        ;;

    "repoforge")
        TOP_REMOTE=$MIRRORREPOFORGE
        TOP_LOCAL="/mnt/data1/dist/repoforge"
        RSYNC_OPTS="-ai4CH --safe-links --delay-updates --exclude=redhat/el2.1 --exclude=redhat/el3 --exclude=redhat/el4"
        ;;

    "emi")
        TOP_REMOTE="$MIRROREMI"
        TOP_LOCAL="/mnt/data1/dist/emi"
        RSYNC_OPTS="$RSYNC_OPTS --include=2 --include=3 --exclude=3/SL5 --exclude=3/debian --exclude=2/debian --exclude=2/RC --exclude=0 --exclude=1 --exclude=certification --exclude=testing --exclude=devel --exclude=tarball --exclude=deployment --exclude=backup2"
        ;;
        #  "theforeman" )   TOP_REMOTE=""
        #                                TOP_LOCAL=""
        #       # let's do it with lftp
        #       RSYNC_OPTS=""
        #       RSYNC="/usr/bin/lftp -e 'mirror --verbose=1 --exclude icons/ --exclude releases/1.0 / /mnt/data1/dist/theforeman; quit' $MIRRORTHEFOREMAN"
        #       ;;

    "linuxtech")
        TOP_REMOTE=""
        TOP_LOCAL=""
        # let's do it with lftp
        RSYNC_OPTS=""
        RSYNC="/usr/bin/lftp -e 'mirror --verbose=1 --exclude linode.png --exclude obsolete_packages --exclude workinprogress /el6/ /mnt/data1/dist/linuxtech/el6/; quit' $MIRRORLINUXTECH"
        ;;
        #  "xrootd" )     TOP_REMOTE=""
        #                                TOP_LOCAL=""
        #                                # let's do it with lftp
        #                                RSYNC_OPTS=""
        #                                RSYNC="/usr/bin/lftp -e 'mirror --verbose=1 --exclude xrdtest . /mnt/data1/dist/xrootd/; quit' $MIRRORXROOTD"
        #       ;;

    *)
        echo "Error: unknown comp to sync"
        exit 1
        ;;
    esac

    if [ "$RSYNC_VERBOSE" != "" ]; then
        echo "Starting $(basename $0) at $(date) for $COMP"
        echo "Running: $RSYNC $TOP_REMOTE/ $TOP_LOCAL/ $RSYNC_OPTS"
    fi
    eval $RSYNC $TOP_REMOTE/ $TOP_LOCAL/ $RSYNC_OPTS
    ret=$?
    [ "$ret" -eq 0 ] || echo "last $RSYNC returned an error: $ret"
    if [ "$RSYNC_VERBOSE" != "" ]; then
        echo "Finished $(basename $0) at $(date)."
    fi

    RSYNC_OPTS=$RSYNC_OPTS_ORG

done
