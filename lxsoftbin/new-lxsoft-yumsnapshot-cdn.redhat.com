#!/bin/bash
#
# lxoft-sync-yumsnapshot-cdn.redhat.com
#
# script to take snapshot of yum repositories for given date.
#
# Jaroslaw.Polok@cern.ch
#

#[[ $EUID -eq 0 ]] && echo "Do not run it as root. Exit!" && exit 1


MASTER=lxsoftadm01.cern.ch

[ -f /etc/lxsoft.conf ] && . /etc/lxsoft.conf

[ x`/bin/hostname` != x$MASTER ] && echo "This host (`/bin/hostname`) is not MASTER ($MASTER). Exit." &&  exit 0


SNAP="all"
VERBOSE=0
CHECKSUM=0
NOACTION=0
FORCE=0
DATE=`/bin/date +%Y%m%d`
TODO="rhel5 rhel6 rhel7 rhvt5 rhev6 rhev7 rhrt6 rhrt7 rhel5-fastrack rhel6-fastrack rhel7-fastrack" 
SPREF="/mnt/data1/dist"
DPREF="/mnt/data1/dist/internal/yumsnapshot"

help() {
 echo "`basename $0` [ -v ][ -f ][ -n ] [ -d YYYYMMDD ] [ -s ARG ]"
 echo "          -f = force the run"
 echo "          -v = verbose"
 echo "          -n = noaction"
 echo "          -d YYYYMMDD - run for given date"
 echo "          -s ARG , where ARG = $TODO"
 exit 0 
}

while getopts  ":vhs:nfd:" flag
do
  [ "$flag" == "v" ] && VERBOSE=1
  [ "$flag" == "h" ] && help
  [ "$flag" == "s" ] && SNAP=$OPTARG
  [ "$flag" == "n" ] && NOACTION=1
  [ "$flag" == "f" ] && FORCE=1
  [ "$flag" == "d" ] && DATE=$OPTARG
done


[ "$SNAP" != "all" ] && TODO=$SNAP

for COMP in ${TODO}; do

 case "$COMP" in

  "rhel5")
                TOPSRC="cdn.redhat.com/content/dist/rhel/server/5/5Server"
		PROT=""
		TOPPROT=""
                TOPDST=$TOPSRC
                ARCHES="i386 x86_64"
                REPOS="
			debug
			os
			source/SRPMS
			oracle-java/os
			oracle-java/source/SRPMS
			rh-common/debug
			rh-common/os
			rh-common/source/SRPMS
			productivity/debug
			productivity/os
			productivity/source/SRPMS
			rhn-tools/debug
			rhn-tools/os
			rhn-tools/source/SRPMS
			supplementary/debug
			supplementary/os
			supplementary/source/SRPMS
			rhev-agent/3/debug
                        rhev-agent/3/os
                        rhev-agent/3/source/SRPMS
                        rhev-mgmt-agent/3/debug
                        rhev-mgmt-agent/3/os
                        rhev-mgmt-agent/3/source/SRPMS
			"
                ;;

   "rhvt5")
                TOPSRC="cdn.redhat.com/content/dist/rhel/server/5/5Server"
                PROT=""
	        TOPPROT=""
                TOPDST=$TOPSRC
                ARCHES="i386 x86_64"
                REPOS="
                        vt/debug
                        vt/os
                        vt/source/SRPMS
                        "               
                ;;


  "rhel5-fastrack")
                TOPSRC="cdn.redhat.com/content/fastrack/rhel/server/5"
		PROT=""
		TOPPROT=""
                TOPDST=$TOPSRC
                ARCHES="i386 x86_64"
                REPOS="
			debug
			os
			source/SRPMS
			"
                ;;


  "rhel6")
                TOPSRC="cdn.redhat.com/content/dist/rhel/server/6/6Server"
		PROT=""
		TOPPROT=""
                TOPDST=$TOPSRC
                ARCHES="i386 x86_64"
                REPOS="
			os
			extras/os
			extras/debug
			extras/source/SRPMS
			debug
			source/SRPMS
			rhn-tools/debug
			rhn-tools/os
			rhn-tools/source/SRPMS
			subscription-asset-manager/1/debug
			subscription-asset-manager/1/os
			subscription-asset-manager/1/source/SRPMS
			optional/debug
			optional/os
			optional/source/SRPMS
			supplementary/debug
			supplementary/os
			supplementary/source/SRPMS
			rhev-agent/3/debug
                        rhev-agent/3/os
                        rhev-agent/3/source/SRPMS
			rhev-mgmt-agent/3/debug
                        rhev-mgmt-agent/3/os
                        rhev-mgmt-agent/3/source/SRPMS
			rhv-agent/4/debug
                        rhv-agent/4/os
                        rhv-agent/4/source/SRPMS
                        v2win/debug
                        v2win/os
                        v2win/source/SRPMS
			"
                ;;
  "rhev6")
		TOPSRC="cdn.redhat.com/content/dist/rhel/server/6/6Server"
                TOPDST=$TOPSRC
		PROT="../../../../../../../../../../.rhev"
		TOPPROT="rhevh"
                ARCHES="x86_64"
                REPOS="
			rhevh/os	
			rhevh/debug
			rhevh/source/SRPMS
			rhevm/3.3/os
			rhevm/3.3/debug
			rhevm/3.3/source/SRPMS
			rhevm/3.4/os
                        rhevm/3.4/debug
                        rhevm/3.4/source/SRPMS
			rhevm/3.5/os
                        rhevm/3.5/debug
                        rhevm/3.5/source/SRPMS
			rhevm/3.6/os
                        rhevm/3.6/debug
                        rhevm/3.6/source/SRPMS
			"
                ;;

  "rhrt6")
		TOPSRC="cdn.redhat.com/content/dist/rhel/server/6/6Server"
                TOPDST=$TOPSRC
                PROT="../../../../../../../../../../.rhrt"
                TOPPROT="mrg-r"
                ARCHES="x86_64"
                REPOS="
                        mrg-r/2/os
                        mrg-r/2/debug
                        mrg-r/2/source/SRPMS
                        "
                ;;

  "rhel6-fastrack")
                TOPSRC="cdn.redhat.com/content/fastrack/rhel/server/6"
                TOPDST=$TOPSRC
		PROT=""
		TOPPROT=""
                ARCHES="i386 x86_64"
                REPOS="
			debug
			os
			source/SRPMS
			optional/debug
			optional/os
			optional/source/SRPMS
			"
                ;;

                 
  "rhel7")	
		TOPSRC="cdn.redhat.com/content/dist/rhel/server/7/7Server"
		TOPDST=$TOPSRC
		PROT=""
		TOPPROT=""
		ARCHES="x86_64"
		REPOS="
			os
			debug
			optional/os
			optional/source/SRPMS
			optional/debug
			extras/os	
			extras/source/SRPMS
			extras/debug
			supplementary/os
			supplementary/debug
			supplementary/source/SRPMS
			rhn-tools/source/SRPMS
			rhn-tools/os
			rhn-tools/debug
			oracle-java/os
			oracle-java/source/SRPMS
			rh-common/os
			rh-common/source/SRPMS
			rh-common/debug
			debug
			source/SRPMS
			rhev-mgmt-agent/3/debug
                        rhev-mgmt-agent/3/os
                        rhev-mgmt-agent/3/source/SRPMS
			rhv-mgmt-agent/4/debug
                        rhv-mgmt-agent/4/os
                        rhv-mgmt-agent/4/source/SRPMS
                        v2win/os
                        v2win/debug
                        v2win/source/SRPMS
			"
		;;

   "rhev7")
		TOPSRC="cdn.redhat.com/content/dist/rhel/server/7/7Server"
                TOPDST=$TOPSRC
		PROT="../../../../../../../../../../.rhev"
		TOPPROT="rhevh"
                ARCHES="x86_64"
		REPOS="
			rhevh/debug
			rhevh/os
			rhevh/source/SRPMS
			rhevm/3.5/os
			rhevm/3.5/source/SRPMS
			rhevm/3.5/debug
			rhevm/3.6/os
                        rhevm/3.6/source/SRPMS
                        rhevm/3.6/debug
			rhv/4.0/os
			rhv/4.0/debug
			rhv/4.0/source/SRPMS
			rhvh-build/4/os
			rhvh-build/4/debug
			rhvh-build/4/source/SRPMS
			"		
		;;

  "rhrt7")
		TOPSRC="cdn.redhat.com/content/dist/rhel/server/7/7Server"
                TOPDST=$TOPSRC
		PROT="../../../../../../../../../../.rhrt"
		TOPPROT="rt"
                ARCHES="x86_64"
                REPOS="
			rt/os
			rt/debug
			rt/source/SRPMS
			"               
                ;;


  "rhel7-fastrack")
		TOPSRC="cdn.redhat.com/content/fastrack/rhel/server/7"
		TOPDST=$TOPSRC
		PROT=""
		TOPPROT=""
                ARCHES="x86_64"
                REPOS="
			debug 
                        os 
                        source/SRPMS 
                        optional/debug 
                        optional/os 
                        optional/source/SRPMS 
			"
                ;;		

  *) echo "Error: unknown comp to snapshot"; exit 1 ;;
 esac



 [ $VERBOSE -eq 1 ] && echo "Starting `basename $0` at `date` for $COMP"

 if [ ! -d $DPREF/$DATE/cdn.redhat.com ]; then
  [ $VERBOSE -eq 1 ] && echo /bin/mkdir -p $DPREF/$DATE/cdn.redhat.com
  [ $NOACTION -eq 0 ] && /bin/mkdir -p $DPREF/$DATE/cdn.redhat.com
 fi
 if [ ! -r $DPREF/$DATE/cdn.redhat.com/.htaccess ]; then
  [ $VERBOSE -eq 1 ] && echo /bin/ln -sf ../../.rhel/.htaccess $DPREF/$DATE/cdn.redhat.com/.htaccess
  [ $NOACTION -eq 0 ] && /bin/ln -sf ../../.rhel/.htaccess $DPREF/$DATE/cdn.redhat.com/.htaccess
 fi

 for ARCH in ${ARCHES}; do
  for REPO in ${REPOS}; do
    if [ -d $DPREF/$DATE/$TOPDST/$ARCH/$REPO ] && [ $FORCE -ne 1 ]; then
       [ $VERBOSE -eq 1 ] && echo "skipping $COMP ($REPO), already there"
    else
       [ $VERBOSE -eq 1 ] && [ $FORCE -eq 1 ] && echo /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO 
       [ $NOACTION -eq 0 ] && [ $FORCE -eq 1 ] && /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO
       [ $VERBOSE -eq 1 ] && echo /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO
       [ $NOACTION -eq 0 ] && /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO
       echo "/bin/cp -Rl $SPREF/$TOPSRC/$ARCH/$REPO/* $DPREF/$DATE/$TOPDST/$ARCH/$REPO/"
       [ $NOACTION -eq 0 ] && /bin/cp -Rl $SPREF/$TOPSRC/$ARCH/$REPO/* $DPREF/$DATE/$TOPDST/$ARCH/$REPO/

       [ $VERBOSE -eq 1 ] && echo /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO/{repodata,comps.xml,*-comps.xml.gz,updateinfo.xml,*-updateinfo.xml.gz,productid.gz}
       [ $NOACTION -eq 0 ] && /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO/{repodata,comps.xml,*-comps.xml.gz,updateinfo.xml,*-updateinfo.xml.gz,productid.gz}
       [ $VERBOSE -eq 1 ] && echo /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata
       [ $NOACTION -eq 0 ] && /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata
       echo "/usr/bin/rsync -rlptD $SPREF/$TOP/$TOPSRC/$ARCH/$REPO/repodata/ $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata/"
       [ $NOACTION -eq 0 ] && /usr/bin/rsync -rlptD $SPREF/$TOPSRC/$ARCH/$REPO/repodata/ $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata/

       [ $NOACTION -eq 0 ] && [ -f $SPREF/$TOPSRC/$ARCH/$REPO/comps.xml ] && /bin/cp $SPREF/$TOPSRC/$ARCH/$REPO/comps.xml $DPREF/$DATE/$TOPDST/$ARCH/$REPO/
       [ $NOACTION -eq 0 ] && [ -f $SPREF/$TOPSRC/$ARCH/$REPO/updateinfo.xml ] && /bin/cp $SPREF/$TOPSRC/$ARCH/$REPO/updateinfo.xml $DPREF/$DATE/$TOPDST/$ARCH/$REPO/
       [ $NOACTION -eq 0 ] && [ -f $SPREF/$TOPSRC/$ARCH/$REPO/productid.gz ] && /bin/cp $SPREF/$TOPSRC/$ARCH/$REPO/productid.gz $DPREF/$DATE/$TOPDST/$ARCH/$REPO/

       if [ "x$PROT" != "x" ] && [ "x$TOPPROT" != "x" ]; then 
	 [ $VERBOSE -eq 1 ] && echo /bin/ln -sf $PROT/.htaccess $DPREF/$DATE/$TOPDST/$ARCH/$TOPPROT/.htaccess
         [ $NOACTION -eq 0 ] && /bin/ln -sf $PROT/.htaccess $DPREF/$DATE/$TOPDST/$ARCH/$TOPPROT/.htaccess	
       fi
       
       		
    fi
  done
 done 

done

[ $VERBOSE -eq 1 ] && echo "Finished `basename $0` at `date`."
