#!/usr/bin/env python

import os
import os.path
from optparse import OptionParser
import sqlite3
import re
import createrepo_c as cr


def parse_updateinfo(path,cursor,db,arch):
    uinfo = cr.UpdateInfo(path)
    for update in uinfo.updates:
	try: 
          cursor.execute("INSERT INTO channelerrata(advisory, synopsis, issue_date, update_date) VALUES(?,?,?,?);",(update.id,update.title,update.issued_date,update.updated_date))
        except:
          cursor.execute("UPDATE channelerrata SET advisory=?, synopsis=?, issue_date=?, update_date=? WHERE advisory=?;",(update.id,update.title,update.issued_date,update.updated_date,update.id))
          pass
       
        try:
          cursor.execute("INSERT INTO errata(advisory,type, description) VALUES(?,?,?);",(update.id,update.type,update.description))
        except:
          cursor.execute("UPDATE errata SET advisory=?, type=?, description=? WHERE advisory=?;",(update.id,update.type,update.description,update.id))
          pass
         
        db.commit()

#        print "From:         %s" % update.fromstr
#        print "Status:       %s" % update.status
#        print "Type:         %s" % update.type
#        print "Version:      %s" % update.version
#        print "Id:           %s" % update.id
#        print "Title:        %s" % update.title
#        print "Issued date:  %s" % update.issued_date
#        print "Updated date: %s" % update.updated_date
#        print "Rights:       %s" % update.rights
#        print "Release:      %s" % update.release
#        print "Pushcount:    %s" % update.pushcount
#        print "Severity:     %s" % update.severity
#        print "Summary:      %s" % update.summary
#        print "Description:  %s" % update.description
#        print "Solution:     %s" % update.solution
#        print "References:"
        for ref in update.references:

             if ref.type == "bugzilla":
                cursor.execute("SELECT id FROM erratabugzilla WHERE advisory=? AND bugzilla_id=?;",(update.id,ref.id))
                if cursor.fetchone() == None:
                   cursor.execute("INSERT INTO erratabugzilla(advisory, bugzilla_id, summary) VALUES(?,?,?);",(update.id,ref.id,ref.title))
                else:
                   cursor.execute("UPDATE erratabugzilla SET advisory=?,bugzilla_id=?,summary=? WHERE advisory=? AND bugzilla_id=?;",(update.id,ref.id,ref.title,update.id,ref.id))
             
             if ref.type == "cve":
                cursor.execute("SELECT id FROM erratacves  WHERE advisory=? AND cve_id=?;",(update.id,ref.id))
                if cursor.fetchone() == None:
                   cursor.execute("INSERT INTO erratacves(advisory,cve_id) VALUES(?,?);",(update.id,ref.id))
                else:
                   cursor.execute("UPDATE erratacves SET advisory=?,cve_id=? WHERE advisory=? AND cve_id=?;",(update.id,ref.id,update.id,ref.id))

             db.commit()
#            print "  Href:  %s" % ref.href
#             print "  Id:    %s" % ref.id
#             print "  Type:  %s" % ref.type
#            print "  Title: %s" % ref.title
#            print "  ----------------------------"
#        print "Pkglist (collections):"
        for col in update.collections:
#             print "  Short: %s" % col.shortname
#             print "  name:  %s" % col.name
             try: 
                cursor.execute("INSERT INTO misc(id,lastrundate,channelname) VALUES(?,?,?);",(1,"2006-06-01",col.name))
             except:
                cursor.execute("UPDATE misc SET id=?,lastrundate=?,channelname=? WHERE id=?;",(1,"2006-06-01",col.name,1))
                pass
#            print "  Packages:"
             try:
                cursor.execute("DELETE FROM erratapackages WHERE advisory=?;",(update.id))
             except:
                pass

             db.commit()

             for pkg in col.packages:
		 if re.match( "noarch|%s" % arch, pkg.arch):
                    cursor.execute("INSERT INTO erratapackages(advisory, name, version, release, epoch, arch_label, file, md5sum) VALUES(?,?,?,?,?,?,?,?);",(update.id, pkg.name, pkg.version, pkg.release, pkg.epoch, pkg.arch, pkg.filename, pkg.sum))
                
             db.commit()
#            for pkg in col.packages:
#                print "    Name:     %s" % pkg.name
#                print "    Version:  %s" % pkg.version
#                print "    Relase:   %s" % pkg.release
#                print "    Epoch:    %s" % pkg.epoch
#                print "    Arch:     %s" % pkg.arch
#                print "    Src:      %s" % pkg.src
#                print "    Filename: %s" % pkg.filename
#                print "    Sum:      %s" % pkg.sum
#                print "    Sum type: %s (%s)" % (pkg.sum_type, cr.checksum_name_str(pkg.sum_type))
#                print "    Reboot suggested: %s" % pkg.reboot_suggested
#            print "  ----------------------------"
#        print "=============================="


if __name__ == "__main__":
    parser = OptionParser(usage="%prog -a arch -f updateinfo.xml -d dbfile")
    parser.add_option("-f","--file",dest="updinfofile")
    parser.add_option("-d","--database",dest="dbfile")
    parser.add_option("-a","--arch",dest="archlabel")
    (options, args) = parser.parse_args() 
#    print options.a   
#    if len(args) != 2:
#        parser.error("You have to specify: -f updateinfo.xml -d dbfile")

    if options.archlabel == 'i686': 
       arch="i386|i486|i586|i686"
    else:
       arch=options.archlabel

    db = sqlite3.connect(options.dbfile)
    db.text_factory = str
    cursor = db.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS channelerrata ( advisory VARCHAR ( 255 ) PRIMARY KEY, synopsis VARCHAR ( 255 ), issue_date VARCHAR ( 255 ), update_date VARCHAR ( 255 ));")
    cursor.execute("CREATE TABLE IF NOT EXISTS errata ( advisory VARCHAR ( 255 ) PRIMARY KEY, type VARCHAR ( 255 ) , description VARCHAR ( 255 ) );" )
    cursor.execute("CREATE TABLE IF NOT EXISTS misc ( id INTEGER PRIMARY KEY, lastrundate DATE, channelname VARCHAR ( 255 ) );")
    cursor.execute("CREATE TABLE IF NOT EXISTS erratabugzilla(id INTEGER PRIMARY KEY, advisory VARCHAR ( 255 ), bugzilla_id VARCHAR ( 255 ), summary VARCHAR ( 255 ));" )
    cursor.execute("CREATE TABLE IF NOT EXISTS erratacves( id INTEGER PRIMARY KEY, advisory VARCHAR ( 255 ), cve_id VARCHAR ( 255 ) );" )
    cursor.execute("CREATE TABLE IF NOT EXISTS erratapackages( id INTEGER PRIMARY KEY, advisory VARCHAR ( 255 ), name VARCHAR ( 255 ), version VARCHAR ( 255 ), release VARCHAR ( 255 ), epoch VARCHAR ( 255 ), arch_label VARCHAR ( 255 ), file VARCHAR ( 255 ), md5sum VARCHAR ( 255 ));" )
    db.commit()

    parse_updateinfo(options.updinfofile,cursor,db,arch)

    db.commit()
    db.close() 

