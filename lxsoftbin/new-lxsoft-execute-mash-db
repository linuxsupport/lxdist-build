usage()
{
cat << EOF
usage: $0 [-t <tag(s)>]

This script generate mash repo from all SIG tags.

OPTIONS:
   -d          mash repo destination : default to $MASH_DEST
   -s          koji instance fqdn : default to $KOJI_FQDN
   -t          tag(s) e.g:  "ai7-qa", "ai7-testing ai7-stable"
   -f          force mash to run
   -v          verbose
   -w          write a status report file
   -n          dry run ; run mash but do not sync to $DEST
EOF
}


MASTER=lxsoftadm01.cern.ch

[ -f /etc/lxsoft.conf ] && . /etc/lxsoft.conf

[ x`/bin/hostname` != x$MASTER ] && echo "This host (`/bin/hostname`) is not MASTER ($MASTER). Exit." &&  exit 0


# Global config
TOPDIR="/mnt/data2/mash"
# koji binary
KOJI="/usr/bin/koji"
# mash binary
MASH="/usr/bin/mash"
# default fqdn
KOJI_FQDN="koji.cern.ch"
KOJIHUB_FQDN="kojihub.cern.ch"
LINUXSOFT_FQDN="linuxsoft.cern.ch"
# mash config containing mash.conf
MASH_CONF="$TOPDIR/etc/mash"
MASH_DEST="$TOPDIR/repos"
MASH_CACHE="$TOPDIR/cache"
MASH_STATUS="/mnt/data1/dist/internal/repos/status-db" # option -w
KOJI_GPG_KEYS="2F4F9F6E 246F2A64"
KOJI_EXCLUDE="^slc[5,6,7]|^cc7|^scl6|^ev6|^jbeap|^rhc6|^el6_|^el5_|^rhcommon|^devtoolset|nonpae"
PUBLIC_REPOLIST="$TOPDIR/etc/public.conf"

# log directory
LOG_DIR="$TOPDIR/log"
RSYNC="/usr/bin/rsync -v -rlptD --delete-after"
RSYNC_ARGS=""
RSYNC_DEST="/mnt/data1/dist/internal/repos"
RSYNC_PUBLIC="/mnt/data1/dist/repos"
# End Global

# nss fix for el6/el7
export NSS_STRICT_NOFORK=DISABLED

optiond=false
optionf=false
options=false
optiont=false
optionv=false
optionn=false
optionw=false

while getopts ":hvfnwd:s:t:" OPTION
do
	case ${OPTION} in
	h)
		usage
		exit 0
		;;
	t)
		optiont=true
		TAGS="${OPTARG}"
		;;
	d)
		optiond=true
		MASH_DEST="${OPTARG}"
		;;
	f)
		optionf=true
		;;
	n)
		optionn=true
		;;
	s)
		options=true
		KOJI_FQDN="${OPTARG}"
		;;
	v)
		optionv=true
		;;
	w)
		optionw=true
		;;
	?)
		exit 0
		;;
	:)
		echo "Option -${OPTARG} requires an argument."
		exit 1
		;;
	esac
done

shift $((${OPTIND} - 1))

# Repoviewurl in mash config
MASH_VIEWURL="http://$LINUXSOFT_FQDN/internal/repos"

${KOJI} --noauth moshimoshi &>/dev/null
koji_retval=$?
if [ ! $koji_retval -eq 0 ]
then
    echo "[ERROR] Koji is down. Exiting... (error: $koji_retval)"
    exit 1
else
    ( $optionv ) && echo "Koji is up and running..."
fi

if ( ! ${optiont} )
then
	QA_TAGS=`${KOJI} list-tags | grep "\-qa" | grep -vE "${KOJI_EXCLUDE}" | grep "^db"`
	TESTING_TAGS=`${KOJI} list-tags | grep "\-testing" | grep -vE "${KOJI_EXCLUDE}" | grep "^db"` 
	STABLE_TAGS=`${KOJI} list-tags | grep "\-stable" | grep -vE "${KOJI_EXCLUDE}" | grep "^db"`
	TAGS="${TESTING_TAGS} ${QA_TAGS} ${STABLE_TAGS}"
fi

print_mash_conf()
{
cat << EOF
[defaults]
configdir = $MASH_CONF
buildhost = https://${KOJIHUB_FQDN}/kojihub
repodir = https://${KOJI_FQDN}/kojifiles
use_sqlite = True
use_repoview = False
# this is for RHEL5 compatibility
compress_type = compat
EOF
}

print_mash_template()
{
	local tag=$1
	local arches=$2
	local latest=$3
cat << EOF
[${tag}]
rpm_path = ${MASH_DEST}/${tag}/%(arch)s/os/Packages
repodata_path = ${MASH_DEST}/${tag}/%(arch)s/os/
source_path = source/SRPMS
debuginfo = True
multilib = False
multilib_method = devel
tag = ${tag}
inherit = False
keys = ${KOJI_GPG_KEYS}
strict_keys = False
use_repoview = False
repoviewurl = ${MASH_VIEWURL}/${tag}/%(arch)s/os/
repoviewtitle = "${tag^^}"
arches = ${arches}
delta = True
latest = ${latest}
EOF
}

mash_prepare ()
{
	local tag=$1
	local log=$2
	local arches=$3
    local latest=$4

	local conf=`mktemp`
	# config mash already ok
	( $optionv ) && echo " -> [INFO] Checking mash config: ${tag}.mash..."
	print_mash_template "${tag}" "${arches}" "${latest}" > $conf
	[ -f ${MASH_CONF}/${tag}.mash ] && diff ${MASH_CONF}/${tag}.mash $conf &>> $log
	if [ $? -gt 0 ]
	then
		( $optionv ) && echo " -> [INFO] updating mash config ${tag}.mash"
		mv $conf ${MASH_CONF}/${tag}.mash
		( $optionv ) && echo " -> [INFO] cleaning mash cache ${tag}.buildlist"
        [ -f $MASH_CACHE/${tag}.buildlist ] && rm $MASH_CACHE/${tag}.buildlist
	else
		rm $conf
	fi
}

check_public () {
	local tag=$1
	local log=$2
    local htaccess='Require all granted\n'
    local public=false

    for T in `cat $PUBLIC_REPOLIST 2>/dev/null`
    do
        [[ $tag == "$T"[0-9]* ]] && public=true && break
    done
    
    if ( $public ) 
    then
        [ ! -f ${MASH_DEST}/${tag}/.htaccess ] && printf "${htaccess}" > ${MASH_DEST}/${tag}/.htaccess
        [ ! -h $RSYNC_PUBLIC/${tag} ] && ln -s  $RSYNC_DEST/${tag}  $RSYNC_PUBLIC/${tag}
    else
        [ -h $RSYNC_PUBLIC/${tag} ] && rm $RSYNC_PUBLIC/${tag}
        [ -f ${MASH_DEST}/${tag}/.htaccess ]  && rm -f ${MASH_DEST}/${tag}/.htaccess
    fi
}

rsync_run () {
	local tag=$1
	local log=$2
    [[ ! -d "${RSYNC_DEST}/${tag}" ]] && mkdir -p ${RSYNC_DEST}/${tag}
    ( $optionv ) && echo " -> [INFO] running rsync: ${tag} ..."
    ${RSYNC} ${RSYNC_ARGS} ${MASH_DEST}/${tag} ${RSYNC_DEST} &>> ${log}
	END=`date "+%Y-%m-%d %H:%M:%S"`
    echo ${END} > ${RSYNC_DEST}/${tag}/.last
}

mash_run () {
	local tag=$1
	local log=$2
	( $optionv ) && echo " -> [INFO] running mash: ${tag} ..."
	${MASH} --no-delta -c ${MASH_CONF}/mash.conf -o ${MASH_DEST} -p ${MASH_DEST} ${tag} &>> ${log}
	if [ $? -gt 0 ] 
	then
		NOW=`date "+%Y-%m-%d %H:%M:%S"`
		( $optionw ) && printf "FAILED  (at %s)\n" "$NOW" >> $MASH_STATUS
		echo " -> [ERROR] mash run failed ${log}"
		[ -f ${MASH_CACHE}/${tag}.buildlist ] && rm -rf $MASH_CACHE/${tag}.buildlist
	else
		NOW=`date "+%Y-%m-%d %H:%M:%S"`
		( $optionw ) && printf "DONE    (at %s)\n" "$NOW" >> $MASH_STATUS
		( $optionv ) && echo " -> [INFO] mash run succeeded ${log}"
	fi
    check_public ${tag} ${log}
    rsync_run ${tag} ${log}
}

if [ ! -d $TOPDIR ] 
then
    echo " -> [INFO] Creating local directories"
    [ ! -d $MASH_CONF ] && mkdir -p $MASH_CONF || exit 2
    [ ! -d $MASH_DEST ] && mkdir -p $MASH_DEST || exit 2
    [ ! -d $MASH_CACHE ] && mkdir -p $MASH_CACHE || exit 2
fi

[ ! -f $MASH_CONF/mash.conf ] && print_mash_conf > $MASH_CONF/mash.conf

# Ensure the cache directory is available
[ ! -d $MASH_CACHE ] && mkdir -p $MASH_CACHE
[ ! -d $LOG_DIR ] && mkdir -p $LOG_DIR

if ( ! ${optiont} )
then
	# Ensure we do not have parallel runs
	pidfile=/var/tmp/mash-run-db.pid
	if [ -e $pidfile ]; then
		pid=`cat $pidfile`
		if kill -0 &> /dev/null $pid; then
			( $optionv ) && echo "Mash is already running PID:$pid"
			exit 0
		else
			rm $pidfile &>/dev/null
		fi
	fi
	echo $$ > $pidfile
fi

# Cache target list as it cannot include unknown tag at this point
TARGETS=`mktemp`
${KOJI} list-targets --quiet > $TARGETS
START=`date "+%Y-%m-%d %H:%M:%S"`

( $optionf ) && ( optionw=false )
( $optionv ) && echo "Number of tags to check: `echo $TAGS|wc -w`"
( $optionw ) && echo "[Last mash run started at $START]" > $MASH_STATUS
( $optionw ) && echo "[Number of tags to check: `echo $TAGS|wc -w`]" >> $MASH_STATUS

for TAG in ${TAGS}
do
    ONLY_LATEST="False"
    # Skip mash run if force was used by an admin and not finished.
    [ -f $MASH_CACHE/$TAG.force ] && continue

	( $optionf ) && touch $MASH_CACHE/$TAG.force
	( $optionf ) && rm $MASH_CACHE/$TAG.buildlist && echo "Cache cleared for $TAG tag"
	( $optionv ) && echo "Checking $TAG ..."
	( $optionw ) && printf "regenerating %-40s: " $TAG >> $MASH_STATUS
	NOW=`date "+%Y-%m-%d %H:%M:%S"`
	ARCHES=""
	BUILDTAG=""
    LASTRUN=""
    LAST=""
    if [[ $TAG = *"-testing"* ]]
	then
		ONLY_LATEST="True"
	fi
	LOG="${LOG_DIR}/${TAG}.log"

	FAKETAG=${TAG/-qa/-testing}
	FAKETAG=${FAKETAG/-stable/-testing}

	BUILDTAG=`grep ${FAKETAG} ${TARGETS} | awk '{print $2}' | head -1`
	[ ! -z "$BUILDTAG" ] && ARCHES=`${KOJI} taginfo ${BUILDTAG} | grep Arches | cut -d ":" -f 2-`
	[ -z "$ARCHES" ] && ARCHES="x86_64"
	# Mash not happy with i686 wants i386
	ARCHES=${ARCHES/i686/i386}
	mash_prepare "${TAG}" "${LOG}" "${ARCHES}" "${ONLY_LATEST}"
	if [ ! -f $MASH_CACHE/$TAG.buildlist ]
	then
        	${KOJI} list-tagged $TAG > $MASH_CACHE/$TAG.buildlist
	else
		BUILDLIST=`mktemp`
		${KOJI} list-tagged $TAG > $BUILDLIST
		echo "begin diff ->" &>> $LOG
		diff $BUILDLIST $MASH_CACHE/$TAG.buildlist &>> $LOG
		if [ $? -eq 0 ]
		then
            [ -f ${RSYNC_DEST}/${TAG}/.last ] && LAST=`cat $RSYNC_DEST/$TAG/.last` && LASTRUN="(last run at $LAST)"
		    ( $optionw ) && printf "SKIPPED (no new build at %s) %s\n" "$NOW" "$LASTRUN" >> $MASH_STATUS
			echo " -> skipping. No new build in $TAG" &>> $LOG
			[ -f $BUILDLIST ] &&  rm -rf $BUILDLIST
			continue
		else
			echo " -> updating cache for $TAG" &>> $LOG
			cp $BUILDLIST $MASH_CACHE/$TAG.buildlist
		fi
		[ -f $BUILDLIST ] &&  rm -f $BUILDLIST
	fi
	mash_run "${TAG}" "${LOG}"
	( $optionf ) && rm -f $MASH_CACHE/$TAG.force
done

wait
END=`date "+%Y-%m-%d %H:%M:%S"`
( ! $optiont ) && rm $pidfile
exit 0
