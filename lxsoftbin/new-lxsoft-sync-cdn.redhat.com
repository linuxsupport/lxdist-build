#!/bin/bash

#
# lxsoft-sync-cdn.redhat.com
#
# script to sync lxdist /mnt/data1/dist/cdn.redhat.com 
# from Red Hat Content Delivery Network , using reposync
#
# 28.02.2015 v0.1 - Jaroslaw.Polok@cern.ch
#

[[ $EUID -eq 0 ]] && echo "Do not run it as root. Exit!" && exit 1


MASTER=lxsoftadm01.cern.ch

[ -f /etc/lxsoft.conf ] && . /etc/lxsoft.conf

[ x`/bin/hostname` != x$MASTER ] && echo "This host (`/bin/hostname`) is not MASTER ($MASTER). Exit." &&  exit 0



ALLCOMPS="7Server 6Server 5Server"

reposetup() {
 case "$1" in
 
  "7Server" )
        RNAMEPREF="rhel-7|rhel-server-x86_64-rhscl-7|jb-eap-7-for-rhel-7|jb-eap-7.1-for-rhel-7|rh-sso-7.2-for-rhel-7"
        TOPDST="/mnt/data1/dist/cdn.redhat.com"
        REPOSHA=""
 	;;

  "6Server" )       
        RNAMEPREF="rhel-6|rhel-server-i386-rhscl-6|rhel-server-x86_64-rhscl-6|rhel-server-i386-dts-6|rhel-server-x86_64-dts-6"
        TOPDST="/mnt/data1/dist/cdn.redhat.com"
        REPOSHA=""
	;;

  "5Server" )
	RNAMEPREF="rhel-5|rhel-server-i386-dts-5|rhel-server-x86_64-dts-5"
        TOPDST="/mnt/data1/dist/cdn.redhat.com"
        REPOSHA="-s sha"
	;; 
        
   *) 
	echo "Error: unknown dist to sync" 
	exit 1 
	;;
 esac
} # reposetup()


COMP="all"
VERBOSE=0
FORCE=0
NOACTION=0
LOCAL=0
GENONLY=0

TOPCFG=/mnt/data2/etc/reposync/cdn.redhat.com
YCM="/usr/bin/yum-config-manager -c $TOPCFG/yum.conf"
REPOSYNC="/usr/bin/reposync --config=$TOPCFG/yum.conf"
REPOSYNCCACHE="/mnt/data2/etc/reposync/cdn.redhat.com/cache"
CREATEREPO="/usr/bin/createrepo"
MODIFYREPO="/usr/bin/modifyrepo"
CREATEREPO_OPTS="--workers 100"
# --update
MODIFYREPO_OPTS=""
REPOSYNC_OPTS="--downloadcomps --download-metadata --norepopath --source --gpgcheck"

ALLREPOS="beta rhn-tools fastrack productivity server
	  rh-common rhv rhev rhevh rhvh rhevm rhev-mgmt-agent realtime eus-7.4 eus eus-7.3 eus-7.2
	  supplementary thirdparty-oracle vt rhscl els jb-eap
	  extras optional sam v2vwin rt highavailability loadbalancer rh-sso" 
TMPMSG=`/bin/mktemp /tmp/$$.XXXXXXXX`


help() {
 echo "`basename $0` [ -q | -h | -g | -n | -v | -f ] [ -s dist ] [ -r repo ]"
 echo "          -q = be quiet"
 echo "          -g = only generate repo metadata"
 echo "          -n = noaction"
 echo "          -v = verbose"
 echo "          -r repo ($ALLREPOS)"
 echo "          -f = force (removes reposync cache)"
 echo "          -s dist (all $ALLCOMPS)"
 exit 0 
}

mail() {
 /bin/mail -s "lxsoft-sync-cdn.redhat.com - $1 new package(s)" $2  < $3
}

while getopts  ":qvhngfs:r:" flag
do
  [ "$flag" == "h" ] && help
  [ "$flag" == "s" ] && SYNC=$OPTARG 
  [ "$flag" == "n" ] && NOACTION=1
  [ "$flag" == "q" ] && REPOSYNC_OPTS="$REPOSYNC_OPTS --quiet"
  [ "$flag" == "g" ] && GENONLY=1
  [ "$flag" == "v" ] && VERBOSE=1
  [ "$flag" == "f" ] && FORCE=1
  [ "$flag" == "r" ] && OREPO=$OPTARG
done

OK=0
if [ "x$OREPO" != "x" ]; then
 for TMPR in $ALLREPOS; do
  [ $TMPR == $OREPO ] && OK=1
 done
 [ $OK -ne 1 ] && echo "Error: unknown repo to sync" &&  exit 1
fi


if [ "$SYNC" == "all" ];  then
  TODO=$ALLCOMPS
else
  reposetup $SYNC
  TODO=$SYNC
fi

for COMP in ${TODO}; do

 reposetup $COMP

 if [ "x$OREPO" == "x" ]; then
  REPOS=`$YCM | /bin/grep -P "^\[(${RNAMEPREF})" | /bin/sed "s/\[//g" | /bin/sed "s/\]//g"` 
 else
  REPOS=`$YCM | /bin/grep -P "^\[(${RNAMEPREF})" | /bin/sed "s/\[//g" | /bin/sed "s/\]//g" | /bin/grep -P "(\-)?${OREPO}\-"` 
 fi

 COUNTPRETOTAL=0
 COUNTPOSTTOTAL=0

 for REPO in $REPOS; do

   reposetup $COMP
  
   echo $REPO

   REPOPATH=`$YCM ${REPO} | /bin/grep baseurl | /bin/sed "s@^.*cdn.redhat.com@${TOPDST}@"`

   COUNTPRE=0
   COUNTPOST=0
   SIZEPRE=0
   SIZEPOST=0
   PRELIST=""
   POSTLIST=""
   CHANGELIST=""

   if [ $GENONLY -ne 1 ]; then

     echo "Synchronizing repo: $REPO"
           
          if [ $FORCE -eq 1 ]; then
              echo "Running: /bin/rm -rf $REPOSYNCCACHE/$REPO" 
              [ $NOACTION -ne 1 ] && [ -d $REPOSYNCCACHE/$REPO ] && /bin/rm -rf $REPOSYNCCACHE/$REPO/*
          fi


          # create it to create empty repos too ..

          [ ! -d $REPOPATH ] && /bin/mkdir -p $REPOPATH

          COUNTPRE=`/usr/bin/find $REPOPATH -type f -name "*.rpm" ! -size 0 | /usr/bin/wc -l` 
          PRELIST=`/usr/bin/find $REPOPATH -type f -name "*.rpm" ! -size 0 -exec /bin/basename {} \;` 

          SIZEPRE=`/usr/bin/du -bc Packages/*rpm *rpm source/SRPMS/*rpm source/SRPMS/Packages/*rpm 2>/dev/null | /usr/bin/tail -1 | /bin/sed "s/\stotal//g"`
         if [[ $REPO =~ "-arm-" ]]; then
           REPOARCH="-a aarch64"
         else
           REPOARCH=""
         fi

         COMM="$REPOSYNC $REPOARCH $REPOSYNC_OPTS --download_path=$REPOPATH --repoid=$REPO --cachedir=$REPOSYNCCACHE"
	 echo "repo : $COMM"
	 [ $VERBOSE -eq 1 ] && echo "Running: $COMM"
         [ $NOACTION -ne 1 ] && $COMM
         # ERROR HANDLING HERE ! 

	  COUNTPOST=`/usr/bin/find $REPOPATH -type f -name "*.rpm" ! -size 0 | /usr/bin/wc -l` 
	  POSTLIST=`/usr/bin/find $REPOPATH -type f -name "*.rpm" ! -size 0 -exec /bin/basename {} \;` 

	  [ $COUNTPRE -ne $COUNTPOST ] && CHANGELIST=`/usr/bin/diff --changed-group-format='%>' --unchanged-group-format='' <(echo "$PRELIST") <(echo "$POSTLIST")`	

          SIZEPOST=`/usr/bin/du -bc Packages/*rpm *rpm source/SRPMS/*rpm source/SRPMS/Packages/*rpm 2>/dev/null | /usr/bin/tail -1 | /bin/sed "s/\stotal//g"`

          # cdn.redhat.com delivers us some rpms with junk attached at the end ... ?
          # these are 'useable' but since change size metadata must be regenerated to
          # take it into account ..

          [ $SIZEPRE -ne $SIZEPOST ] && [ $COUNTPRE -eq $COUNTPOST ] && echo "WARN: no rpm count change but size change (pre: $SIZEPRE, post: $SIZEPOST)" >> $POSTLIST
    fi 
  
          if [ $GENONLY -eq 1 ] || [ $FORCE -eq 1 ] || [ $COUNTPRE -ne $COUNTPOST ] || [ $SIZEPRE -ne $SIZEPOST ]; then


             # create it to create empty repos too ..

             [ ! -d $REPOPATH ] && /bin/mkdir -p $REPOPATH

             echo "Removing 0 bytes rpms"

             /usr/bin/find $REPOPATH -type f -size 0b -name "*.rpm" -exec rm -v {} \; 

	     echo "Generating repo metadata: $REPO"	

             COMPXMLFILE=`/bin/ls -1tr $REPOPATH/*-comps*xml.gz  2>&1 | /usr/bin/tail -1` 

	     if [ -f "$COMPXMLFILE" ]; then
		/bin/cp -f $COMPXMLFILE $REPOPATH/comps.xml.gz
                /bin/gunzip -f $REPOPATH/comps.xml.gz

               /bin/rm -f $REPOPATH/*-comps*xml.gz

              COMM="$CREATEREPO $REPOSHA $CREATEREPO_OPTS -g $REPOPATH/comps.xml -o $REPOPATH $REPOPATH"

             else
   
              COMM="$CREATEREPO $REPOSHA $CREATEREPO_OPTS -o $REPOPATH $REPOPATH"
 
             fi

              [ $VERBOSE -eq 1 ] && echo "Running: $COMM"
              [ $NOACTION -ne 1 ] && $COMM

	      /bin/rm -f $REPOPATH/comps.xml	

	     # CHECK ERRORS HERE !	

             UPDXMLFILE=`/bin/ls -1tr $REPOPATH/*-updateinfo.xml.gz 2>&1 | /usr/bin/tail -1`

	     if [ -f "$UPDXMLFILE" ]; then
                /bin/cp -f $UPDXMLFILE $REPOPATH/updateinfo.xml.gz
                /bin/gunzip -f $REPOPATH/updateinfo.xml.gz
               
                /bin/rm -f $REPOPATH/*-updateinfo.xml.gz

		COMM="$MODIFYREPO $REPOSHA $MODIFYREPO_OPTS $REPOPATH/updateinfo.xml $REPOPATH/repodata"
                [ $VERBOSE -eq 1 ] && echo "Running: $COMM"
                [ $NOACTION -ne 1 ] && $COMM

                # CHECK ERRORS HERE !
             fi
          else
             echo "Not running createrepo/modifyrepo: no new rpms"
	     /bin/rm -f $REPOPATH/*-updateinfo.xml.gz
	     /bin/rm -f $REPOPATH/*-comps*xml.gz
	     /bin/rm -f $REPOPATH/*-updateinfo.xml
          fi 

         if [ $COUNTPRE -ne $COUNTPOST ]; then
            COUNTPRETOTAL=$(($COUNTPRETOTAL + $COUNTPRE))
            COUNTPOSTTOTAL=$(($COUNTPOSTTOTAL + $COUNTPOST)) 
            echo "repo: $REPO - $(($COUNTPOST - $COUNTPRE)) new package(s):" >> $TMPMSG
            echo " " >> $TMPMSG
            mapfile -t CHANGEARR <<< "$CHANGELIST"
            for LINE in "${CHANGEARR[@]}"
             do
               echo ${LINE} >> $TMPMSG
             done
            echo " " >> $TMPMSG
            echo " " >> $TMPMSG
         fi
  done
done

[ $COUNTPRETOTAL -ne $COUNTPOSTTOTAL ] && mail $(($COUNTPOSTTOTAL - $COUNTPRETOTAL)) root  $TMPMSG
[ -f $TMPMSG ] && /bin/rm $TMPMSG
