#!/bin/bash

DIST="oc7"
DISTTAG="oc7"

#koji add-tag $DIST
koji add-tag $DIST-testing
koji add-tag $DIST-qa
koji add-tag $DIST-stable
koji add-tag --arches "x86_64" $DIST-build

koji add-target $DIST $DIST-build $DIST-testing

koji add-external-repo --tag=$DIST-build cc7-updates
koji add-external-repo --tag=$DIST-build cc7-extras
koji add-external-repo --tag=$DIST-build cc7-os
koji add-external-repo --tag=$DIST-build buildsys7
koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which buildsys-macros-$DISTTAG cern-koji-addons tar scl-utils-build
koji add-group-pkg $DIST-build srpm-build bash curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils buildsys-macros-$DISTTAG cern-koji-addons tar scl-utils-build

koji add-tag-inheritance --priority 1 $DIST-build $DIST-testing

