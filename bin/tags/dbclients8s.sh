#!/bin/bash

TAG="dbclients8s"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
#EPEL=0
DISTTAG="el8s.cern"

source createtag8s.sh

# Needed for https://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL8/oracle/instantclient/x86_64/
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient -p 5 --mode=bare
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient21 -p 6 --mode=bare --arch x86_64
