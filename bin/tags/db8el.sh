#!/bin/bash

TAG="db8el"
#ARCHES="x86_64"
TESTING=1
EPEL=1
#DISTTAG=""

### You probably don't need to change anything below this
source createtag8el.sh
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient -p 21 --mode=bare
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient21 -p 22 --mode=bare --arch x86_64
$KOJI add-external-repo -t ${TAG}-build postgresql-rhel-8-x86_64 -p 23 --mode=bare
