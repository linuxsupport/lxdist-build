#!/bin/bash

TAG="linuxsupport8"
ARCHES="x86_64 aarch64"
PERMS="admin"
#TESTING=0
EPEL=1
DISTTAG="el8.cern"

### You probably don't need to change anything below this
source createtag8.sh
