#!/bin/bash

TAG="hpc8s"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
#EPEL=0
DISTTAG="el8s.cern"

### You probably don't need to change anything below this
source createtag8s.sh
$KOJI add-tag-inheritance --priority 2 ${TAG}-build batch8s-testing
