#!/bin/bash

TAG="buildsys9"
ARCHES="x86_64 aarch64"
PERMS="admin"


### You probably don't need to change anything below this
PARENT="centos9-defaults"
PROFILE="koji" # change it for the test instance
KOJI="/usr/bin/koji -p $PROFILE"

$KOJI remove-tag $TAG-build

$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-build
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-testing
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-qa
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-stable

$KOJI edit-tag $TAG-testing --perm="$PERMS"
$KOJI edit-tag $TAG-qa      --perm="$PERMS"
$KOJI edit-tag $TAG-stable  --perm="$PERMS"

$KOJI add-target $TAG $TAG-build $TAG-testing

