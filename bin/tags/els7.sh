#!/bin/bash
# This tag is used for rebuilding ELS7 packages
# We use els7-build when building "el7" SRPMs
# We use els7_9-build when building "el7_9" SRPMs
# The difference between these two is just buildsys-macros-el7.cern vs buildsys-macros-el7_9.cern
# The final tag for all ELS packages is els7-stable

DISTS="els7 els7_i386 els7_9 els7_9_i386"
for DIST in $DISTS
do
  if [[ $DIST == *"7_9"* ]]; then
    BUILDSYS_PACKAGE="buildsys-macros-el7_9"
  else
    BUILDSYS_PACKAGE="buildsys-macros-el7"
  fi
  koji remove-tag $DIST-build

  if [[ $DIST == *"i386"* ]]; then
    koji add-tag --arches "i686" $DIST-build
  else
    koji add-tag --arches "x86_64" $DIST-build
  fi
  koji add-group $DIST-build build
  koji add-group $DIST-build srpm-build
  koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar cern-koji-addons $BUILDSYS_PACKAGE python-rpm-macros python3-rpm-macros
  koji add-group-pkg $DIST-build srpm-build bash curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar $BUILDSYS_PACKAGE cern-koji-addons python-rpm-macros python3-rpm-macros

  if [[ $DIST == *"i386"* ]]; then
    # The dependency fixes repo is to cover a corner case where java requires
    # tzdata > 2023, but only tzdata = 2020 is provided by base. Due to the way
    # koji works out dependencies, even though updated versions of tzdata exist
    # in updates, they are never taken into account. With this dependency repo
    # we promote these packages before everything else.
    koji add-external-repo --tag=$DIST-build centos7-dependency-fixes-i386
    koji add-external-repo --tag=$DIST-build centos7-base-i386
    koji add-external-repo --tag=$DIST-build centos7-updates-i386
  fi
  koji add-external-repo --tag=$DIST-build rhel7-os
  koji add-external-repo --tag=$DIST-build rhel7-optional
  koji add-external-repo --tag=$DIST-build rhel7-devtools
  koji add-external-repo --tag=$DIST-build rhel7-sclo
  koji add-external-repo --tag=$DIST-build linuxsupport7-stable
  koji add-external-repo --tag=$DIST-build epel7
  koji add-external-repo --tag=$DIST-build buildsys7

  koji add-tag $DIST-testing
  koji add-tag $DIST-qa
  koji add-tag $DIST-stable

  koji add-tag-inheritance --priority 1 $DIST-build $DIST-testing

  koji add-target $DIST $DIST-build $DIST-testing

  # For ELS package rebuilds, we disable the %check stage
  # This is done for several reasons:
  # a) Some upstream packages require custom rpm macros to be set in order for package tests to pass
  # b) Some tests can take hours to complete
  # c) We are simply rebuilding, so there is nothing that can change and thus we don't need to tes
  # As there is not currently a nice way to instruct rpmbuild to disable checks, we can simply exit
  # at the %check stage.
  # See https://github.com/rpm-software-management/rpm/issues/316 for more details
  koji edit-tag $DIST-build -x rpm.macro.__spec_check_pre=exit
done
