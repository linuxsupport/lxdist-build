#!/bin/bash

TAG="el7_6.cern"
DIST="cc7-cern-$TAG-biarch"

koji remove-tag $DIST-build

koji add-tag --arches "i686 x86_64" $DIST-build
# that one exists
#koji add-tag --arches "x86_64" cc7-cern-testing

koji add-target $DIST $DIST-build cc7-cern-testing

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar buildsys-macros-$TAG
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-$TAG

koji add-external-repo --tag=$DIST-build c7-multiarch-cr
koji add-external-repo --tag=$DIST-build c7-multiarch-extras
koji add-external-repo --tag=$DIST-build c7-multiarch-updates
koji add-external-repo --tag=$DIST-build c7-multiarch-os
koji add-external-repo --tag=$DIST-build cc7-extras
koji add-external-repo --tag=$DIST-build buildsys7

koji add-tag-inheritance --priority 5 $DIST-build cc7-cern-testing
