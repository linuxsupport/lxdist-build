#!/bin/bash

TAG="rhel8-image-8x"

koji add-tag --arches "x86_64 aarch64" $TAG-build

koji add-group $TAG-build build
koji add-group $TAG-build srpm-build
koji add-group-pkg $TAG-build build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz
koji add-group-pkg $TAG-build srpm-build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz

koji add-external-repo --tag=$TAG-build rhel8-cern -p 20 --mode=bare
koji add-external-repo --tag=$TAG-build rhel8-baseos -p 30 --mode=bare
koji add-external-repo --tag=$TAG-build rhel8-appstream -p 40 --mode=bare
koji add-external-repo --tag=$TAG-build rhel8-codeready-builder -p 50 --mode=bare

koji add-tag $TAG

koji add-target $TAG $TAG-build $TAG
