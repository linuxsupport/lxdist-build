#!/bin/bash

DIST="ai7_rh-ruby22"

koji add-tag --arches "x86_64" $DIST-build

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar cern-koji-addons buildsys-macros-ai7 rh-ruby22-build
koji add-group-pkg $DIST-build srpm-build bash curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-ai7 cern-koji-addons rh-ruby22-build

koji add-external-repo --tag=$DIST-build cc7-updates
koji add-external-repo --tag=$DIST-build cc7-extras
koji add-external-repo --tag=$DIST-build cc7-os
koji add-external-repo --tag=$DIST-build cc7-sclo-rh
koji add-external-repo --tag=$DIST-build epel7
koji add-external-repo --tag=$DIST-build buildsys7

koji add-target $DIST $DIST-build ai7-testing
