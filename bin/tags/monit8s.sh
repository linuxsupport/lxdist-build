#!/bin/bash

TAG="monit8s"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
EPEL=1
DISTTAG="el8s.cern"

### You probably don't need to change anything below this
source createtag8s.sh

$KOJI add-external-repo --tag=$TAG-build dl.yarnpkg.com -p 100 --mode=bare
