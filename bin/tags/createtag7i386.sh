BASE_DIST=el7

DIST="el7-i686"

#koji add-tag $DIST
koji add-tag $DIST
koji add-tag $DIST-testing
koji add-tag $DIST-stable
koji add-tag --parent $DIST --arches "i686" $DIST-build
koji add-target $DIST $DIST-build
koji edit-target --build-tag=$DIST-build --dest-tag=$BASE_DIST-testing $DIST
koji add-external-repo --tag=$DIST-build buildsys
koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which buildsys-macros-$DIST cern-koji-addons tar
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils buildsys-macros-$DIST cern-koji-addons tar
koji add-tag-inheritance --priority 1 $DIST-build $DIST-testing
#koji add-tag-inheritance --priority 2 $DIST-build $BASE_DIST-build

# ADD mash files
# ADD to the policy
# ADD users from egroup
