DIST=wlcgmon5
BASE_DIST=el5

koji add-tag $DIST
koji add-tag $DIST-testing
koji add-tag $DIST-qa
koji add-tag $DIST-stable
koji add-tag --parent $DIST --arches "i386 x86_64" $DIST-build
koji add-target $DIST $DIST-build
koji edit-target --build-tag=$DIST-build --dest-tag=$DIST-testing $DIST
koji add-external-repo --tag=$DIST-build slc5-os
koji add-external-repo --tag=$DIST-build slc5-updates
koji add-external-repo --tag=$DIST-build slc5-extras
koji add-external-repo --tag=$DIST-build slc5-cernonly
koji add-external-repo --tag=$DIST-build buildsys
koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which buildsys-macros-$DIST cern-koji-addons tar buildsys-macros-slc5
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils buildsys-macros-$DIST cern-koji-addons tar buildsys-macros-slc5
koji add-tag-inheritance --priority 2 $DIST-build $BASE_DIST-build
koji add-tag-inheritance --priority 1 $DIST-build $DIST-testing
