BASE_DIST=el6

DIST="el6_9"

koji add-tag --arches "i686 x86_64" $DIST-build
koji add-target $DIST $DIST-build el6
koji add-external-repo --tag=$DIST-build slc6-updates
koji add-external-repo --tag=$DIST-build slc6-extras
koji add-external-repo --tag=$DIST-build slc6-cernonly
koji add-external-repo --tag=$DIST-build slc6-os
koji add-external-repo --tag=$DIST-build buildsys
koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which buildsys-macros-$DIST cern-koji-addons tar
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils buildsys-macros-$DIST cern-koji-addons tar
koji add-tag-inheritance --priority 2 $DIST-build el6

# ADD mash files
# ADD to the policy
# ADD users from egroup
