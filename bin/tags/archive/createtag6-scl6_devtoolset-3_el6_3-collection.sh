BASE_DIST=el6

DIST=scl6
COLLECTIONS="devtoolset-3_el6_3"

for COLLECTION in $COLLECTIONS
do
        koji remove-tag ${DIST}_$COLLECTION-build
        koji add-tag ${DIST}_$COLLECTION
        koji add-tag ${DIST}_$COLLECTION-testing
        koji add-tag ${DIST}_$COLLECTION-stable
        koji add-tag --parent ${DIST}_$COLLECTION --arches "i686 x86_64" ${DIST}_$COLLECTION-build
        koji add-target ${DIST}_$COLLECTION ${DIST}_$COLLECTION-build
        koji edit-target --build-tag=${DIST}_$COLLECTION-build --dest-tag=$DIST-testing ${DIST}_$COLLECTION

        koji add-external-repo --tag=${DIST}_$COLLECTION-build slc6-os
        koji add-external-repo --tag=${DIST}_$COLLECTION-build slc6-updates
        koji add-external-repo --tag=${DIST}_$COLLECTION-build slc6-extras
        koji add-external-repo --tag=${DIST}_$COLLECTION-build slc6-cernonly

        koji add-group ${DIST}_$COLLECTION-build build
        koji add-group ${DIST}_$COLLECTION-build srpm-build
        koji add-group-pkg ${DIST}_$COLLECTION-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which devtoolset-3-build cern-koji-addons tar scl-utils-build buildsys-macros-el6_3
        koji add-group-pkg ${DIST}_$COLLECTION-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils devtoolset-3-build cern-koji-addons tar scl-utils-build buildsys-macros-el6_3
        koji add-tag-inheritance --priority 1 ${DIST}_$COLLECTION-build ${DIST}-testing
        koji add-tag-inheritance --priority 2 ${DIST}_$COLLECTION-build $BASE_DIST-build
done
