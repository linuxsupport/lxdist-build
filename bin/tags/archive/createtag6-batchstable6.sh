#!/bin/bash

DIST="batchstable6"

koji add-tag --arches "i686 x86_64" $DIST-build

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar buildsys-macros-$DIST buildsys-macros-el6
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-$DIST buildsys-macros-el6


koji add-external-repo --tag=$DIST-build htcondor6-stable
koji add-external-repo --tag=$DIST-build slc6-cernonly
koji add-external-repo --tag=$DIST-build slc6-extras
koji add-external-repo --tag=$DIST-build slc6-updates
koji add-external-repo --tag=$DIST-build slc6-os
koji add-external-repo --tag=$DIST-build buildsys
koji add-external-repo --tag=$DIST-build epel6

koji add-target $DIST $DIST-build batch6-testing

koji add-tag-inheritance $DIST-build batch6-testing
