BASE_DIST=el6

DIST="mwr6"
koji remove-tag $DIST-build
koji add-tag $DIST-testing
koji add-tag $DIST-qa
koji add-tag $DIST-stable
koji add-tag --arches "i686 x86_64" $DIST-build
koji add-target $DIST $DIST-build $DIST-testing

koji add-external-repo --tag=$DIST-build epel6
koji add-external-repo --tag=$DIST-build slc6-extras
koji add-external-repo --tag=$DIST-build slc6-updates
koji add-external-repo --tag=$DIST-build slc6-cernonly
koji add-external-repo --tag=$DIST-build slc6-os
koji add-external-repo --tag=$DIST-build buildsys

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which cern-koji-addons tar buildsys-macros-$BASE_DIST
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils cern-koji-addons tar  buildsys-macros-$BASE_DIST
koji add-tag-inheritance --priority 1 $DIST-build $DIST-testing
