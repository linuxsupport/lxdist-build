BASE_DIST=el6
DIST="ceph6"

koji remove-tag $DIST-build

koji add-tag $DIST-testing
koji add-tag $DIST-qa
koji add-tag $DIST-stable
koji add-tag --arches "x86_64" $DIST-build
koji add-target $DIST $DIST-build $DIST-testing
koji add-external-repo --tag=$DIST-build slc6-updates
koji add-external-repo --tag=$DIST-build slc6-extras
koji add-external-repo --tag=$DIST-build slc6-os
koji add-external-repo --tag=$DIST-build slc6-cernonly
koji add-external-repo --tag=$DIST-build slc6-sclo-rh
koji add-external-repo --tag=$DIST-build slc6-sclo-sclo
koji add-external-repo --tag=$DIST-build epel6
koji add-external-repo --tag=$DIST-build buildsys
koji add-group $DIST-build build
koji add-group $DIST-build srpm-build

# hack: for building support python27 packages
#koji add-group-pkg $DIST-build build python27-scldevel python27-build python27-runtime bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which buildsys-macros-$DIST cern-koji-addons tar 
#koji add-group-pkg $DIST-build srpm-build python27-scldevel python27-build python27-runtime bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils buildsys-macros-$DIST cern-koji-addons tar

koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which buildsys-macros-$DIST cern-koji-addons tar

koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils buildsys-macros-$DIST cern-koji-addons tar

koji add-tag-inheritance --priority 1 $DIST-build $DIST-testing
koji add-tag-inheritance --priority 2 $DIST-build $DIST-stable
