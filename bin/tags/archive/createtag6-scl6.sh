BASE_DIST=el6

DIST=scl6
COLLECTIONS="mysql55 php54 mariadb55 nodejs010 perl516 postgresql92 python27 python33 ruby193"

#koji add-tag $DIST
koji add-tag $DIST
koji add-tag $DIST-testing
koji add-tag $DIST-stable
koji add-tag --parent $DIST --arches "i686 x86_64" $DIST-build
koji add-target $DIST $DIST-build
koji edit-target --build-tag=$DIST-build --dest-tag=$DIST-testing $DIST
koji add-external-repo --tag=$DIST-build slc6-os
koji add-external-repo --tag=$DIST-build slc6-updates
koji add-external-repo --tag=$DIST-build slc6-extras
koji add-external-repo --tag=$DIST-build slc6-cernonly
#koji add-external-repo --tag=$DIST-build epel6
koji add-external-repo --tag=$DIST-build buildsys
#koji add-external-repo --tag=$DIST-build devtoolset6
#koji add-external-repo --tag=$DIST-build devtoolset6-testing
koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which buildsys-macros-ai6 cern-koji-addons tar scl-utils-build 
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils buildsys-macros-$DIST cern-koji-addons tar scl-utils-build
koji add-tag-inheritance --priority 1 $DIST-build $DIST-testing
koji add-tag-inheritance --priority 2 $DIST-build $BASE_DIST-build

# ADD mash files
# ADD to the policy
# ADD users from egroup

for COLLECTION in $COLLECTIONS
do
        koji remove-tag ${DIST}_${COLLECTION}-build
        koji add-tag ${DIST}_${COLLECTION}
        koji add-tag ${DIST}_${COLLECTION}-testing
        koji add-tag ${DIST}_${COLLECTION}-stable
        koji add-tag --parent ${DIST}_${COLLECTION} --arches "i686 x86_64" ${DIST}_${COLLECTION}-build
        koji add-target ${DIST}_${COLLECTION} ${DIST}_${COLLECTION}-build
        koji edit-target --build-tag=${DIST}_${COLLECTION}-build --dest-tag=$DIST-testing ${DIST}_${COLLECTION}

        koji add-external-repo --tag=${DIST}_${COLLECTION}-build slc6-os
        koji add-external-repo --tag=${DIST}_${COLLECTION}-build slc6-updates
        koji add-external-repo --tag=${DIST}_${COLLECTION}-build slc6-extras
        koji add-external-repo --tag=${DIST}_${COLLECTION}-build slc6-cernonly

        koji add-group ${DIST}_${COLLECTION}-build build
        koji add-group ${DIST}_${COLLECTION}-build srpm-build
        koji add-group-pkg ${DIST}_${COLLECTION}-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which ${COLLECTION}-build cern-koji-addons tar scl-utils-build
        koji add-group-pkg ${DIST}_${COLLECTION}-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils ${COLLECTION}-build cern-koji-addons tar scl-utils-build
        koji add-tag-inheritance --priority 1 ${DIST}_${COLLECTION}-build ${DIST}-testing
        koji add-tag-inheritance --priority 2 ${DIST}_${COLLECTION}-build $BASE_DIST-build
done
