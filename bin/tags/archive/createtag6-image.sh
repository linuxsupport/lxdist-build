#!/bin/bash

DIST="slc6-image-6x"

koji add-tag --arches "i686 x86_64" $DIST-build

koji add-group $DIST-build build

koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which cern-koji-addons tar



koji add-external-repo --tag=$DIST-build slc6-extras
koji add-external-repo --tag=$DIST-build slc6-updates
koji add-external-repo --tag=$DIST-build slc6-os

koji add-tag $DIST

koji add-target $DIST $DIST-build  $DIST
