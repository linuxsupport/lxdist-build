#!/bin/bash

TAG="dbclients8al"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="al8.cern"

### You probably don't need to change anything below this
source createtag8al.sh

# Needed for https://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL8/oracle/instantclient/x86_64/
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient -p 5 --mode=bare
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient21 -p 6 --mode=bare --arch x86_64
