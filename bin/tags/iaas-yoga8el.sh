#!/bin/bash

TAG="iaas-yoga8el"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
#DISTTAG=""
CLOUD="yoga"

### You probably don't need to change anything below this
source createtag8el.sh

$KOJI edit-tag $TAG-testing -x mock.yum.module_hotfixes=1
