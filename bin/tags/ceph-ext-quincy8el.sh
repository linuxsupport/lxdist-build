#!/bin/bash

TAG="ceph-ext-quincy8el"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="el8"

### You probably don't need to change anything below this
source createtag8el.sh

# Enable a module
$KOJI edit-tag $TAG-build -x mock.module_setup_commands='[["enable", "subversion-devel"]]'
