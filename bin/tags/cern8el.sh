#!/bin/bash

TAG="cern8el"
ARCHES="x86_64 aarch64"
EPEL=1
TESTING=1
DISTTAG="rh8.cern"

### You probably don't need to change anything below this
PARENT="buildsys8el-stable"
PROFILE="koji" # change it for the test instance
KOJI="/usr/bin/koji -p $PROFILE"

$KOJI remove-tag $TAG-build

$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-build
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-testing
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-qa
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-stable

if [[ $DISTTAG != "" ]]; then
  $KOJI add-group-pkg $TAG-build build buildsys-macros-$DISTTAG
  $KOJI add-group-pkg $TAG-build srpm-build buildsys-macros-$DISTTAG
fi

if [[ $EPEL -eq 1 ]]; then
  $KOJI add-tag-inheritance --priority 10 ${TAG}-build epel8
fi

if [[ $TESTING -eq 1 ]]; then
  $KOJI add-external-repo -t $TAG-build rhel8-testing-baseos -p 25 --mode=bare
  $KOJI add-external-repo -t $TAG-build rhel8-testing-appstream -p 35 --mode=bare
  $KOJI add-external-repo -t $TAG-build rhel8-testing-codeready-builder -p 45 --mode=bare
fi

$KOJI edit-tag $TAG-testing --perm="$PERMS"
$KOJI edit-tag $TAG-qa      --perm="$PERMS"
$KOJI edit-tag $TAG-stable  --perm="$PERMS"

$KOJI add-target $TAG $TAG-build $TAG-testing

