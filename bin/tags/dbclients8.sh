#!/bin/bash

TAG="dbclients8"
ARCHES="x86_64"
#PERMS=""
#TESTING=0
#EPEL=0
DISTTAG="el8.cern"

source createtag8.sh

# Needed for https://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL8/oracle/instantclient/x86_64/
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient -p 5 --mode=bare
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient21 -p 6 --mode=bare
