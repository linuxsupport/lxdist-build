#!/bin/bash

DIST="net7_py36"
DISTDEST="net7"

koji remove-tag $DIST-build
koji add-tag --arches "x86_64" $DIST-build

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar cern-koji-addons buildsys-macros-el7 python-rpm-macros gcc gcc-c++ epel-rpm-macros rh-python36-build
koji add-group-pkg $DIST-build srpm-build bash curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-el7 cern-koji-addons python-rpm-macros gcc gcc-c++ epel-rpm-macros rh-python36-build

koji add-external-repo --tag=$DIST-build cc7-cernonly
koji add-external-repo --tag=$DIST-build cc7-cern
koji add-external-repo --tag=$DIST-build cc7-extras
koji add-external-repo --tag=$DIST-build cc7-updates
koji add-external-repo --tag=$DIST-build cc7-os
koji add-external-repo --tag=$DIST-build cc7-sclo-rh
koji add-external-repo --tag=$DIST-build buildsys7

koji add-tag $DISTDEST-testing
koji add-tag $DISTDEST-qa
koji add-tag $DISTDEST-stable

koji add-tag-inheritance --priority 1 $DIST-build $DISTDEST-testing

koji add-target $DIST $DIST-build $DISTDEST-testing
