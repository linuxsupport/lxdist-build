#!/bin/bash
DISTTAG="el7_2"
DIST="cc7-cern-el7_2.cern-biarch"

koji remove-tag $DIST-build
koji add-tag --arches "i686 x86_64" $DIST-build

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils system-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux which xz buildsys-macros-$DISTTAG cern-koji-addons
koji add-group-pkg $DIST-build srpm-build bash bzip2 coreutils cpio diffutils system-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux which xz buildsys-macros-$DISTTAG cern-koji-addons

koji add-external-repo --tag=$DIST-build buildlogs-c7.1511.u
koji add-external-repo --tag=$DIST-build buildlogs-c7.1511.00
koji add-external-repo --tag=$DIST-build buildlogs-c7.01.u 
koji add-external-repo --tag=$DIST-build buildlogs-c7.01.00  
koji add-external-repo --tag=$DIST-build buildlogs-updates
koji add-external-repo --tag=$DIST-build buildlogs-c7.00.04  
koji add-external-repo --tag=$DIST-build buildlogs-c7.00.03 
koji add-external-repo --tag=$DIST-build buildlogs-c7.00.02 

koji add-external-repo --tag=$DIST-build buildsys7

koji add-target $DIST $DIST-build cc7-cern-testing

koji add-tag-inheritance $DIST-build cc7-cern-testing
