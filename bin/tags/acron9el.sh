#!/bin/bash

TAG="acron9el"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="rh9.cern"
#CLOUD="train"

### You probably don't need to change anything below this
source createtag9el.sh

# acron{release} needs dependencies openafs{release}-stable
$KOJI add-tag-inheritance --priority 3 ${TAG}-build openafs9el-stable
# We override the default koji behaviour to show all packages of openafs,
# to avoid confusion in openafs packages naming.
$KOJI edit-tag ${TAG}-build -x repo_include_all=True
