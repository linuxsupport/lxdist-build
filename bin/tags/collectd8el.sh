#!/bin/bash

TAG="collectd8el"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="rh8.cern"

### You probably don't need to change anything below this
source createtag8el.sh

# RQF1979659
$KOJI add-tag-inheritance --priority 11 ${TAG}-build batch8el-stable
