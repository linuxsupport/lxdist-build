#!/bin/bash

TAG="monit8al"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="al8.cern"

### You probably don't need to change anything below this
source createtag8al.sh

$KOJI add-external-repo --tag=$TAG-build dl.yarnpkg.com -p 100 --mode=bare
