# This script is to be executed from within the other *9.sh scripts
if [[ "${TAG: -1}" != "9" ]]; then
  echo "Error: The tag name must end with a 9 (ie. 'mytag9')"
  exit 1
fi

ARCHES="${ARCHES:-x86_64 aarch64}"
PERMS="${PERMS:-}"
TESTING="${TESTING:-0}"
EPEL="${EPEL:-0}"
CLOUD="${CLOUD:-}"
DISTTAG="${DISTTAG:-el9}"

PARENT="${PARENT:-centos9-cern}"
PROFILE="${PROFILE:-koji}" # change it for the test instance
KOJI="${KOJI:-/usr/bin/koji -p $PROFILE}"

$KOJI remove-tag ${TAG}-build

$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" ${TAG}-build
$KOJI add-tag --arches "$ARCHES" ${TAG}-testing
if [[ $? -ne 0 ]]; then $KOJI edit-tag --arches "$ARCHES" ${TAG}-testing; fi
$KOJI add-tag --arches "$ARCHES" ${TAG}-qa
if [[ $? -ne 0 ]]; then $KOJI edit-tag --arches "$ARCHES" ${TAG}-qa; fi
$KOJI add-tag --arches "$ARCHES" ${TAG}-stable
if [[ $? -ne 0 ]]; then $KOJI edit-tag --arches "$ARCHES" ${TAG}-stable; fi

if [[ $DISTTAG != "" ]]; then
  $KOJI add-group-pkg ${TAG}-build build buildsys-macros-${DISTTAG}
  $KOJI add-group-pkg ${TAG}-build srpm-build buildsys-macros-${DISTTAG}
fi

if [[ $EPEL -eq 1 ]]; then
  $KOJI add-tag-inheritance --priority 10 ${TAG}-build epel9s
fi

if [[ $TESTING -eq 1 ]]; then
  $KOJI add-external-repo -t ${TAG}-build cs9-testing-baseos -p 25 --mode=bare
  $KOJI add-external-repo -t ${TAG}-build cs9-testing-appstream -p 35 --mode=bare
  $KOJI add-external-repo -t ${TAG}-build cs9-testing-crb -p 45 --mode=bare
fi

if [[ $CLOUD != "" ]]; then
  $KOJI add-external-repo -t ${TAG}-build cs9-openstack-${CLOUD} -p 15 --mode=bare
  $KOJI add-external-repo -t ${TAG}-build cs9-openstack-${CLOUD}-builddep -p 20 --mode=bare
fi

$KOJI edit-tag ${TAG}-testing --perm="$PERMS"
$KOJI edit-tag ${TAG}-qa      --perm="$PERMS"
$KOJI edit-tag ${TAG}-stable  --perm="$PERMS"

$KOJI add-tag-inheritance --priority 5 ${TAG}-build ${TAG}-testing
$KOJI add-target ${TAG} ${TAG}-build ${TAG}-testing
