#!/bin/bash

TAG="monit9al"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="al9.cern"

### You probably don't need to change anything below this
source createtag9al.sh

$KOJI add-external-repo --tag=$TAG-build dl.yarnpkg.com -p 100 --mode=bare
