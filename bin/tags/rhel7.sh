#!/bin/bash

DIST="rhel7"

koji remove-tag $DIST-build
koji add-tag --arches "x86_64" $DIST-build

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
# Please note that macro is forced to el7.
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils system-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux which xz buildsys-macros-el7.cern cern-koji-addons
# Please note that macro is forced to el7.
koji add-group-pkg $DIST-build srpm-build bash bzip2 coreutils cpio diffutils system-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux which xz buildsys-macros-el7.cern cern-koji-addons

koji add-external-repo --tag=$DIST-build rhel7-os
koji add-external-repo --tag=$DIST-build buildsys7

koji add-target $DIST $DIST-build cc7-cernonly-testing

