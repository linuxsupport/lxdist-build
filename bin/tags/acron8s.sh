#!/bin/bash

TAG="acron8s"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
EPEL=1
#DISTTAG=""
#CLOUD="train"

### You probably don't need to change anything below this
source createtag8s.sh

# acron{release} needs dependencies openafs{release}-stable
$KOJI add-tag-inheritance --priority 3 ${TAG}-build openafs8s-stable
# We override the default koji behaviour to show all packages of openafs,
# to avoid confusion in openafs packages naming.
$KOJI edit-tag ${TAG}-build -x repo_include_all=True
