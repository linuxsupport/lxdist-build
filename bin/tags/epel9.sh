#!/bin/bash

TAG="epel9"

koji add-tag --arches "x86_64 aarch64" $TAG
koji add-external-repo --tag=$TAG epel9 -p 60 --mode=bare

