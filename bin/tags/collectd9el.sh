#!/bin/bash

TAG="collectd9el"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="rh9.cern"

### You probably don't need to change anything below this
source createtag9el.sh

# RQF1979659
$KOJI add-tag-inheritance --priority 11 ${TAG}-build batch9el-stable
