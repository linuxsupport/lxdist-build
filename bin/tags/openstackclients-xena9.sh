#!/bin/bash
TAG="openstackclients-xena9"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
EPEL=1
#DISTTAG=""
CLOUD="xena"

### You probably don't need to change anything below this
source createtag9.sh

$KOJI edit-tag $TAG-testing -x mock.yum.module_hotfixes=1
