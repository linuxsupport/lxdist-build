#!/bin/bash
TAG="openstackclients-ussuri8s"
#ARCHES="x86_64"
#PERMS=""
#TESTING=0
EPEL=1
#DISTTAG=""
CLOUD="ussuri"

### You probably don't need to change anything below this
source createtag8s.sh

$KOJI edit-tag $TAG-testing -x mock.yum.module_hotfixes=1
