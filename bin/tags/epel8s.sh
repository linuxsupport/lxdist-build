#!/bin/bash

TAG="epel8s"

koji add-tag --arches "x86_64 aarch64" $TAG
koji add-external-repo --tag=$TAG epel-next8s -p 59 --mode=bare
koji add-external-repo --tag=$TAG epel8s -p 60 --mode=bare

