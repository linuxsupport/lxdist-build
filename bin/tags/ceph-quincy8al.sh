#!/bin/bash

TAG="ceph-quincy8al"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
#DISTTAG="al8.cern"

### You probably don't need to change anything below this
source createtag8al.sh

# Enable a module
$KOJI edit-tag $TAG-build -x mock.module_setup_commands='[["enable", "subversion-devel"]]'
