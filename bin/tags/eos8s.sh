#!/bin/bash

TAG="eos8s"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
EPEL=1
#DISTTAG=""

### You probably don't need to change anything below this
source createtag8s.sh

$KOJI add-tag-inheritance --priority 11 ${TAG}-build ceph-client8s-stable

$KOJI add-tag --arches "$ARCHES" ${TAG}-desktop
$KOJI edit-tag ${TAG}-desktop      --perm="$PERMS"
