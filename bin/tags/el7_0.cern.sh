#!/bin/bash

DIST="el7_0.cern"

koji add-tag --arches "x86_64" $DIST-build
koji add-target cc7-cern-$DIST $DIST-build cc7-cern-testing
koji add-target cc7-cernonly-$DIST $DIST-build cc7-cern-testing

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-$DIST

koji add-external-repo --tag=$DIST-build cc7-os
koji add-external-repo --tag=$DIST-build cc7-updates
koji add-external-repo --tag=$DIST-build cc7-extras
koji add-external-repo --tag=$DIST-build buildsys7
