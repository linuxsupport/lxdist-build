#!/bin/bash

TAG="iaas-antelope9al"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
#DISTTAG=""
CLOUD="antelope"

### You probably don't need to change anything below this
source createtag9al.sh

$KOJI edit-tag $TAG-testing -x mock.yum.module_hotfixes=1
$KOJI add-external-repo -t ${TAG}-build cs9-sig-nfv-openvswitch -p 30 --mode=bare