#!/bin/bash

TAG="openafs9el"
#ARCHES="x86_64 aarch64"
TESTING=1
#EPEL=0
DISTTAG="rh9.cern"

### You probably don't need to change anything below this
source createtag9el.sh

$KOJI add-external-repo -t ${TAG}-build rhel9-latest-baseos -p 20 --mode=bare
$KOJI add-external-repo -t ${TAG}-build rhel9-latest-appstream -p 30 --mode=bare
# As we may need dependencies apart from the 'latest' build,
# we add -stable to ensure that we have everything
$KOJI add-tag-inheritance --priority 3 ${TAG}-build ${TAG}-stable
# However, as kmod-openafs is built from the 'openafs' package, it's often the
# case that koji only sees the latest kmod package. We we may expect to see
# other packages that are also built from 'openafs' (such as openafs-devel)
# We can override the default koji behaviour to show all packages, which avoids
# this unfortunate confusion
$KOJI edit-tag ${TAG}-build -x repo_include_all=True
