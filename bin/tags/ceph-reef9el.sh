#!/bin/bash

TAG="ceph-reef9el"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
#DISTTAG="rh9.cern"

### You probably don't need to change anything below this
source createtag9el.sh

# Copr repos are screwed up and ship two different noarch packages for each arch.
# This means you can't add a single external repo to the tag because there will be
# two copies of each noarch with different signatures and koji will get confused.
# So, the workaround is to add two external repos, one for each arch.
#$KOJI add-external-repo --tag=$TAG-build ceph-el9-copr-x86_64  --mode=bare --arch=x86_64
#$KOJI add-external-repo --tag=$TAG-build ceph-el9-copr-aarch64 --mode=bare --arch=aarch64
