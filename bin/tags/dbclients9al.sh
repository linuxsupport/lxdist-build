#!/bin/bash

TAG="dbclients9al"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="al9.cern"

### You probably don't need to change anything below this
source createtag9al.sh

# Needed for https://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL9/oracle/instantclient/x86_64/
$KOJI add-external-repo -t ${TAG}-build oracle-linux9-instantclient -p 5 --mode=bare --arch x86_64
