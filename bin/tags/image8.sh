#!/bin/bash

TAG="c8-image-8x"

koji add-tag --arches "x86_64 aarch64" $TAG-build

koji add-group $TAG-build build
koji add-group $TAG-build srpm-build
koji add-group-pkg $TAG-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar cern-koji-addons buildsys-macros-el8
koji add-group-pkg $TAG-build srpm-build bash curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-el8 cern-koji-addons

koji add-external-repo --tag=$TAG-build c8-cern -p 20 --mode=bare
koji add-external-repo --tag=$TAG-build c8-baseos -p 30 --mode=bare
koji add-external-repo --tag=$TAG-build c8-appstream -p 40 --mode=bare
koji add-external-repo --tag=$TAG-build c8-powertools -p 50 --mode=bare

koji add-tag $TAG

koji add-target $TAG $TAG-build  $TAG
