#!/bin/bash

TAG="eos8el"
#ARCHES="x86_64 aarch64"
#TESTING=0
EPEL=1
DISTTAG="rh8.cern"

### You probably don't need to change anything below this
source createtag8el.sh

$KOJI add-tag-inheritance --priority 11 ${TAG}-build ceph-client8s-stable

$KOJI add-tag --arches "$ARCHES" ${TAG}-desktop
