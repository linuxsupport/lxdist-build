#!/bin/bash

TAG="db8s"
#ARCHES="x86_64 aarch64"
#PERMS=""
TESTING=1
EPEL=1
DISTTAG="el8s.cern"

### You probably don't need to change anything below this
source createtag8s.sh
$KOJI add-external-repo -t ${TAG}-build cs8-latest-baseos -p 20 --mode=bare
# RQF1805989 add instantclient repo and epel
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient -p 21 --mode=bare
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient21 -p 22 --mode=bare --arch x86_64
