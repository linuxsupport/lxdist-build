#!/bin/bash

TAG="el7.cern"
DIST="cc7-cr-el7.cern-biarch"
#cc7-cr-el7.cern-biarch

koji remove-tag $DIST-build

koji add-tag --arches "x86_64 i686" $DIST-build

koji add-target $DIST $DIST-build cc7-cern-testing

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar buildsys-macros-$TAG
koji add-group-pkg $DIST-build srpm-build bash buildsys-macros curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-$TAG

koji add-external-repo --tag=$DIST-build buildlogs-c7.1611.u
koji add-external-repo --tag=$DIST-build buildlogs-c7.1611.01
koji add-external-repo --tag=$DIST-build buildlogs-c7.1611.00
koji add-external-repo --tag=$DIST-build cc7-updates
koji add-external-repo --tag=$DIST-build cc7-extras
koji add-external-repo --tag=$DIST-build cc7-os
koji add-external-repo --tag=$DIST-build buildsys7

koji add-tag-inheritance --priority 5 $DIST-build cc7-cern-testing

