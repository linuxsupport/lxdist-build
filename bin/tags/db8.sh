#!/bin/bash

TAG="db8"
#ARCHES="x86_64"
#PERMS=""
#TESTING=0
EPEL=1
#DISTTAG=""

### You probably don't need to change anything below this
source createtag8.sh

# add the oracle repository to the build: RQF1732003
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient -p 5 --mode=bare
$KOJI add-external-repo -t ${TAG}-build oracle-linux8-instantclient21 -p 6 --mode=bare
