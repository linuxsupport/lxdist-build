#!/bin/bash

MASTER=137.138.144.47
LOG="/mnt/data2/log/lxdist-execute-mash.log"
TMP="mktemp /tmp/"
RSYNC="/usr/bin/rsync -v -rlptD"
RSYNC_ARGS=""
SRC="/mnt/data2/koji/mash/repo"
DEST="/mnt/data1/dist/internal/repos"
NOW=`date "+%Y-%m-%d %H:%M:%S"`

[ -f /etc/lxsoft.conf ] && . /etc/lxsoft.conf

[ `/sbin/ip addr | grep -c $MASTER` -ne 1 ] && echo "This host (`hostname`) is not MASTER ($MASTER). Exit." &&  exit 0

echo "[$NOW] Starting mash..." &>> $LOG

pidfile=/mnt/data2/koji/mash/mash-run.pid
if [ -e $pidfile ]; then
    pid=`cat $pidfile`
    if kill -0 &> /dev/null $pid; then
        echo "Mash is already running"
        exit 1
    else
        rm $pidfile &>/dev/null
    fi
fi
echo $$ > $pidfile

for REPO in `ls $SRC | grep -v "updates"`
do
    echo " -> generating $REPO" &>> $LOG
    /usr/bin/mash --no-delta -c /mnt/data2/etc/mash/mash.conf $REPO -o /mnt/data2/koji/mash/repo -p /mnt/data2/koji/mash/repo &>> $LOG
    # RSYNC /mnt/data1/dist/internal/repos
    [[ ! -d "$DEST/$REPO" ]] && mkdir -p $DEST/$REPO
    if [ "$(( $(date +"%s") - $(stat -c "%Y" $DEST/$REPO) ))" -gt "14400" ]
    then
        echo " -> syncing $REPO" &>> $LOG
        $RSYNC $RSYNC_ARGS $SRC/$REPO $DEST &>> $LOG
    else
    	echo " -> skipping $REPO ; too new" &>> $LOG
    fi
    #[[ ! -f "$DEST/$REPO.repo" ]] && cp /mnt/data2/koji/bin/template.repo $DEST/$REPO.repo && sed -i "s/XXXXX/$REPO/g" $DEST/$REPO.repo
done

rm $pidfile
echo "[`date "+%Y-%m-%d %H:%M:%S"`] Stopping mash..." &>> $LOG
