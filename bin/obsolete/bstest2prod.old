#!/bin/bash

usage()
{
cat << EOF
usage: $0 [packagename]

If no argument, display list of all packages available in testing.

Otherwise it promotes [packagename] to the production repository.

OPTIONS:
   -a 		     All distributions, All repositories
   -d dist           Distribution: slc5 slc6 cc7
   -r repo           Repo: updates mrg ev devtoolset rhcommon scl jbeap sclo virt cloud
   -v version	     Full CentOS Version (Default to $CENTOS_VERSION)
EOF
}

BIN="/mnt/data2/home/build/bin"

#source $BIN/koji-helpers.sh
source $BIN/utils.sh
source $BIN/config.sh

BASE="/mnt/data2/home/build/packages"
CONF="$BASE/configs"
INCOMING="$BASE/incoming"
RESULTS="$BASE/results"
LOGS="$BASE/logs"
BUILD=""
SYNC=""
DRYRUN=""
DISTS=""
REPOS=""
DATE=`date +%Y%m%d`
optiond=false
optionr=false
optionra=false
optiona=false
optionn=false

while getopts “:hnav:d:r:” OPTION
do
	case $OPTION in
        h)
             usage
             exit 0
             ;;
	d)
	     optiond=true
             DISTS=$OPTARG
             ;;
	n)
	     optionn=true
             ;;
	a)
	     optiona=true
             DISTS=`ls $INCOMING | grep -v slc7`
             ;;
	r)
	     optionr=true
	     if [ "x$OPTARG" == "xall" ]
             then
                optionra=true
             fi
             REPOS=$OPTARG
             ;;
	v)
             optionv=true
             CENTOS_VERSION="$OPTARG"
             ;;
	?)
             exit 0
             ;;
        :)
             echo "Option -$OPTARG requires an argument."
             exit 1
             ;;
        esac
done

shift $(($OPTIND - 1))

if ! ( (( $optiona ) || ( $optiond && $optionr )) )
then
        usage
        exit 0
fi

for D in $DISTS
do
        ls $INCOMING/$D &>/dev/null
        if [ $? -eq 0 ]
        then
                continue
        else
                echo -e $ROUGE "*** Distribution '$D' doesn't exist"
                tput sgr0
                echo "   -> Available distribution:"
                ls $INCOMING
                exit 1
        fi
done

# Create log dir
mkdir -p $LOGS/test2prod/

trap "echo 'NO CTRL-C please'" SIGINT SIGTERM

# Check user rights
#user_can_build
#if [ $? -gt 0 ]
#then
#        exit 1
#fi

if [ -z "$1" ]
then
	DRYRUN=1
fi

# Main loop
for DIST in $DISTS
do
	if ( $optiona || $optionra )
	then
		#FIXME when ready for -a
		if [ "x$DIST" != "xcc7" ]
		then
	        	REPOS="updates mrg rhcommon scl ev"
		else
	        	REPOS=`ls $INCOMING/$DIST`
			REPOS="$REPOS updates extras centosplus cr fasttrack sclo virt cloud"
		fi
	fi
	for REPO in $REPOS
        do

          RPMPATHS="Packages"

          if [ $DIST == "cc7" ] 
          then
           case "$REPO" in
               sclo)
                  RPMPATHS="rh/devassist09 rh/devtoolset-3 rh/git19 rh/httpd24 rh/maven30 rh/mariadb55 rh/mongodb24 rh/mysql55 rh/nginx16 rh/nodejs010 rh/passenger40 rh/perl516 rh/php54 rh/php55 rh/postgresql92 rh/rh-java-common rh/python27 rh/python33 rh/python34 rh/rh-mariadb100 rh/rh-mongodb26 rh/rh-mysql56 rh/rh-nginx18 rh/rh-perl520 rh/rh-php56 rh/rh-postgresql94 rh/rh-varnish4 rh/ror40 rh/ror41 rh/ruby22 rh/ruby193 rh/ruby200 rh/thermostat1 rh/v8314 sclo/vagrant1"
                  ;;
               sclo-testing)
                  RPMPATHS="rh/devassist09 rh/devtoolset-3 rh/git19 rh/httpd24 rh/mariadb55 rh/mongodb24 rh/mysql55 rh/nginx16 rh/nodejs010 rh/passenger40 rh/perl516 rh/php54 rh/php55 rh/postgresql92 rh/python27 rh/python33 rh/python34 rh/rh-mariadb100 rh/rh-mongodb26 rh/rh-mysql56 rh/rh-nginx18 rh/rh-perl520 rh/rh-php56 rh/rh-postgresql94 rh/rh-varnish4 rh/ror40 rh/ror41 rh/ruby22 rh/ruby193 rh/ruby200 rh/thermostat1 rh/v8314 sclo/vagrant1"
                  ;;
               virt)
                  RPMPATHS="kvm-common"
                  ;;
               virt-testing)
                  RPMPATHS="kvm-common"
                  ;;
               cloud)
                  RPMPATHS="openstack-kilo openstack-kilo/common openstack-liberty openstack-liberty/common"
                  ;;
               cloud-testing)
                  RPMPATHS="openstack-kilo openstack-kilo/common openstack-liberty openstack-liberty/common"
                  ;;
               *)
                  RPMPATHS="Packages"
                  ;;
            esac
          fi

          for RPMPATH in $RPMPATHS
          do

		FILELIST=""
		SOURCES_FILELIST=""
		DEBUGX86_FILELIST=""
		DEBUGX64_FILELIST=""
		if [ "x$DIST" != "xcc7" ]
		then
			[[ "x$REPO" == "xcernonly" || "x$REPO" == "xextras" ]] && echo " *** Skipping special repository $DIST/$REPO. Use: bstest2prod -d $DIST -r updates " && continue
		fi
		ALL=""
		DEST_TESTING=""
		DEST_PROD=""
		SDEST=""
		DDEST=""
		BDEST=""
		UPSTREAM=false
		[ -f $CONF/$DIST.$REPO.cfg ] && source $CONF/$DIST.$REPO.cfg
		if [ "x$DEST_TESTING" == "x" ]
		then
			echo -e $ROUGE  "*** Skipping $DIST/$REPO. Please configure \$DEST_TESTING in $CONF/$DIST.$REPO.cfg" 
			tput sgr0
			continue
		fi
		if [ "x$DIST" == "xcc7" ]
		then
			FILELIST=`find $DEST_TESTING/x86_64/$RPMPATH/$1*rpm -nowarn -type f -printf "%p\n" 2>/dev/null`
# no sources or debuginfo on virt for now... TMP
			if [ $REPO == "virt" ] || [ $REPO == "virt-testing" ] || [ $REPO == "cloud" ] || [ $REPO == "cloud-testing" ]
                        then
			SOURCES_FILELIST=""
			DEBUGX86_FILELIST=""
			DEBUGX64_FILELIST=""
			else
			SOURCES_FILELIST=`find $DEST_TESTING/Sources/$RPMPATH/$1*rpm -nowarn -type f -printf "%p\n" 2>/dev/null`
			DEBUGX86_FILELIST=`find $DEST_TESTING/Debug/i386/$RPMPATH/$1*rpm -nowarn -type f -printf "%p\n" 2>/dev/null`
			DEBUGX64_FILELIST=`find $DEST_TESTING/Debug/x86_64/$RPMPATH/$1*rpm -nowarn -type f -printf "%p\n" 2>/dev/null`
			fi

# no sources or debuginfo on virt for now... TMP

			case "$REPO" in
				"updates") UPSTREAM=true
				;;
				"extras") UPSTREAM=true
				;;
				"centosplus") UPSTREAM=true
				;;
				"cr") UPSTREAM=true
				;;
				"fasttrack") UPSTREAM=true
				;;
				"sclo") UPSTREAM=true
				;;
				"virt") UPSTREAM=true
                                ;;
				"cloud") UPSTREAM=true
				;;
			esac
		else
			if [ "x$REPO" == "xupdates-nonpae" ]
                        then
				FILELIST=`find $DEST_TESTING/{RPMS,SRPMS,debug}/$1*rpm -nowarn -type f -printf "%p\n" 2>/dev/null`
			else
				FILELIST=`find $DEST_TESTING/{x86_64,i386}/{RPMS,SRPMS,debug}/$1*rpm -nowarn -type f -printf "%p\n" 2>/dev/null`
			fi
		fi

		if [ $DRYRUN ]
		then
			echo -e $VERT "*** Currently in $DIST/$REPO ($RPMPATH) testing :"
			tput sgr0
			for FILE in $FILELIST
                	do
				wheredoIbelong $FILE $DIST
				if [ "x$REPO" == "xupdates" ]
				then
					if ( $UPSTREAM )
					then
						[[ ! -e $DEST_PROD/x86_64/Packages/`basename $FILE` ]] && ALL="$ALL\n$NAME-$VERSION-$RELEASE->$DIST/$THERE"
					else
						ALL="$ALL\n$NAME-$VERSION-$RELEASE->$DIST/$THERE"
					fi
				else
					if ( $UPSTREAM )
					then
						[[ ! -e $DEST_PROD/x86_64/Packages/`basename $FILE` ]] && ALL="$ALL\n$NAME-$VERSION-$RELEASE->$DIST/$REPO"
					else
						ALL="$ALL\n$NAME-$VERSION-$RELEASE->$DIST/$REPO"
					fi
					
				fi
			done
			for FILE in $SOURCES_FILELIST 
                	do
				wheredoIbelong $FILE $DIST
				ALL="$ALL\n$NAME-$VERSION-$RELEASE->$DIST/$REPO"
			done	
			if [[ "x$REPO" =~ "^xcern" ]] 
			then
			for FILE in $DEBUGX86_FILELIST 
                	do
				wheredoIbelong $FILE $DIST
				ALL="$ALL\n$NAME-$VERSION-$RELEASE->$DIST/$REPO"
			done	
			for FILE in $DEBUGX64_FILELIST 
                	do
				wheredoIbelong $FILE $DIST
				ALL="$ALL\n$NAME-$VERSION-$RELEASE->$DIST/$REPO"
			done	
			fi
			if [ $ALL ] 
			then
				printf $ALL|sort|uniq
			else
				echo " -> None"
			fi
			continue
		else
			if [ "x$DIST" == "xcc7" ]
			then
			    echo -e $VERT "*** Processing $DIST-$REPO ($RPMPATH) :"
			    tput sgr0
			    echo " -> Logging to $LOGS/test2prod/$DIST.$REPO.$DATE.log"
			    tput sgr0
			    BDEST="/mnt/data2/dist/cern/centos/$CENTOS_VERSION/$REPO/x86_64/$RPMPATH"
			    SDEST="/mnt/data2/dist/cern/centos/$CENTOS_VERSION/$REPO/Sources/$RPMPATH"
			    DX86DEST="/mnt/data2/dist/cern/centos/$CENTOS_VERSION/$REPO/Debug/i386/$RPMPATH"
			    DX64DEST="/mnt/data2/dist/cern/centos/$CENTOS_VERSION/$REPO/Debug/x86_64/$RPMPATH"
			    [[ ! -e $BDEST ]] && echo "Missing destination: $BDEST. No file copied." && continue
                        # no sources for now for virt ..
			if [ $REPO != "virt" ] && [ $REPO != "cloud" ]
                        then
			    [[ ! -e $SDEST ]] && echo "Missing destination: $SDEST. No file copied." && continue
                        fi
			    if [[ "x$FILELIST" == "x" && "x$SOURCES_FILELIST" == "x" && "x$DX86DEST_FILELIST" == "x" && "x$DX64DEST_FILELIST" == "x" ]]
			    then
				echo " -> No testing package found for $DIST-$REPO."
			    fi 
			    for FILE in $FILELIST
                            do
				mv $FILE $BDEST	
        			if [ $? -gt "0" ]
				then
					echo -e $ROUGE "command: mv $FILE $BDEST failed. Something went terribly wrong. space issue? exiting..."
			    		tput sgr0
					exit 1
				else
					echo " -> moved `basename $FILE`"
					echo "    to     $BDEST"
					echo $FILE >> $LOGS/test2prod/$DIST.$REPO.$DATE.log
				fi
			    done

			    for FILE in $SOURCES_FILELIST
                            do
				mv $FILE $SDEST	
        			if [ $? -gt "0" ]
				then
					echo -e $ROUGE "command: mv $FILE $SDEST failed. Something went terribly wrong. space issue? exiting..."
			    		tput sgr0
					exit 1
				else
					echo " -> moved `basename $FILE`"
					echo "    to     $SDEST"
					#echo $FILE >> $LOGS/test2prod/$DIST.$REPO.$DATE.log
				fi
			    done
				
			    if [[ "x$REPO" =~ "^xcern" ]] 
			    then
			    [[ ! -e $DX86DEST ]] && echo "Missing destination: $DX86DEST. No file copied." && continue
			    [[ ! -e $DX64DEST ]] && echo "Missing destination: $DX64DEST. No file copied." && continue
			    for FILE in $DEBUGX86_FILELIST
                            do
				
				mv $FILE $DX86DEST
        			if [ $? -gt "0" ]
				then
					echo -e $ROUGE "command: mv $FILE $DX86DEST failed. Something went terribly wrong. space issue? exiting..."
			    		tput sgr0
					exit 1
				else
					echo " -> moved `basename $FILE`"
					echo "    to     $DX86DEST"
					#echo $FILE >> $LOGS/test2prod/$DIST.$REPO.$DATE.log
				fi
			    done

			    for FILE in $DEBUGX64_FILELIST
                            do
				mv $FILE $DX64DEST
        			if [ $? -gt "0" ]
				then
					echo -e $ROUGE "command: mv $FILE $DX64DEST failed. Something went terribly wrong. space issue? exiting..."
			    		tput sgr0
					exit 1
				else
					echo " -> moved `basename $FILE`"
					echo "    to     $DX64DEST"
					#echo $FILE >> $LOGS/test2prod/$DIST.$REPO.$DATE.log
				fi
			    done
			    fi
			    continue
			fi
			tput sgr0
			if [ "x$REPO" == "xupdates" ]
			then
			    echo " *** Logging: $LOGS/test2prod/$DIST.$DATE.log"
			    tput sgr0
			    for FILE in $FILELIST
                            do
                                if [ -e "$FILE" ]
				then
					wheredoIbelong $FILE $DIST
				else
					continue
				fi
				#FIXME bsmailuser-ng need second line only
				echo $FILE >> $LOGS/test2prod/$DIST.$DATE.log
				echo $FILE >> $LOGS/test2prod/$DIST.$THERE.$DATE.log
				case "$FILE" in
					*/x86_64/*)
						ARCH="x86_64"
						;;
					*/i386/*)
						ARCH="i386"
						;;
				esac
                                case $THERE in
                                        "updates")
						SDEST="/mnt/data2/dist/cern/${DIST}X/updates/SRPMS"
						BDEST="/mnt/data2/dist/cern/${DIST}X/updates/$ARCH/RPMS"
						DDEST="/mnt/data2/dist/cern/${DIST}X/updates/$ARCH/debug"
                                                ;;
                                        "extras")
						SDEST="/mnt/data2/dist/cern/${DIST}X/i386/extras/SRPMS/"
						BDEST="/mnt/data2/dist/cern/${DIST}X/$ARCH/extras/RPMS"
						DDEST="/mnt/data2/dist/cern/${DIST}X/$ARCH/extras/debug"
                                                ;;
                                        "cernonly")
						SDEST="/mnt/data2/dist/onlycern/${DIST}X/SRPMS/"
						BDEST="/mnt/data2/dist/onlycern/${DIST}X/$ARCH/RPMS"
						DDEST="/mnt/data2/dist/onlycern/${DIST}X/$ARCH/debug"
                                                ;;
                                esac
				[[ ! -e $SDEST ]] && echo "Missing destination: $SDEST. No file copied." && continue
				[[ ! -e $BDEST ]] && echo "Missing destination: $BDEST. No file copied." && continue
				[[ ! -e $DDEST ]] && echo "Missing destination: $DDEST. No file copied." && continue
				echo -e $CYAN "*** Moving  `basename $FILE` from $DIST-$REPO-testing to $DIST-$THERE..."
				tput sgr0
				if [ $ISSRC -eq 1 ] 
				then
					[ -e "$FILE" ] && mv $FILE $SDEST
				else
					if [[ "$FILE" =~ "-debuginfo-" ]]
					then
						[ -e "$FILE" ] && mv $FILE $DDEST	
					else
						[ -e "$FILE" ] && mv $FILE $BDEST
					fi
				fi
                          done
			else
				[[ "x$DEST_PROD" == "x" ]] && echo -e $ROUGE  "*** Skipping $DIST/$REPO. Please configure \$DEST_PROD in $CONF/$DIST.$REPO.cfg" && tput sgr0 && continue

				if [ "x$REPO" == "xupdates-nonpae" ]
                        	then
					[[ ! -e $DEST_PROD/SRPMS ]] && echo "Missing filesystem destination: $DEST_PROD/SRPMS. No file copied." && continue
					[[ ! -e $DEST_PROD/RPMS ]] && echo "Missing filesystem destination: $DEST_PROD/RPMS. No file copied." && continue
					[[ ! -e $DEST_PROD/debug ]] && echo "Missing filesystem destination: $DEST_PROD/debug. No file copied." && continue
					
				else
					[[ ! -e $DEST_PROD/SRPMS ]] && echo "Missing filesystem destination: $DEST_PROD/SRPMS. No file copied." && continue
                                	[[ ! -e $DEST_PROD/x86_64 ]] && echo "Missing filesystem destination: $DEST_PROD/x86_64. No file copied." && continue
                                	[[ ! -e $DEST_PROD/x86_64/debug ]] && echo "Missing filesystem destination: $DEST_PROD/x86_64/debug. No file copied." && continue
                                	[[ ! -e $DEST_PROD/i386 ]] && echo "Missing filesystem destination: $DEST_PROD/i386. No file copied." && continue
                                	[[ ! -e $DEST_PROD/i386/debug ]] && echo "Missing filesystem destination: $DEST_PROD/i386/debug. No file copied." && continue
				fi
				echo " *** Logging: $LOGS/test2prod/$DIST.$REPO.$DATE.log"
				tput sgr0
				for FILE in $FILELIST
                            	do
					ARCH=""
					SDEST=""
                                        BDEST=""
                                        DDEST=""
                                	echo $FILE >> $LOGS/test2prod/$DIST.$REPO.$DATE.log
					case "$FILE" in
                                        	*/x86_64/*)
                                                	ARCH="x86_64"
                                                	;;
                                        	*/i386/*)
                                                	ARCH="i386"
                                                	;;
                                	esac
					SDEST="$DEST_PROD/SRPMS"
					if [ "x$REPO" == "xupdates-nonpae" ]
					then
						BDEST="$DEST_PROD/RPMS"
						DDEST="$DEST_PROD/debug"
					else
						BDEST="$DEST_PROD/$ARCH/RPMS"
						DDEST="$DEST_PROD/$ARCH/debug"
					fi
					res=`file -b $FILE | grep "RPM v3.0 src" 2> /dev/null`
        				if [ $? -gt "0" ]
					then
				                ISSRC=0
					else
						ISSRC=1
					fi

                                	if [ $ISSRC -eq 1 ]
                                	then
						echo "- > executing: mv $FILE $SDEST"
                                        	[ -e "$FILE" ] && mv $FILE $SDEST
                                	else
                                        	if [[ "$FILE" =~ "-debuginfo-" ]]
                                        	then
							echo " -> executing: mv $FILE $DDEST"
                                                	[ -e "$FILE" ] && mv $FILE $DDEST
                                        	else
							echo " -> executing: mv $FILE $BDEST"
                                                	[ -e "$FILE" ] && mv $FILE $BDEST
                                        	fi
                                	fi
				done
			fi
	   	fi
          done # RPMPATH in $RPMPATHS
	done
done
