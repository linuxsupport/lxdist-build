#!/bin/bash

TAG="cc7-cern-el7_2.cern-biarch-build"
TAG_MODEL="cc7-cern-el7.cern-biarch-build"

for i in `koji list-external-repos --tag=$TAG --quiet | cut -c4-25`
do 
	koji remove-external-repo $i $TAG
done

for i in `koji list-external-repos --tag=$TAG_MODEL --quiet | cut -c4-25`
do
	koji add-external-repo --tag=$TAG $i
done
