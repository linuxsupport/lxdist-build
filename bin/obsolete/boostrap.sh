#!/bin//bash

KOJI="echo /usr/bin/koji"

PERMS=`koji list-permissions --mine`

for P in $PERMS
do
    [[ $P == 'admin' ]] && ADMIN=true && break
done

if [ "$ADMIN" != true ]
then
    echo "[ERROR] Koji misconfigure/missing admin privilege for bootstraping"
    exit 1
fi

# CC7
$KOJI add-external-repo cc7-updates                 "http://linuxsoft.cern.ch/cern/centos/7/updates/\$arch/"
$KOJI add-external-repo cc7-os                      "http://linuxsoft.cern.ch/cern/centos/7/os/\$arch/"
$KOJI add-external-repo cc7-cern                    "http://linuxsoft.cern.ch/cern/centos/7/cern/\$arch/"
$KOJI add-external-repo cc7-cernonly                "http://linuxsoft.cern.ch/cern/centos/7/cernonly/\$arch/"
$KOJI add-external-repo cc7-extras                  "http://linuxsoft.cern.ch/cern/centos/7/extras/\$arch/"
$KOJI add-external-repo cc7-updates-testing         "http://linuxsoft.cern.ch/cern/centos/7/updates-testing/\$arch/"
$KOJI add-external-repo cc7-cr-testing              "http://linuxsoft.cern.ch/cern/centos/7/cr-testing/\$arch/"
$KOJI add-external-repo cc7-cern-testing            "http://linuxsoft.cern.ch/cern/centos/7/cern-testing/\$arch/"
$KOJI add-external-repo cc72-os                     "http://linuxsoft.cern.ch/cern/centos/7.2/os/\$arch/"
$KOJI add-external-repo cc7-imgbuild-cern           "http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/CERN/"
$KOJI add-external-repo cc72-cern                   "http://linuxsoft.cern.ch/cern/centos/7.2/os/\$arch/CERN/"
$KOJI add-external-repo cc73-cern                   "http://linuxsoft.cern.ch/cern/centos/7.3/os/\$arch/CERN/"
$KOJI add-external-repo cc7-cr                      "http://linuxsoft.cern.ch/cern/centos/7/cr/\$arch/"
$KOJI add-external-repo cc7-sclo-rh                 "http://linuxsoft.cern.ch/cern/centos/7/sclo/\$arch/rh/"
$KOJI add-external-repo cc73-os                     "http://linuxsoft.cern.ch/cern/centos/7.3/os/\$arch/"
$KOJI add-external-repo cc7-cernonly-testing        "http://linuxsoft.cern.ch/cern/centos/7/cernonly-testing/\$arch/"
$KOJI add-external-repo cc7-sclo-sclo               "http://linuxsoft.cern.ch/cern/centos/7/sclo/\$arch/sclo/"

# SLC6
$KOJI add-external-repo slc6-external-repo-cernonly "http://linuxsoft.cern.ch/onlycern/slc6X/\$arch/yum/cernonly/"
$KOJI add-external-repo slc6-cernonly               "http://linuxsoft.cern.ch/onlycern/slc6X/\$arch/yum/cernonly/"
$KOJI add-external-repo slc6-extras                 "http://linuxsoft.cern.ch/cern/slc6X/\$arch/yum/extras/"
$KOJI add-external-repo slc6-os                     "http://linuxsoft.cern.ch/cern/slc6X/\$arch/yum/os/"
$KOJI add-external-repo slc6-updates                "http://linuxsoft.cern.ch/cern/slc6X/\$arch/yum/updates/"
$KOJI add-external-repo slc6-nonpae                 "http://linuxsoft.cern.ch/cern/slc6X/i386nonpae/yum/updates-nonpae/"
$KOJI add-external-repo slc6-external-repo-updates  "http://linuxsoft.cern.ch/cern/slc6X/\$arch/yum/updates/"
$KOJI add-external-repo slc6-sclo-rh                "http://linuxsoft.cern.ch/cern/centos/6/sclo/\$arch/rh/"
$KOJI add-external-repo slc6-rhcommon               "http://linuxsoft.cern.ch/cern/rhcommon/slc6X/\$arch/RPMS/"
$KOJI add-external-repo slc6-sclo-sclo              "http://linuxsoft.cern.ch/cern/centos/6/sclo/\$arch/sclo/"
$KOJI add-external-repo slc6-testing                "http://linuxsoft.cern.ch/cern/slc6X/updates/testing/\$arch/RPMS/"
$KOJI add-external-repo slc6-external-repo-os       "http://linuxsoft.cern.ch/cern/slc6X/\$arch/yum/os/"

# SLC5
$KOJI add-external-repo slc5-extras                 "http://linuxsoft.cern.ch/cern/slc5X/\$arch/yum/extras/"
$KOJI add-external-repo slc5-updates                "http://linuxsoft.cern.ch/cern/slc5X/\$arch/yum/updates/"
$KOJI add-external-repo slc5-os                     "http://linuxsoft.cern.ch/cern/slc5X/\$arch/yum/os/"
$KOJI add-external-repo slc5-cernonly               "http://linuxsoft.cern.ch/onlycern/slc5X/\$arch/yum/cernonly/"
$KOJI add-external-repo slc5-testing                "http://linuxsoft.cern.ch/cern/slc5X/updates/testing/\$arch/RPMS/"

# Internal koji tools

$KOJI add-external-repo buildsys7                   "http://koji.cern.ch/buildsys/7/"
$KOJI add-external-repo buildsys6                   "http://koji.cern.ch/buildsys/6/"
$KOJI add-external-repo buildsys5                   "http://koji.cern.ch/buildsys/5/"


# Add default tag for repository inheritance

$KOJI add-tag cc7-default-repositories

$KOJI add-external-repo -t cc7-default-repositories cc7-cr       -p 10
$KOJI add-external-repo -t cc7-default-repositories cc7-cernonly -p 15
$KOJI add-external-repo -t cc7-default-repositories cc7-cern     -p 20
$KOJI add-external-repo -t cc7-default-repositories cc7-extras   -p 25
$KOJI add-external-repo -t cc7-default-repositories cc7-updates  -p 30
$KOJI add-external-repo -t cc7-default-repositories cc7-os       -p 35
$KOJI add-external-repo -t cc7-default-repositories buildsys7    -p 40

$KOJI add-tag slc6-default-repositories
$KOJI add-external-repo -t slc6-default-repositories slc6-cernonly -p 10
$KOJI add-external-repo -t slc6-default-repositories slc6-extras   -p 15
$KOJI add-external-repo -t slc6-default-repositories slc6-updates  -p 20
$KOJI add-external-repo -t slc6-default-repositories slc6-os       -p 25
$KOJI add-external-repo -t slc6-default-repositories buildsys6     -p 30

$KOJI add-tag slc5-default-repositories
$KOJI add-external-repo -t slc5-default-repositories slc5-cernonly -p 10
$KOJI add-external-repo -t slc5-default-repositories slc5-extras   -p 15
$KOJI add-external-repo -t slc5-default-repositories slc5-updates  -p 20
$KOJI add-external-repo -t slc5-default-repositories slc5-os       -p 25
$KOJI add-external-repo -t slc5-default-repositories buildsys5     -p 30


# Add default targets slc5 slc6, cc7 for testing

$KOJI add-tag cc7-testing
$KOJI add-tag cc7-qa
$KOJI add-tag cc7-stable
$KOJI add-tag --parent cc7-default-repositories --arches "x86_64" cc7-build
$KOJI add-target cc7 cc7-build cc7-testing
$KOJI add-group cc7-build build
$KOJI add-group cc7-build srpm-build
$KOJI add-group-pkg cc7-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar cern-koji-addons python-rpm-macros gcc gcc-c++
$KOJI add-group-pkg cc7-build srpm-build bash curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar cern-koji-addons python-rpm-macros gcc gcc-c++ 
$KOJI add-tag-inheritance --priority 5 cc7-build cc7-testing

$KOJI add-tag slc6-testing
$KOJI add-tag slc6-qa
$KOJI add-tag slc6-stable
$KOJI add-tag --parent slc6-default-repositories --arches "i686 x86_64" slc6-build
$KOJI add-target slc6 slc6-build slc6-testing
$KOJI add-group slc6-build build
$KOJI add-group slc6-build srpm-build
$KOJI add-group-pkg slc6-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which cern-koji-addons tar
$KOJI add-group-pkg slc6-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils cern-koji-addons tar
$KOJI add-tag-inheritance --priority 5 slc6-build slc6-testing

$KOJI add-tag slc5-testing
$KOJI add-tag slc5-qa
$KOJI add-tag slc5-stable
$KOJI add-tag --parent slc5-default-repositories --arches "i686 x86_64" slc5-build
$KOJI add-target slc5 slc5-build slc5-testing
$KOJI add-group slc5-build build
$KOJI add-group slc5-build srpm-build
$KOJI add-group-pkg slc5-build build bash bzip2 coreutils cpio diffutils redhat-release findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which cern-koji-addons tar buildsys-macros-slc5
$KOJI add-group-pkg slc5-build srpm-build bash buildsys-macros curl cvs redhat-release gnupg make redhat-rpm-config rpm-build shadow-utils cern-koji-addons tar buildsys-macros-slc5
$KOJI add-tag-inheritance --priority 5 slc5-build slc5-testing
