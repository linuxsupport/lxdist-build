#!/bin/bash

usage()
{
cat << EOF
usage: $0 [-n] -k kernel 

This script build packages against specific kernel.

OPTIONS:
   -k kernel    Kernel version e.g: 2.6.32-504.23.4.el6
   -d dist      Distribution - slc6 cc7
   -c           Copy modules to incoming
   -a           Copy modules and zfs tools to incoming
EOF
}

BIN="/mnt/data2/home/build/bin"

source $BIN/koji-helpers.sh
source $BIN/utils.sh
source $BIN/config.sh

MOCKCHAIN="/mnt/data2/home/build/bin/mockchain"
BASE="/mnt/data2/home/build/modules"
CONF="$BASE/configs"
ARCHES="x86_64"
INCOMING="$BASE/incoming/"
LOGS="$BASE/logs"
DESTINATION="/mnt/data2/home/build/packages/incoming"


optionplus=false
optiona=false
optionc=false

while getopts “:hack:d:” OPTION
do
        case $OPTION in
         h)
             usage
             exit 1
             ;;
	 c) 
	     optionc=true
             ;;
	 a) 
	     optionc=true
	     optiona=true
             ;;
	 d) 
	     DIST=$OPTARG
	     ;;
	 k)
             KERNEL=$OPTARG
             if [[ $KERNEL =~ ^plus.* ]]
             then
                echo "Plus kernel detected"
                optionplus=true
                KERNEL=`echo $KERNEL| cut -d "-" -f 2-`
             fi
             if [[ $KERNEL =~ ^[0-9].* ]]
             then
                echo "Valid kernel string" &> /dev/null
             else
                echo "Invalid kernel string, not starting with a number"
                exit 1
             fi
             KERNEL=$OPTARG
             ;;
	?)
             exit 1
             ;;
        :)
             echo "Option -$OPTARG requires an argument."
             exit 1
             ;;
	esac
done

shift $(($OPTIND - 1))

if [[ -z $KERNEL ]]
then
     usage
     exit 1
fi

#RESULTS="$BASE/results/mockchain/$DIST/$KERNEL/"
RESULTS="/var/builder/tmp/mock-chain-build*/results/$DIST-x86_64-$KERNEL/"

if ( $optionc )
then
	if ( $optiona )
	then
		PKGS=`find $RESULTS -name "*rpm" | grep -v src.rpm | grep $KERNEL | sort 2> /dev/null`
	else
		PKGS=`find $RESULTS -name "*rpm" | grep -v src.rpm | grep kmod | grep $KERNEL | sort 2> /dev/null`
	fi
	if [ "x$PKGS" != "x" ]
	then
		echo "The following package will be copied :"
		echo ""
		for P  in $PKGS
		do
			echo `basename $P`
		done
		echo ""
		if [ "x$DIST" != "xcc7" ]
                 then
                   cp -a $PKGS $DESTINATION/$DIST/extras
                   echo "Please run 'bsdownload -d slc6 -r extras -s' to sign the packages"
                 else
                   cp -a $PKGS $DESTINATION/$DIST/cern
		   echo "Please run 'bsdownload -d cc7 -r cern -s' to sign the packages"
                fi
	else
		echo "No package available for kernel $KERNEL"
	fi
	exit 0
fi

if [[ -z $KERNEL ]] || [[ -z $DIST ]]
then
     usage
     exit 1
fi

# Check if DIST has config
if [ ! -f $CONF/$DIST-x86_64-template.cfg ] || [ ! -f $CONF/$DIST-x86_64-template.cfg ]
then
        echo "No configs found"
        echo "$DIST-x86_64-template.cfg and $DIST-x86_64-template.cfg must be created first"
        exit 1
fi

PKGS=`find -L $INCOMING/$DIST/zfs -name "*src.rpm" | grep -v openafs | sort 2> /dev/null`
if [ "x$PKGS" == "x" ]
then
        echo "Nothing to build"
        exit 1
fi

echo ""
echo "### SUMMARY ###"
echo "# KERNEL : $KERNEL"
echo "# DIST   : $DIST  "
echo "# PACKAGES TO BUILD :"
echo "$PKGS"
echo "### END     ###"
echo ""

if [ "x$DIST" != "xcc7" ]
then

	K=`koji list-tagged $DIST-testing-updates | grep $KERNEL`
	if [ $? -gt 0 ]
	then
		echo "Kernel $KERNEL not found or tagged in koji"
		echo "Did you run: koji tag-build $DIST-testing-updates kernel-$KERNEL ?"
		exit 1
	fi

	echo "Regenerating koji $DIST repository..."
	#/usr/bin/mash -c /mnt/data2/etc/mash/mash.conf $DIST-testing-updates -o /mnt/data2/koji/mash/repo -p /mnt/data2/koji/mash/repo >& $LOGS/mashzfs.log
	echo "Repository OK"
	echo ""

fi

#if [ ! -f $CONF/$DIST-i686-$KERNEL.cfg ]
#then
#        cp $CONF/$DIST-i686-template.cfg $CONF/$DIST-i686-$KERNEL.cfg
#        sed -i "s/XXX/$KERNEL/g" $CONF/$DIST-i686-$KERNEL.cfg
#        cp $CONF/$DIST-i686-$KERNEL.cfg $CONF/$DIST-i386-$KERNEL.cfg
#        echo "wrote $CONF/$DIST-i686-$KERNEL.cfg" 
#        echo "wrote $CONF/$DIST-i386-$KERNEL.cfg" 
#fi

if [ ! -f $CONF/$DIST-x86_64-$KERNEL.cfg ]
then
        cp $CONF/$DIST-x86_64-template.cfg $CONF/$DIST-x86_64-$KERNEL.cfg
        sed -i "s/XXX/$KERNEL/g" $CONF/$DIST-x86_64-$KERNEL.cfg
        echo "wrote $CONF/$DIST-x86_64-$KERNEL.cfg"
fi

$MOCKCHAIN -c --recurse -r $DIST-$ARCHES-$KERNEL $PKGS
if [ $? -eq 0 ]
then
	if [ "x$DIST" != "xcc7" ]
        then 
	 echo "[INFO] Please run 'bsbuildzfs -k $KERNEL -c -d slc6' to copy the packages to slc6-extras incoming"
        else
         echo "[INFO] Please run 'bsbuildzfs -k $KERNEL -c -d cc7' to copy the packages to cc7-cern incoming"
        fi
else
	echo "[ERROR] Something went wrong, please try again"
fi
