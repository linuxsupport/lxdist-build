#!/bin/bash

usage()
{
cat << EOF
usage: $0

This script retrieve rpm from Centos and add it to Centos Cern -testing.

OPTIONS:
   -a          	Sync and build all distributions and repositories 
   -d           Distribution: $DISTS
   -n           Dry-run - Only show what will be synced
   -r           Repository: all or 
		       cc7: $REPOS_CC7
                      slc6: $REPOS_SLC6
   -x          	Full CentOS Version (Default to $CENTOS_VERSION)
   -v           Verbose
EOF
}

BIN="/mnt/data2/home/build/bin"

#source $BIN/koji-helpers.sh
#source $BIN/utils.sh
source $BIN/config.sh

EXCLUDE_UPSTREAM_LIST="$BIN/exclude_list_upstream"
PATCHPKGS="kernel-3 kernel-plus-3 nss-3"
CERNCENTOSDIR="/mnt/data2/dist/cern/centos"
BASE="/mnt/data2/home/build/packages"
CONF="$BASE/configs"
INCOMING="$BASE/incoming"
BUILD=1
SYNC=1
NOACTION=0
VERBOSE=0
DISTS="cc7 slc6"
REPOS_SLC6="sclo"
# removed cr
REPOS_CC7="updates extras fasttrack centosplus sclo virt cloud storage"
REPOS=""
RSYNC="/usr/bin/rsync"
RSYNC_OPTS="-rav"
optiond=false
optionr=false
optionra=false
optiona=false
optionn=false

while getopts “:hnavd:r:x:” OPTION
do
	case $OPTION in
        h)
             usage
             exit 1
             ;;
	d)
	     optiond=true
             DISTS=$OPTARG
             ;;
	r)
	     optionr=true
	     if [ "x$OPTARG" == "xall" ]
             then
		optionra=true
             fi
             REPOS=$OPTARG
             ;;
	a)
	     optiona=true
             ;;
	n)
	     NOACTION=1
             ;;
	x)
             CENTOS_VERSION="$OPTARG"
             ;;
        v)   
	     VERBOSE=1 
	     ;;
	?)
             exit 1
             ;;
        :)
             echo "Option -$OPTARG requires an argument."
             exit 1
             ;;
        esac
done

shift $(($OPTIND - 1))

if ! ( (( $optiona ) || ( $optiond && $optionr )) )
then
	usage
        exit 1
fi

for DIST in $DISTS
do
   case $DIST in
      cc7)
         ;;
      slc6)
         ;;
      *)
         echo -e $ROUGE  "*** Unknown dist: $DIST"
         exit 1
   esac
done

#trap "echo 'NO CTRL-C please'" SIGINT SIGTERM


SUMMARYMSG=`mktemp /tmp/bscentos2test.$$.XXXXXX`

for DIST in $DISTS
do
  DATE=`date +%Y%m%d`
  DATE2=`date +%Y%m%d`

  if [ "x$DIST" == "xslc6" ] 
  then
    CENTOS_VER="6"
    CENTOS_VER_SHORT="6"
  else
     CENTOS_VER=$CENTOS_VERSION
     CENTOS_VER_SHORT="7"
  fi

    DIRSRC_BIN="/mnt/data1/dist/centos/$CENTOS_VER"
    [ ! -d $DIRSRC_BIN ] && echo -e $ROUGE "*** no src directory: $DIRSRC_BIN (check -x option). Skipping $DIST for now." && continue
    DIRSRC_SRC="/mnt/data1/dist/mirror/vault.centos.org/centos/$CENTOS_VER_SHORT"
    #DIRSRC_DBGOT="/mnt/data1/dist/mirror/debuginfo.centos.org/centos/$CENTOS_VER_SHORT"
    #DIRSRC_DBGOS="/mnt/data1/dist/mirror/debuginfo.centos.org/$CENTOS_VER_SHORT"
    DIRSRC_DBGOT="/mnt/data1/dist/centos-debuginfo/centos/$CENTOS_VER_SHORT"
    DIRSRC_DBGOS="/mnt/data1/dist/centos-debuginfo/$CENTOS_VER_SHORT"

    DIRDST="/mnt/data2/dist/cern/centos/$CENTOS_VER"

    echo -e $JAUNE "*** Working on CentOS Version: $CENTOS_VER"
        
    if [ ! -d $CERNCENTOSDIR/$CENTOS_VER ]
    then
      echo -e $ROUGE "*** $CERNCENTOSDIR/$CENTOS_VER doesn't exist."
      exit 13
    fi

   case $DIST in
     cc7) 
       if ( $optiona || $optionra )
       then
         REPOS=$REPOS_CC7
       fi
       ;;
     slc6)
       if ( $optiona || $optionra )
       then
         REPOS=$REPOS_SLC6
       fi  
       ;;
     *) 
       echo -e $JAUNE "*** $DIST not supported" && tput sgr0 && exit 1
       ;;
  esac

  [ $NOACTION -eq 1 ] && echo " *** DRYRUN, not syncing" && RSYNC_OPTS="$RSYNC_OPTS -n"


  for REPO in $REPOS
  do
   
    if [ "x$REPO" == "xupdates" ]
    then
      DIRSRC_DBG=$DIRSRC_DBGOS
      DBGREPO=""
    else 
      DIRSRC_DBG=$DIRSRC_DBGOT
      DBGREPO="$REPO/"
    fi
    LOG=$BASE/logs/build2test/$DIST\.$REPO\.$DATE2.log
    TMPLOG=`mktemp /tmp/bscentos2test.$$.XXXXXX`

    echo -e $VERT " *** Syncing CentOS $CENTOS_VER - ${REPO} Source to Cern $DIST $REPO-testing"    

    echo -e $VERT " *** --> Source\c"
    [ $NOACTION -eq 1 ] && echo -e $VERT "(dryrun)"
    tput sgr0
    SKIPLIST=`mktemp /tmp/$$.XXXXXX`
    [ ! -d $DIRDST/$REPO-testing/Sources ] && echo -e $ROUGE "no directory: $DIRDST/$REPO-testing/Sources/" && continue
    find -L $DIRDST/$REPO/Sources -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null 
    find -L $DIRDST/$REPO-testing/Sources -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null
    COMM="$RSYNC  $RSYNC_OPTS --exclude=drpms/ --exclude=repodata/ --exclude="\.*" --exclude="*\.rpm\.*" --exclude= --exclude-from=$SKIPLIST --exclude-from=$EXCLUDE_UPSTREAM_LIST --include='.*\.rpm' $DIRSRC_SRC/$REPO/Source/ $DIRDST/$REPO-testing/Sources/"
    [ $VERBOSE -eq 1 ] && echo "running: $COMM"
    [ $VERBOSE -eq 1 ] && echo "Skip list contains:" && cat $SKIPLIST
    $COMM | grep rpm | tee -a $TMPLOG
    rm $SKIPLIST 

    if [ "x$REPO" == "xsclo" ] || [ "x$REPO" == "xupdates" ] || [ "x$REPO" == "xvirt" ] || [ "x$REPO" == "xcloud" ] || [ "x$REPO" == "xstorage" ]
    then
      echo -n -e $VERT " *** --> Debug\c"
      [ $NOACTION -eq 1 ] && echo -e $VERT "(dryrun)"
      tput sgr0

      if [ "x$REPO" == "xupdates" ]
      then 
       ARCHES="i386 x86_64"
      else
       ARCHES="x86_64"
      fi 
      for ARCH in $ARCHES 
      do
        echo -e $VERT "($ARCH)"
        tput sgr0
        SKIPLIST=`mktemp /tmp/$$.XXXXXX`
        [ ! -d $DIRDST/$REPO-testing/Debug/$ARCH/ ] && echo -e $ROUGE "no directory: $DIRDST/$REPO-testing/Debug/$ARCH/" && continue
        find $DIRDST/$REPO/Debug/$ARCH -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null            
        find $DIRDST/$REPO-testing/Debug/$ARCH -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null
        COMM="$RSYNC  $RSYNC_OPTS --exclude=drpms/ --exclude=repodata/ --exclude="\.*" --exclude="*\.rpm\.*" --exclude-from=$SKIPLIST --exclude-from=$EXCLUDE_UPSTREAM_LIST --include='.*\.rpm' $DIRSRC_DBG/${DBGREPO}${ARCH}/ $DIRDST/$REPO-testing/Debug/$ARCH/"
        [ $VERBOSE -eq 1 ] && echo "running: $COMM"
        [ $VERBOSE -eq 1 ] && echo "Skip list contains:" && cat $SKIPLIST
        $COMM | grep rpm | tee -a $TMPLOG
        rm $SKIPLIST 
      done
    fi  

    echo -e $VERT " *** --> x86_64"
    tput sgr0
    if [ "x$REPO" == "xcloud" ]
    then
	SUBREPOS="openstack-queens openstack-rocky openstack-stein openstack-train"
	for SUBREPO in $SUBREPOS
	do
		echo -e $VERT " *** --> $SUBREPO\c"
        [ $NOACTION -eq 1 ] && echo -e $VERT "(dryrun)"
		tput sgr0
		SKIPLIST=`mktemp /tmp/$$.XXXXXX`
		[ ! -d $DIRDST/$REPO-testing/x86_64/$SUBREPO ] && echo -e $ROUGE "no directory: $DIRDST/$REPO-testing/x86_64/$SUBREPO" && continue
		find $DIRDST/$REPO/x86_64/$SUBREPO -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null
		find $DIRDST/$REPO-testing/x86_64/$SUBREPO -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null
		COMM="$RSYNC $RSYNC_OPTS --exclude=drpms/ --exclude=repodata/ --exclude="\.*" --exclude="*\.rpm\.*" --exclude-from=$SKIPLIST --exclude-from=$EXCLUDE_UPSTREAM_LIST --include='.*\.rpm' $DIRSRC_BIN/$REPO/x86_64/$SUBREPO/ $DIRDST/$REPO-testing/x86_64/$SUBREPO/"
		[ $VERBOSE -eq 1 ] && echo "running: $COMM"
        [ $VERBOSE -eq 1 ] && echo "* Skip list contains:" && cat $SKIPLIST
        $COMM | grep rpm | tee -a $TMPLOG 
		rm $SKIPLIST 
	done
    else
    SKIPLIST=`mktemp /tmp/$$.XXXXXX`
    [ ! -d $DIRDST/$REPO-testing/x86_64/ ] && echo -e $ROUGE "no directory: $DIRDST/$REPO-testing/x86_64/" && continue
    find $DIRDST/$REPO/x86_64 -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null            
    find $DIRDST/$REPO-testing/x86_64 -type f -name '*.rpm' 2>/dev/null | xargs -n1 basename 2>/dev/null | tee >> $SKIPLIST 2>/dev/null
    COMM="$RSYNC $RSYNC_OPTS --exclude=drpms/ --exclude=repodata/ --exclude="\.*" --exclude="*\.rpm\.*" --exclude-from=$SKIPLIST --exclude-from=$EXCLUDE_UPSTREAM_LIST --include='.*\.rpm' $DIRSRC_BIN/$REPO/x86_64/ $DIRDST/$REPO-testing/x86_64/"
    [ $VERBOSE -eq 1 ] && echo "running: $COMM"
    [ $VERBOSE -eq 1 ] && echo "* Skip list contains:" && cat $SKIPLIST
    $COMM | grep rpm | tee -a $TMPLOG 
    rm $SKIPLIST
    
    fi    
    if [ `grep -c rpm  $TMPLOG` -eq 0 ]; then
      echo -e $VERT " *** No new packages\n"           
    else
      if [ $NOACTION -eq 1 ]; then
        echo -e $VERT " *** There would be new packages if -n had not been passed\n"
      else
        for PATCHPKG in $PATCHPKGS
        do
          TODO=`grep -c -h "/$PATCHPKG" $TMPLOG`
          if [ $TODO -gt 0 ]; then
#           echo -e $JAUNE " *** --> Need to patch/build module(s) : $PATCHPKG before regenerating repo:"          
#           PPK=`grep -h "$PATCHPKG" $TMPLOG`
#           echo -e $JAUNE " *** --> $PPK" 
            echo "*** todo: $DIST $REPO-testing - patch/build modules: $PATCHPKG" >> $SUMMARYMSG          
          fi
        done
        echo "*** todo: $DIST $REPO-testing - regenerate" >> $SUMMARYMSG
        for FILE in `cat $TMPLOG`
        do
          echo `basename $FILE` >> $LOG
        done
      fi
    fi
    rm $TMPLOG
  done
done

echo ""
echo -e $VERT " *** Done" 
echo -n -e $JAUNE 
cat $SUMMARYMSG
echo
rm  $SUMMARYMSG
