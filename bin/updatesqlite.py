#!/usr/bin/env python
"""Connection to the SQlite3 db and Queries"""

import sqlite3

class SQLiteConnection:
    """SQLite3 connection"""

    def __init__(self, dbfile):
        self.connection = None
        self.cursor = None
        self.dbfile = dbfile


    def connect_db(self):
        """
        Connect to the sqlite db
        """
        self.db = sqlite3.connect(self.dbfile)
        self.cursor = self.db.cursor()

    def close_connection(self):
        self.cursor.close()
        self.db.close()

class SQLiteQueries(SQLiteConnection):
    """SQLite3 useful queries"""

    def create_tables(self):
        """
        Create channelerrata, errata, misc, erratabugzilla
        erratacves and erratapackages tables
        """
        self.cursor.execute("CREATE TABLE IF NOT EXISTS channelerrata "
                            "( advisory VARCHAR ( 255 ) PRIMARY KEY,"
                            " synopsis VARCHAR ( 255 ), "
                            "issue_date VARCHAR ( 255 ), "
                            "update_date VARCHAR ( 255 ));")

        self.cursor.execute("CREATE TABLE IF NOT EXISTS errata "
                            "( advisory VARCHAR ( 255 ) PRIMARY KEY, "
                            "type VARCHAR ( 255 ), "
                            "description VARCHAR ( 255 ) );")

        self.cursor.execute("CREATE TABLE IF NOT EXISTS misc "
                            "( id INTEGER PRIMARY KEY, "
                            "lastrundate DATE, "
                            "channelname VARCHAR ( 255 ) );")

        self.cursor.execute("CREATE TABLE IF NOT EXISTS erratabugzilla"
                            "(id INTEGER PRIMARY KEY, "
                            "advisory VARCHAR ( 255 ), "
                            "bugzilla_id VARCHAR ( 255 ), "
                            "summary VARCHAR ( 255 ));")

        self.cursor.execute("CREATE TABLE IF NOT EXISTS erratacves"
                            "( id INTEGER PRIMARY KEY, "
                            "advisory VARCHAR ( 255 ), "
                            "cve_id VARCHAR ( 255 ) );")

        self.cursor.execute("CREATE TABLE IF NOT EXISTS erratapackages"
                            "( id INTEGER PRIMARY KEY, "
                            "advisory VARCHAR ( 255 ), "
                            "name VARCHAR ( 255 ), "
                            "version VARCHAR ( 255 ), "
                            "release VARCHAR ( 255 ), "
                            "epoch VARCHAR ( 255 ), "
                            "arch_label VARCHAR ( 255 ), "
                            "file VARCHAR ( 255 ), "
                            "md5sum VARCHAR ( 255 ));")
        self.db.commit()

    def select(self, table, advisory=None, type=None,
               synopsis=None, bugzilla_id=None, cve_id=None):
        """Get information from the *errata* or misc tables"""
        if table == 'errata':
            if advisory and type:
                self.cursor.execute(
                    "SELECT advisory, type, description "
                    "FROM errata "
                    "WHERE advisory=? AND type=?;",
                    (advisory, type))
            elif advisory:
                self.cursor.execute(
                    "SELECT description "
                    "FROM errata "
                    "WHERE advisory=?;",
                    (advisory,))
            else:
                self.cursor.execute("SELECT advisory, type "
                                    "FROM errata "
                                    "ORDER BY advisory ASC;")
        elif table == 'channelerrata':
            if synopsis:
                self.cursor.execute(
                    "SELECT * "
                    "FROM channelerrata "
                    "WHERE advisory=? AND synopsis=?;",
                    (advisory, synopsis))
            else:
                self.cursor.execute(
                    "SELECT synopsis, issue_date, update_date "
                    "FROM channelerrata "
                    "WHERE advisory=?;",
                    (advisory,))
        elif table == 'erratabugzilla':
            if bugzilla_id:
                self.cursor.execute(
                    "SELECT id, summary "
                    "FROM erratabugzilla "
                    "WHERE advisory=? AND bugzilla_id=?;",
                    (advisory, bugzilla_id))
            else:
                self.cursor.execute(
                    "SELECT bugzilla_id, summary "
                    "FROM erratabugzilla "
                    "WHERE advisory=?;",
                    (advisory,))
        elif table == 'erratacves':
            if cve_id:
                self.cursor.execute(
                    "SELECT id "
                    "FROM erratacves "
                    "WHERE advisory=? AND cve_id=?;",
                    (advisory, cve_id))
            else:
                self.cursor.execute(
                    "SELECT cve_id "
                    "FROM erratacves "
                    "WHERE advisory=?;",
                    (advisory,))
        elif table == 'misc':
            self.cursor.execute("SELECT channelname FROM misc")
        elif table == 'erratapackages':
            self.cursor.execute("SELECT name, version, release, epoch, arch_label, file, md5sum "
                                "FROM erratapackages "
                                "WHERE advisory=?;",
                                (advisory,))
        return self.cursor.fetchall()

    def insert(self, table, advisory=None, type=None,
               synopsis=None, issued_date=None, updated_date=None,
               description=None, bugzilla_id=None, summary=None, cve_id=None,
               channelname=None, pkgname=None, pkgversion=None, release=None,
               epoch=None, arch_label=None, file=None, md5sum=None):
        """Insert data into a specific table"""
        if table == 'errata':
            self.cursor.execute("INSERT INTO errata(advisory,type, description)"
                                " VALUES(?,?,?);", (advisory, type, description))
        elif table == 'channelerrata':
            self.cursor.execute("INSERT INTO channelerrata"
                                "(advisory, synopsis, issue_date, update_date) "
                                "VALUES(?,?,?,?);",
                                (advisory, synopsis, issued_date, updated_date))
        elif table == 'erratabugzilla':
            self.cursor.execute(
                "INSERT INTO erratabugzilla(advisory, bugzilla_id, summary) "
                "VALUES(?,?,?);", (advisory, bugzilla_id, summary))
        elif table == 'erratacves':
            self.cursor.execute("INSERT INTO erratacves(advisory,cve_id) "
                                "VALUES(?,?);", (advisory, cve_id))
        elif table == 'misc':
            self.cursor.execute("INSERT INTO misc(id,lastrundate,channelname) "
                                "VALUES(?,?,?);", (1, "2006-06-01", channelname))
        elif table == 'erratapackages':
            self.cursor.execute("INSERT INTO erratapackages"
                                "(advisory, name, version, release, epoch, "
                                "arch_label, file, md5sum) VALUES(?,?,?,?,?,?,?,?);",
                                (advisory, pkgname, pkgversion, release, epoch,
                                 arch_label, file, md5sum))

        self.db.commit()

    def update(self, table, advisory=None, type=None,
               synopsis=None, issued_date=None, updated_date=None,
               description=None, bugzilla_id=None, summary=None, channelname=None):
        """Update data of a specific table"""
        if table == 'errata':
            self.cursor.execute(
                "UPDATE errata "
                "SET advisory=?, type=?, description=? "
                "WHERE advisory=?;",
                (advisory, type, description, advisory))
        elif table == 'channelerrata':
            self.cursor.execute(
                "UPDATE channelerrata "
                "SET advisory=?, synopsis=?, issue_date=?, update_date=? "
                "WHERE advisory=?;",
                (advisory, synopsis, issued_date, updated_date, advisory))
        elif table == 'erratabugzilla':
            self.cursor.execute(
                "UPDATE erratabugzilla "
                "SET advisory=?,bugzilla_id=?,summary=? "
                "WHERE advisory=? AND bugzilla_id=?;",
                (advisory, bugzilla_id, summary, advisory, bugzilla_id))
        elif table == 'misc':
            self.cursor.execute("UPDATE misc "
                                "SET channelname=? "
                                "WHERE id=1;", (channelname,))
        self.db.commit()
