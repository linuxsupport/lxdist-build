SUPPORTED_DISTS="slc6 cc7 c8"
CENTOS_VERSION="7.9.2009"

_GOTUSER=0
# Real user
USER=`klist | grep "Default principal" | cut -d '@' -f 1 | cut -d ' ' -f 3`
if [ "x$USER" == "x" ]; then
  cat /etc/linuxci.credential | kinit linuxci &>/dev/null
  USER=`klist | grep "Default principal" | cut -d '@' -f 1 | cut -d ' ' -f 3`
fi
[ "x$USER" == "xlinuxci" ] && USERFULL="CERN Linux Droid" && USEREMAIL="linux.ci@cern.ch" && _GOTUSER=1
[ $_GOTUSER -eq 0 ] && echo -e $ROUGE "*** User krb credential only works with linuxci. Please do 'kdestroy' and run the script again." && exit 1

# Learn colors in french
VERT="\\033[1;32m"
NORMAL="\\033[0;39m"
NORMALB="\\033[1;39m"
ROUGE="\\033[1;31m"
ROSE="\\033[1;35m"
BLEU="\\033[1;34m"
BLANC="\\033[0;02m"
BLANCLAIR="\\033[1;08m"
JAUNE="\\033[1;33m"
CYAN="\\033[1;36m"
