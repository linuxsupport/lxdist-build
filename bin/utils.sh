#!/bin/bash

# INPUT   $1    : filename
#	  $2    : DIST slc5 slc6
# OUTPUT  THERE : 
#         ISSRC :
# 
# TEST suite ;)
#wheredoIbelong /mnt/data2/dist/cern/slc5X/updates/x86_64/RPMS/man-pages-overrides-5.8.3-2.el5.noarch.rpm slc5
#echo "###$THERE"
#wheredoIbelong /mnt/data2/dist/cern/slc5X/extras/x86_64/RPMS/cern-get-sso-cookie-0.4.2-2.slc5.noarch.rpm slc5
#echo "###$THERE" 
#wheredoIbelong /mnt/data2/dist/onlycern/slc5X/i386/RPMS/cx_Oracle-5.1.1-1.slc5.i386.rpm slc5
#echo "###$THERE" 
#wheredoIbelong /mnt/data2/dist/cern/slc6X/updates/x86_64/RPMS/man-1.6f-30.el6.x86_64.rpm slc6	
#echo "###$THERE" 
#wheredoIbelong /mnt/data2/dist/cern/slc6X/extras/x86_64/RPMS/xmltooling-schemas-1.4.2-3.slc6.x86_64.rpm slc6
#echo "###$THERE" 
#wheredoIbelong /mnt/data2/dist/onlycern/slc6X/x86_64/RPMS/cern-smartcard-0.3-1.slc6.x86_64.rpm slc6
#echo "###$THERE" 

# For cc7 $REPO must be define.
function wheredoIbelong() 
{
	ISSRC=1
	THERE="updates"
	NAME=""
	VERSION=""
	RELEASE=""
	PKG=""
	if [ -z "$2"  ]
	then
		echo "*** Wrong parameters"
		exit 1
	fi
	if [ -z "$1"  ]
	then
		echo "*** Wrong parameters"
		exit 1
	fi

	NAME=""
        VERSION=""
        RELEASE=""
        _TMP=`mktemp`
        /bin/rpm --nosignature -qp $1 --qf "%{NAME};%{VERSION};%{RELEASE};%{ARCH}\n" > "$_TMP"
        OLDIFS="$IFS"
        IFS=';'
        read NAME VERSION RELEASE ARCH <"$_TMP"
        IFS="$OLDIFS"
        rm -f "$_TMP"
	[ "x$ARCH" == "xi586"   ] && ARCH="i386"
	[ "x$ARCH" == "xi686"   ] && ARCH="i386"
	
	# FIXME Arch not defined correctly for SRC/RPM
	res=`file -b $1 | grep "RPM v3.0 src" 2> /dev/null`
	if [ $? -gt "0" ]
	then
		ISSRC=0
		PKG=`basename $PKG .rpm`
	else
		PKG=`basename $PKG src.rpm`
	fi

	case $2 in
	slc5)
		TOEXTRAS="
          	php-json
          	linuxsoft-
          	firefox-adblock_plus
          	firefox-adblock_plus_ehhelper
          	thunderbird-adblock_plus
          	thunderbird-adblock_plus_ehhelper
          	adblock_plus
          	adblock_plus_ehhelper
          	enigmail
          	thunderbird-enigmail
          	lightning
          	thunderbird-lightning
          	lightning-exchange-provider
          	thunderbird-lightning-exchange-provider
          	provider_for_google_calendar
		sage
		firefox-sage
		vcs_support
		thunderbird-vcs_support
		noscript
		firefox-noscript
		google_contacts
		thunderbird-google_contacts
		moonlight
		firefox-moonlight
		googleearth
		mshvic
		kmod-mshvic
		kmod-mshvic-PAE
		kmod-mshvic-xen
		aims2client
		aims2-client
		aims2-server
		adaptec-cli
		3ware-util-cli
		areca_tools
		fsplit
		fsprobe
		cernlib
		geant321
		kuipc-2006
		patchy-2006
		patchy-gfortran
		paw-2006
		paw-gfortran
		megaraid-util-cli
		mplayer-codecs-addon
		distcc
		distcc-server
		rfbench
		rpmver
		slac-console
		flash-plugin
		acroread
		kernel-module-sstat
		nvidia-glx kernel-module-nvidia
		ati-fglrx kernel-module-ati-fglrx
		madwifi kmod-madwifi kernel-module-madwifi
		e1000 e1000-kmod kernel-module-e1000
		e1000e e1000e-kmod kernel-module-e1000e
		ipw3945 kernel-module-ipw3945
		netlog2 kernel-module-netlog2
		st-driver kernel-module-st
		ioatdma-driver kernel-module-ioatdma
		ixgbe-driver kernel-module-ixgbe
		firefox-add-on
                firefox-add-ons 
		firefox-noscript_security_suite
		firefox-adblock_plus
		firefox-adblock_plus_ehhelper
		thunderbird-add-on
                thunderbird-add-ons
                thunderbird-calendar-tweaks
                thunderbird-provider_for_google_calendar
		thunderbird-exchange
		firefox-thunderbird-add-ons
		odf-converter
		x264
		xine-lib-extras-nonfree
		xvidcore
		vcdimager
		twolame
		mplayer
		mencoder
		lxdvdrip
		live
		lirc
		libtunepimp
		libdvdread
		libdvdnav
		libmp4v2
		libdvdcss
		libdca
		libcaca
		kplayer
		k3b-extras-nonfree
		id3lib
		gnome-mplayer
		gecko-mediaplayer
		ffmpeg
		faad2
		faac
		dvdauthor
		caca
		amarok-extras-nonfree
		a52dec
		3w-9xxx kmod-3w-9xxx
		aacraid kmod-aacraid
		e1000 kmod-e1000
		e1000e kmod-e1000e
		ioatdma kmod-ioatdma
		ixgbe kmod-ixgbe
		ixgb kmod-ixgb
		st-driver kmod-st-driver
		mpeg2dec
		xosd
		k9copy
		amrwb
		amrnb
		libtar
		libcddb
		libdvbpsi
		libebml
		libbopendaap
		libmatroska
		libshout
		wxGTK
		wxBase
		vlc
		xorg-x11-drv-nvidia kernel-module-nvidia
		xorg-x11-drv-fglrx kernel-module-fglrx
		xine-lib-extras-freeworld
		compat-expat2
		log4shib
		opensaml
		xerces-c
		xmlsecurity-c
		xmltooling
		shibboleth
		liblog4shib
		log4shib
		libsaml
		libxerces-c-3_1
		libxerces-c-devel-3.1
		libxml-security
		libxmltooling
		xml-security-c
		lin_tape
		kmod-lin_tape
		libsamplerate
		vamp-plugin
		soundtouch
		audacity
		lustre
		kernel-module-lustre
		xrootd-devel
		xrootd-server
		xrootd-client
		kernel-module-iscsi-scst
		kernel-module-scst
		iscsi-scst-utils
		scst-kernel-headers
		truecrypt
		pidgin-sipe
		flawfinder
		rats
		pychecker
		drupal6
		pgplot
		pgplot-devel
		pgplot-demos
		tcl-pgplot
		tcl-pgplot-devel
		iozone
		cern-cvi-addons
                cern-private-cloud-addons
                cern-ipv6-addons
		remoteapp
		femail
		mod_chroot
		mod_ruid2
		shibboleth-selinux
		mhvtl
		kmod-mhvtl
		kernel-module-cifs
		thunderbird-lightning
		thunderbird-provider
		thunderbird-startupmaster
		firefox-startupmaster
		cern-get-sso-cookie
		perl-WWW-CERNSSO-Auth
		perl-WWW-Curl
        	hpglview
        	castor-devel
        	castor-lib
        	castor-ns-client
        	castor-rfio-client
        	castor-stager-client
        	castor-upv-client
        	castor-vdqm2-client
        	castor-vmgr-client
		VidyoDesktop
		cmake28
		cern-java-deployment-ruleset
                libxerces-c-3_0
		koji
		koji-
		cern-get-certificate
          	"
		TOCERNONLY="
		oracle-instantclient
		perl-DBD-Oracle
		php-oci8    
		php-oracle
		cx_Oracle
		tora
		java-1.6.0-sun
		bcm43xx_firmware
		skype 
		SafesignIdentityClient
		SafeSign_Identity_Client
		aetssic-
		firefox-aetssic
		thunderbird-aetssic
		cern-rpmverify
		tty-kraven
		kmod-tty-kraven
		splunk
		splunkforwarder
		gskcrypt64
		gskssl64
		TIVsm-
		TDP-Oracle
		TDP-Oracle.Utility
		"
		;;
		
	slc6)
		TOEXTRAS="
                cloud-init
		cloud-utils
		dracut-modules-growroot
                sl-release-scl-rh
                sl-release-scl
		linuxsoft-
		firefox-adblock_plus
		firefox-adblock_plus_ehhelper
		thunderbird-adblock_plus
		thunderbird-adblock_plus_ehhelper
		adblock_plus
		adblock_plus_ehhelper
		enigmail
		thunderbird-enigmail
		lightning
		cronolog
		thunderbird-lightning
		lightning-exchange-provider
		thunderbird-lightning-exchange-provider
		provider_for_google_calendar
		thunderbird-provider_for_google_calendar
		sage
		firefox-sage
		vcs_support
		thunderbird-vcs_support
		noscript
		firefox-noscript
		google_contacts
		thunderbird-google_contacts
		moonlight
		firefox-moonlight
		googleearth
		mshvic
		kmod-mshvic
		kmod-mshvic-PAE
		kmod-mshvic-xen
		microsoft-hyper-v
		kmod-microsoft-hyper-v
		aims2client
		aims2-client
		aims2-server
		adaptec-cli
		3ware-util-cli
		areca_tools
		fsplit
		fsprobe
		cernlib
		geant321
		kuipc-2006
		patchy-2006
		patchy-gfortran
		paw-2006
		paw-gfortran
		megaraid-util-cli
		mplayer-codecs-addon
		distcc
		distcc-server
		rfbench
		rpmver
		slac-console
		flash-plugin
		acroread
		kernel-module-sstat
		nvidia-glx kernel-module-nvidia
		ati-fglrx kernel-module-ati-fglrx
		madwifi kmod-madwifi kernel-module-madwifi
		e1000 e1000-kmod kernel-module-e1000
		e1000e e1000e-kmod kernel-module-e1000e
		ipw3945 kernel-module-ipw3945
		netlog2 kernel-module-netlog2
		netlog kernel-module-netlog
		st-driver kernel-module-st
		ioatdma-driver kernel-module-ioatdma
		ixgbe-driver kernel-module-ixgbe
		firefox-add-on
		thunderbird-add-on
		firefox-thunderbird-add-ons
		odf-converter
		x264
		xine-lib-extras-nonfree
		xvidcore
		vcdimager
		twolame
		mplayer
		mencoder
		lxdvdrip
		live
		lirc
		libtunepimp
		libdvdread
		libdvdnav
		libmp4v2
		libdvdcss
		libdca
		libcaca
		kplayer
		k3b-extras-nonfree
		id3lib
		gnome-mplayer
		gecko-mediaplayer
		ffmpeg
		faad2
		faac
		dvdauthor
		caca
		amarok-extras-nonfree
		a52dec
		3w-9xxx kmod-3w-9xxx
		aacraid kmod-aacraid
		e1000 kmod-e1000
		e1000e kmod-e1000e
		ioatdma kmod-ioatdma
		ixgbe kmod-ixgbe
		ixgb kmod-ixgb
		st-driver kmod-st-driver
		mpeg2dec
		xosd
		k9copy
		amrwb
		amrnb
		libcddb
		libdvbpsi
		libebml
		libbopendaap
		libmatroska
		libshout
		wxGTK
		vlc
		xorg-x11-drv-nvidia kernel-module-nvidia
		xorg-x11-drv-fglrx kernel-module-fglrx
		xine-lib-extras-freeworld
		compat-expat2
		log4shib
		opensaml
		xerces-c
		xmlsecurity-c
		libcurl-openssl
		xmltooling
		shibboleth
		log4shib
		liblog4shib
                libsaml
                libxerces-c-3_1
                libxerces-c-devel-3.1
                libxml-security
                libxmltooling
                xml-security-c
		curl-openssl
		libcurl-openssl
		lin_taped
		lin_tape
		kmod-lin_tape
		libsamplerate
		vamp-plugin
		soundtouch
		audacity
		lustre
		kernel-module-lustre
		xrootd-devel
		xrootd-server
		xrootd-client
		kernel-module-iscsi-scst
		kernel-module-scst
		iscsi-scst-utils
		scst-kernel-headers
		truecrypt
		pidgin-sipe
		flawfinder
		rats
		pychecker
		drupal6
		pgplot
		pgplot-devel
		pgplot-demos
		tcl-pgplot
		tcl-pgplot-devel
		iozone
		cern-cvi-addons 
                cern-private-cloud-addons
                cern-ipv6-addons
		remoteapp
		mhvtl
		kmod-mhvtl
		freerdp
		kernel-module-cifs
		thunderbird-lightning
		thunderbird-provider
		rhythmbox-screensaver
		thunderbird-startupmaster
		firefox-startupmaster
		cern-get-sso-cookie
		perl-WWW-CERNSSO-Auth
		hpglview
		castor-devel
		castor-lib
		castor-ns-client
		castor-rfio-client
		castor-stager-client
		castor-upv-client
		castor-vdqm2-client
		castor-vmgr-client
		VidyoDesktop
		HEP_OSlibs
		shibboleth-selinux
		kS4U
		parallel-
		cern-java-deployment-ruleset
		koji
		koji-
        python2-koji
        python2-koji-
		cern-get-certificate
		mod_auth_mellon_cern
		spl-
		spl
		kmod-spl-
		kmod-spl
		spl-kmod
		zfs-kmod
		kmod-zfs
		libnvpair1-
		libnvpair1
		libuutil1-
		libuutil1
		libzfs2-
		libzfs2
		libzpool2-
		libzpool2
		zfs-
		zfs
		zfs-dracut
		zfs-test
		subversion17
		cln
		protobuf3
		python-protobuf3
		"

		TOCERNONLY="
		oracle-instantclient
		perl-DBD-Oracle
		php-oci8    
		php-oracle
		cx_Oracle
		tora
		java-1.6.0-sun
		bcm43xx_firmware
		skype 
		SafesignIdentityClient
		SafeSign_Identity_Client
		aetssic
		firefox-aetssic
		thunderbird-aetssic
		cern-smartcard 
		cern-rpmverify
		tty-kraven
		kmod-tty-kraven
		redeemer
		kmod-redeemer
		splunk
		splunkforwarder
		cmake28
		gskcrypt64
		gskssl64
		TIVsm-
		TDP-Oracle
		TDP-Oracle.Utility
		"
		;;
	slc6nonpae)
		THERE=""
		;;
	cc7)
		TOEXTRAS=""
		TOCERNONLY=""
		THERE="$REPO"
		;;
	*)
		echo "*** $2 is not configured"
		exit 1
		;;
	esac
	
	# Check EXTRAS
	for PKG in ${TOEXTRAS} 
	do
		if [[ "$NAME" == "$PKG"*  ]]
		then 
			THERE="extras"
			break
		fi
	done

	# Check CERNONLY
	for PKG in ${TOCERNONLY} 
	do
		if [[ "$NAME" == "$PKG"*  ]]
		then 
			THERE="cernonly"
			break
		fi
	done
} 

# Tsst suite
#pkgSummary "/mnt/data2/home/build/tmp/slc6/updates/x86_64/lvm2-libs-2.02.95-10.el6_3.3.x86_64.rpm /mnt/data2/home/build/tmp/slc6/updates/x86_64/device-mapper-event-devel-1.02.74-10.el6_3.3.i686.rpm /mnt/data2/home/build/tmp/slc6/updates/x86_64/device-mapper-event-libs-1.02.74-10.el6_3.3.i686.rpm /mnt/data2/home/build/tmp/slc6/updates/x86_64/device-mapper-libs-1.02.74-10.el6_3.3.i686.rpm"
#echo $pkgSummaryOUTPUT
function pkgSummary ()
{
        importantPackages="centos-release kernel kernel-plus kernel-rt dbus glibc cern-get-keytab hepix qemu-kvm systemd libvirt firewalld python sssd"
	pkgSummaryOUTPUT=""
	pattern='^([^.]*)-([0-9].*-.*)'
	for FILE in $@
	do
		FILENAME=`echo $FILE | rev | cut -d '/' -f1 |rev`
		[[ $FILENAME =~ $pattern ]]	
		NAME=${BASH_REMATCH[1]}
		pkgSummaryOUTPUT="$pkgSummaryOUTPUT\n$NAME"
	done
	pkgSummaryOUTPUT=`printf $pkgSummaryOUTPUT | sort -u | uniq | grep -v devel | grep -v '^$'`
        importantpkgSummaryOUTPUT=""
        for importantPackage in $importantPackages; do
          for standardPackage in $pkgSummaryOUTPUT; do
	    if [[ "$standardPackage" == "$importantPackage" ]]; then
              importantpkgSummaryOUTPUT="$importantpkgSummaryOUTPUT $importantPackage"
            fi
          done
        done
        pkgSummaryOUTPUT="`echo $importantpkgSummaryOUTPUT $pkgSummaryOUTPUT | uniq | cut -d " " -f1-4| sed 's/ /, /g'` [...]"
        echo "$pkgSummaryOUTPUT"
}
