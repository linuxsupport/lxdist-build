"""Update advisories info"""
from argparse import ArgumentParser
import xml.etree.cElementTree as ET
from updatesqlite import SQLiteQueries


def generate_xmlfile(pargs, advisory_db):
    """
    Generating xml file.
    """

    m_encoding = 'UTF-8'
    updateinfofile = pargs.updinfofile

    # updates
    root = ET.Element("updates")
    erratarows = advisory_db.select(table='errata')
    for erratarow in erratarows:
        advisory, type_ad = erratarow
        xml_update_type = "bugfix"
        if type_ad == "bug fix":
            xml_update_type = "bugfix"
        else:
            xml_update_type = type_ad

        # update
        doc_update = ET.SubElement(
            root, "update", {'from': 'security@redhat.com', 'status': 'final',
                             'type': xml_update_type, 'version': '1'})
        ET.SubElement(doc_update, "id").text = advisory
        channelerratarows = advisory_db.select(table='channelerrata',
                                               advisory=advisory)

        for channelerratarow in channelerratarows:
            synopsis, issue_date, update_date = channelerratarow
            ET.SubElement(doc_update, "title").text = synopsis
            ET.SubElement(doc_update, "issued", {'date': issue_date})
            ET.SubElement(doc_update, "updated", {'date': update_date})

        erratarows_ad = advisory_db.select(table='errata', advisory=advisory)
        for description_ad in erratarows_ad:
            ET.SubElement(doc_update, "description").text = description_ad[0]

        erratabugzillafixesrows = advisory_db.select(table='erratabugzilla',
                                                     advisory=advisory)
        erratabugzillacount = len(erratabugzillafixesrows)
        erratacvesrows = advisory_db.select(table='erratacves',
                                            advisory=advisory)
        erratacvescount = len(erratacvesrows)

        doc_references = ET.SubElement(doc_update, "references")
        if erratabugzillacount > 0 or erratacvescount > 0:
            # references
            for erratabugzillafixesrow in erratabugzillafixesrows:
                bugzilla_id, summary = erratabugzillafixesrow
                ET.SubElement(doc_references, 'reference',
                              {'href': "http://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=%s" % (bugzilla_id),
                               'id': bugzilla_id, 'type': 'bugzilla'}).text = summary
            for erratacvesrow in erratacvesrows:
                ET.SubElement(doc_references, 'reference',
                              {'href': "http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=%s" % (erratacvesrow),
                               'id': erratacvesrow[0], 'type': 'cve'})

        xmlchannelnamevalue = advisory_db.select(table='misc')
        # pkglist
        doc_pkglist = ET.SubElement(doc_update, "pkglist")
        # collection
        rhchannel = "RHEL"
        doc_collection = ET.SubElement(doc_pkglist, "collection", {'short': rhchannel})
        ET.SubElement(doc_collection, 'name').text = xmlchannelnamevalue[0][0]
        erratapackagesrows = advisory_db.select(table='erratapackages',
                                                advisory=advisory)
        for erratapackagesrow in erratapackagesrows:
            name, version, release, epoch, arch_label, file, md5sum = erratapackagesrow
            md5sum = ""
            epoch = "0" if not epoch else epoch
            doc_pkg = ET.SubElement(
                doc_collection, "package",
                {'name': name, 'version': version, 'release': release,
                 'epoch': epoch, 'arch': arch_label, 'src': ""})
            ET.SubElement(doc_pkg, "filename").text = file
            ET.SubElement(doc_pkg, 'sum', {'type': "md5"}).text = md5sum

    tree = ET.ElementTree(root)
    with open(updateinfofile, 'w') as xfile:
        tree.write(xfile, encoding='utf-8', xml_declaration=True)

def main():
    """Main function to parse the advisories arguments"""
    parser = ArgumentParser(description="Advisores - update information")
    parser.add_argument('--file', metavar="FILE", dest="updinfofile",
                        help="Filename to update information")
    parser.add_argument('--database', metavar="FILE", dest="dbfile",
                        help="Database file")
    parser.add_argument('--arch', dest='archlabel',
                        help="Arch")
    parser.add_argument('--updatexml', action="store_true", default=False, dest='updatexml',
                        help="Update the xml file with the data in the database")
    pargs = parser.parse_args()

    if pargs.archlabel == 'i686':
        pargs.archlabel = "i386|i486|i586|i686"


    # Create tables if they do not exist
    db = SQLiteQueries(pargs.dbfile)
    db.connect_db()
    db.create_tables()

    if pargs.updatexml:
        generate_xmlfile(pargs, db)
    
    db.close_connection()


if __name__ == '__main__':
    main()
