#!/bin/bash

### Helpers
# Need to be admin to run this script
function user_can_build {
	RET=0
	PERM=`koji list-permissions --mine --quiet | grep -v image | grep -v build | grep -v "Permission name" | xargs`
	if [ "$PERM" != "admin"  ]; then
        	echo "[ERROR] Koji misconfigure/missing admin privilege for building packages"
		RET=1
	fi
	if [ `whoami` != "build"  ]; then
        	echo "[ERROR] You need to run this script as the 'build' user not '`whoami`'"
		RET=1
	fi
	return $RET
}

function build_status {
    if [ "$1" != "" ]; then
        status=`koji buildinfo $1 | grep "State: " | cut -d":" -f2`
        echo ${status/ /}
    fi
}

function koji_download {
    case $1 in
        32)
            opts="--arch=i686 --arch=i586" # --arch=i686 --arch=i586
        ;;
        64)
            opts="--arch=x86_64"
        ;;
        noarch)
            opts="--arch=noarch"
        ;;
        *)
            echo "No arch specified"
            exit 1
    esac
    if [ "x$2" == "x" ]; then
        echo "No package specified"
        exit 1
    fi
    if [ "x$3" == "x" ]; then
        echo "No path specified"
        exit 1
    fi
    cd $3
    echo "### Downloading package [$2] (destination:$3) :"
    koji \
        download-build \
        --debuginfo \
        $opts $2
    koji_retval=$?
    if (( $koji_retval )); then
        echo "No package $2 for $1. (error: $koji_retval)"
    fi
    cd -
}

function koji_download_all {
	if [ "x$1" == "x" ]; then
        	echo "No package specified"
        	exit 1
	fi
	if [ "x$2" == "x" ]; then
        	echo "No path specified"
        	exit 1
	fi
	cd $2
	koji \
        	download-build \
        	--debuginfo \
		$1
	koji_retval=$?
	if (( $koji_retval )); then
        	echo "No package $1. (error: $koji_retval)"
	fi
	cd -
}

function list_tags {
    echo `koji list-tags | grep -v build | sort`
}

function list_tags_static {
    echo "el7sat el7ev el7cp el6cp el6_10 el6_9 el6_8 el6_7 el6_6 el7_9.cern el7_8.cern el7_7.cern el7_6.cern el7_5.cern el7_4.cern el7_3.cern el7_2.cern el7_1.cern el7_0.cern el7.cern el7_9 el7_8 el7_7 el7_6 el7_5 el7_4 el7_3 el7_2 el7_1 el7_0 el7 el6rhs el6sat el6ev ep5.el6 ep6.el6 el5 el5_0 el5_11 el5_10 el5_1 el5_2 el5_3 el5_4 el5_5 el5_6 el5_7 el5_8 el5_9 el5rt el6 el6_0 el6_1 el6_2 el6_3 el6_4 el6_5 el6rt ev6 slc5 slc6 el6sat"
}

# List task from a file by id or pkg or link
# INPUT  : [filename] [argument]
# OUTPUT : pkg name or pkg id or full description
function list_tasks {
    for task in `cat $1`; do
        id=`echo "$task" | cut -d';' -f1`
        pkg=`echo "$task" | cut -d';' -f2`
        if [ "$2" == "id" ]; then
            echo "$id" 
        elif [ "$2" == "pkg" ]; then
            echo "$pkg"
        else
            echo "[$pkg]$KOJIURL/taskinfo?taskID=$id"
        fi
    done
}

# Return the task status
function task_status {
    if [ "$1" != "" ]; then
        status=`koji taskinfo $1 | grep "State: " | cut -d":" -f2`
        echo ${status/ /}
    fi
}

# Return the task type
function task_type {
    if [ "$1" != "" ]; then
        type=`koji taskinfo $1 | grep "Type: " | cut -d":" -f2`
        echo ${type/ /}
    fi
}

function detect_tag {
        echo ""
        for tag in list_tags
        do
                echo "$tag"
        done
}

# $1 = SRPM path
# Return : the task id 
# 	 -2 if tag does not exist.
function send_koji {
	KOJITASKID=""
	NAME=""
	VERSION=""
	RELEASE=""
	COLLECTION=""
	EXIST=""
	_TMP=`mktemp`
	/bin/rpm --nosignature -qp $1 --qf "%{NAME};%{VERSION};%{RELEASE}\n" > "$_TMP"
	OLDIFS="$IFS"
	IFS=';'
	read NAME VERSION RELEASE <"$_TMP"
	IFS="$OLDIFS"
	rm -f "$_TMP"
	PKG=`basename $1 .src.rpm`
	#TODO Better Detect tag
	for TAG in `list_tags_static`
	do
		KOJITAG=$LATESTTAG
		C=`echo $1 | /bin/grep -c -P "$TAG\."`
		if [ "$C" -gt "0" ]; then
			KOJITAG=$TAG
			break
		fi
	done

#### Seriously Red Hat ???? qemu-kvm-rhev-2.1.2-23.el7_1.1.src.rpm -> dist tag = .el7 !!!! WTF !!!
        if [[ `basename $SRPM` == qemu-kvm-rhev-2.1.2-23.el7_1.1.src.rpm ]] 
        then
               KOJITAG="el7"
        fi

	if [ "x$FORCETAG" != "x" ]
	then
		REALTAG=$KOJITAG
		KOJITAG=$FORCETAG
		if [ "$FORCETAG" == "jbeap6" ]
		then
			KOJITAG=${FORCETAG}_${REALTAG}
		fi
		if [ "$FORCETAG" == "ev6" ]
		then
			KOJITAG=${FORCETAG}_${REALTAG}
		fi
		if [ "$FORCETAG" == "devtoolset6" ]
		then
			KOJITAG=${FORCETAG}_${REALTAG}
		fi
                if [ "$FORCETAG" == "rhc6" ] && [ "$REALTAG" == "el6rhs" ]
                then
                        KOJITAG=${FORCETAG}_${REALTAG}
                fi
                if [ "$FORCETAG" == "rhc6" ] && [ "$REALTAG" == "el6sat" ]
                then
                        KOJITAG=${FORCETAG}_${REALTAG}
                fi
                if [ "$FORCETAG" == "rhc6" ] && [ "$REALTAG" == "el6_4" ]
                then
                        KOJITAG=${FORCETAG}_${REALTAG}
                fi
                if [ "$FORCETAG" == "rhc6" ] && [ "$REALTAG" == "el6cp" ]
                then
                        KOJITAG=${FORCETAG}_${REALTAG}
                fi
		if [ "x$USECOLLECTION" == "xyes" ]
		then
			if [[ $NAME == "devtoolset-3"* ]]
			then
				COLLECTION="devtoolset-3"
			else
				COLLECTION=`echo $NAME | cut -d "-" -f 1`
			fi
			EXIST=$(koji list-tags | grep "${KOJITAG}_$COLLECTION-build")
			if  [ "x$EXIST" != "x" ]
			then
				KOJITAG="${FORCETAG}_${COLLECTION}"
			fi	
			if  [[ $REALTAG == el5 ]] || [[ $REALTAG == el6 ]] 
			then
				TOTO=""
			else
				KOJITAG="${FORCETAG}_${COLLECTION}_${REALTAG}"
			fi
			#if [[ "x$REALTAG" != "*xel5*" ]] || [[ "x$REALTAG" != "*xel6*" ]]
			#then
				#KOJITAG="${FORCETAG}_${COLLECTION}_${REALTAG}"
				#EXIST2=$(koji list-tags | grep "${KOJITAG}-build")
				#if  [ "x$EXIST2" == "x" ]
                        	#then
				#	#echo " -> [FIXME] tag ${KOJITAG}-build does not exist."
				#	echo "0"
				#	exit -1
				#fi
			#fi
		fi
		KOJIADD=`koji add-pkg $2 --owner slc-team $FORCETAG-testing $NAME`
	fi
	KOJIADD=`koji add-pkg $2 --owner slc-team $KOJITAG-qa $NAME 2>/dev/null`
	KOJIADD=`koji add-pkg $2 --owner slc-team $KOJITAG-testing $NAME`
	KOJIADD=`koji add-pkg $2 --owner slc-team $KOJITAG-stable $NAME`
	KOJIADD=`koji add-pkg $2 --owner slc-team $KOJITAG $NAME`
	SRPM=$1
	if [ "x$DIST" == "xslc5" ]
	then
		KOJIADD=`koji add-pkg $2 --owner slc-team el5 $NAME`
		if [[ `basename $SRPM` == GFS-kernel-* ]] ||
			[[ `basename $SRPM` == gnbd-kernel-* ]] ||
			[[ `basename $SRPM` == gfs-kmod-* ]] ||
			[[ `basename $SRPM` == cmirror-kmod-* ]] ||
			[[ `basename $SRPM` == cman-kernel-* ]] ||
			[[ `basename $SRPM` == dlm-kernel-* ]] ||
			[[ `basename $SRPM` == kernel-2.6* ]] ||
			[[ `basename $SRPM` == fence-kernel-* ]] ||
			[[ `basename $SRPM` == gulm-* ]] ||
			[[ `basename $SRPM` == iddev-* ]] ||
			[[ `basename $SRPM` == ccs-* ]] ||
			[[ `basename $SRPM` == cman-* ]] ||
			[[ `basename $SRPM` == magma-* ]] ||
			[[ `basename $SRPM` == gnbd-* ]] ||
			[[ `basename $SRPM` == fence-* ]] ||
			[[ `basename $SRPM` == 3w-9xxx-* ]] ||
			[[ `basename $SRPM` == aacraid-* ]] ||
			[[ `basename $SRPM` == be2net-kmod-* ]] ||
			[[ `basename $SRPM` == e1000-* ]] ||
			[[ `basename $SRPM` == e1000e-* ]] ||
			[[ `basename $SRPM` == ioatdma-* ]] ||
			[[ `basename $SRPM` == ixgbe-* ]] ||
			[[ `basename $SRPM` == ixgb-* ]] ||
			[[ `basename $SRPM` == lpfc-kmod-* ]] ||
			[[ `basename $SRPM` == st-driver-* ]] ||
			[[ `basename $SRPM` == fuse-2* ]] ||
			[[ `basename $SRPM` == madwifi-* ]] ||
			[[ `basename $SRPM` == ipw3945-* ]] ||
			[[ `basename $SRPM` == kernel-module-nvidia-* ]] ||
			[[ `basename $SRPM` == kernel-module-fglrx-* ]] ||
			[[ `basename $SRPM` == kernel-module-ixgbe-* ]] ||
			[[ `basename $SRPM` == cmirror-kernel* ]] ||
			[[ `basename $SRPM` == dlm-* ]] ||
			[[ `basename $SRPM` == kernel-rt-* ]] ||
			[[ `basename $SRPM` == mhvtl-* ]] ||
			[[ `basename $SRPM` == be2iscsi-kmod-* ]] ||
			[[ `basename $SRPM` == bnx2*-kmod-* ]] ||
			[[ `basename $SRPM` == cnic-kmod-* ]] ||
			[[ `basename $SRPM` == igb-kmod-* ]] ||
			[[ `basename $SRPM` == tg3-kmod-* ]] ||
			[[ `basename $SRPM` == lpfc-kmod-* ]] ||
			[[ `basename $SRPM` == be2net-kmod-* ]] ||
			[[ `basename $SRPM` == lustre-* ]] ||
			[[ `basename $SRPM` == qla2xxx-* ]] ||
			[[ `basename $SRPM` == openssl-* ]] ||
			[[ `basename $SRPM` == glibc-* ]] ||
			[[ `basename $SRPM` == spice-client-* ]] ||
			[[ `basename $SRPM` == lin-tape-* ]] ||
			[[ `basename $SRPM` == tty-kraven-* ]] ||
			[[ `basename $SRPM` == kmod-tty-kraven-* ]] ||
			[[ `basename $SRPM` == netlog-* ]] ||
			[[ `basename $SRPM` == kernel-2.6* ]]; then
			RESULT=`koji set-pkg-arches 'i386 i686 x86_64' $KOJITAG $NAME`
			#echo " -> $NAME will be built for i386 i686 x86_64"
		fi
		if [[ `basename $SRPM` == acroread-* ]]; then
			RESULT=`koji set-pkg-arches 'i386' $KOJITAG $NAME`
			#echo " -> $NAME will be built for i386 only"
		fi	
	fi
	if [ "x$DIST" == "xslc6" ]
	then
		KOJIADD=`koji add-pkg $2 --owner slc-team el6-release $NAME`
		KOJIADD=`koji add-pkg $2 --owner slc-team el6 $NAME`
		if [[ `basename $SRPM` == mingw32-libxml2-* ]]
		then
			RESULT=`koji set-pkg-arches 'x86_64' $KOJITAG $NAME`
			#echo " -> $NAME will be built for x86_64 only"
		fi	
	fi
	if [ "x$DIST" == "xcc7" ]
	then
		if [ "x$REPO" != "x" ]
		then	
			KOJITAG=$FORCETAG-$REALTAG
			if grep -q $NAME "/mnt/data2/home/build/packages/configs/$DIST.biarch.list"
			then
				KOJITAG=${KOJITAG}-biarch
			fi
			KOJIADD=`koji add-pkg --owner slc-team $FORCETAG-testing $NAME`
			KOJIADD=`koji add-pkg --owner slc-team $FORCETAG $NAME`
		fi
	fi
	KOJITASKID=`koji build --nowait $KOJITAG $1  | grep "Task info:" | cut -d "=" -f 2`
	#if [ "x$KOJITASKID" == "x" ]
	#then
	#	exit $KOJITASKID
	#fi
	echo $KOJITASKID
}
